#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <termios.h>
#include <math.h>
#include <time.h>

#define BAUDRATE B9600
#define _POSIX_SOURCE 1		/* POSIX compliant source */
#define FALSE 0
#define TRUE 1
#define DEFAULT_SERIAL_PORT "/dev/ttyS1"

/*  Ultrasonic Positions
 * 
 *  Gaia-2
 *               5
 *           _________
 *      6 _-~         ~-_ 4
 *     _-~               ~-_
 *    /                     \
 * 7 /                       \ 3
 *  /                         \
 * /                           \
 * |                           |
 * |                           |
 * |                           |
 * |                           |
 * |                           |
 * |                           |
 * |                           |
 * |                           |
 * |                           |
 * |                           |
 * |                           |
 * |                           |
 * +---------------------------+
 * 
 */
static struct termios oldtio, newtio;
int
serOpen (char *device)
{
  int fd;
  fd = open (device, O_RDWR | O_NOCTTY);
  if (fd < 0)
    {
      perror (device);
      return -1;
    }

  tcgetattr (fd, &oldtio);	/* save current port settings */
  bzero (&newtio, sizeof (newtio));
  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR;
  newtio.c_oflag = 0;

  /* set input mode (non-canonical, no echo,...) */
  newtio.c_lflag = 0;
  newtio.c_cc[VTIME] = 2;	/* 2/10 second time out */
  newtio.c_cc[VMIN] = 0;	/* blocking read until 5 chars received */
  tcflush (fd, TCIFLUSH);
  tcsetattr (fd, TCSANOW, &newtio);
  return (fd);
}

void
serClose (int fd)
{
/*  tcsetattr (fd, TCSANOW, &oldtio); */
  close (fd);
}


int
openPort (char *device)
{
  int fd;
  if (!device)
    fd = serOpen (DEFAULT_SERIAL_PORT);
  else
    fd = serOpen (device);

  if (fd < 0)
    {
      fprintf (stderr, "Could not open serial port\n");
      exit (-1);
    }

  /*  fprintf (stderr, "Opened serial port\n"); */
  return (fd);
}

int
readSonar (int fd, unsigned char sonar[])
{
  unsigned char readus[8];
  int i;
  int result;

  errno = 0;
  result = -10;
  while (read (fd, readus, 1) == 1)
    {
      if (readus[0] == 0)
	{
	  i = 0;
	  while (i < 8)
	    {
	      if ((result = read (fd, readus + i, 1)) == 1)
		{
		  if (readus[i])
		    {
		      sonar[i] = readus[i];
		      i++;
		    }
		  else
		    break;
		}
	      result = 0;
	    }
	  if (i == 8)
	    result = 0;
	  break;
	}
    }
  if(result == -10)
	  perror("read");
  return (result);
}
