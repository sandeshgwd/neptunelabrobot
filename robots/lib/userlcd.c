/*
 */

#include <unistd.h>		/*usleep(), read(), write(), etc... */
#include <stdio.h>
#include <sys/types.h>		/* Next 3 for open() */
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include "lcd.h"


#if 0
#define LCD_IOC_ID 'Z'
#define LCD_IOC_INIT	_IO(LCD_IOC_ID, 0)
#define LCD_IOC_CLEAR	_IO(LCD_IOC_ID, 1)
#define LCD_IOC_HOME	_IO(LCD_IOC_ID, 2)
#define LCD_IOC_GOTO	_IOW(LCD_IOC_ID, 3, struct LCD_S)
#define LCD_IOC_CTRL	_IOW(LCD_IOC_ID, 4, char)
#endif

static int lcdfd;		/*file descriptor */

int
lcdopen (int dev)
{				/*Gets file descriptors to access lcd RTFIFO */
  if (dev)
    lcdfd = open ("/dev/lcd1", O_WRONLY);
  else
    lcdfd = open ("/dev/lcd0", O_WRONLY);
  if (lcdfd < 0)
    return -1;

  return 0;
}				/*lcdopen */

int
lcdclose (void)
{				/*Releases lcd RTFIFO */
  int err;
  err = close (lcdfd);
  lcdfd = -1;			/*so no further I/O succeeds */
  return err;
}				/*lcdclose */

/*
   NOTE: A single jiffy (10ms on x86, ~0.98ms on Alpha) of delay is
   built in to the function by making the task sleep.
   (Though why a schedule() call works in RT-Linux escapes me)
   There seems to be no shorter delays possible without using
   a busy-wait loop with udelay()...a Bad Thing in RTOS code.
   Delay between chars must be at least 2ms if no busy-checking of
   the lcd is done, should a better, shorter delay method is found.
 */
int
lcdputc (unsigned char data)
{				/*Outputs one datum to lcd FIFO */
  int err;

  err = write (lcdfd, &data, 1);
  if (err != 1)
    return -1;
  return 0;
}				/*lcdputc */

/*
   see warning for lcdputc()
 */
int
lcdputbyte (unsigned int data)
{				/* outputs one datum to lcd FIFO */
  int err;
  char byteData, nibble;
  byteData = (char) data;
  nibble = ((byteData >> 4) & 0x0F);
  nibble = (nibble > 9) ? (nibble - 10 + 'A') : (nibble + '0');
  err = lcdputc (nibble);
  if (err)
    return err;
  nibble = (byteData & 0x0F);
  nibble = (nibble > 9) ? (nibble - 10 + 'A') : (nibble + '0');
  err = lcdputc (nibble);
  return err;
}

/*
 * Use this function to send a single byte to the
 * control register on the LCD controller.
 * This function is used primarily by other function
 * in this file. 
 */
int
lcdputi (unsigned char data)
{				/*Outputs one instruction to lcd FIFO */
  int err;
  err = ioctl (lcdfd, LCD_IOC_CTRL, &data);
  return err;
}				/*lcdputi */

/*
 * String length is limited to 255 chars.
 * This is a feature to prevent semi-endless loops which could happen with
 * erroneous data passed to the function.
 */
int
lcdputs (unsigned char *data)
{				/*Outputs a string of data to lcd */
  int err;
  unsigned char *ptr;
  err = 0;
  ptr = data;
  while (*ptr)
    {
      err = lcdputc (*ptr++);
      if (err)
	break;
    }
  return err;
}				/*lcdputs */

int
lcdclr (void)
{				/*Clears the lcd */
  int err;
  err = lcdputi (0x01);
  if (err != 0)
    return -1;
//  err = lcdputi (0x80);
  // if (err != 0)
//    return -1;
  return 0;
}				/*lcdclr */

int
lcdgoto (unsigned char row, unsigned char column)
{

  int err = -1;

  if (column <= 15)
    {
      err = 0;
      switch (row)
	{
	case 0:
	  lcdputi (0x80 + column);
	  break;
	case 1:
	  lcdputi (0xC0 + column);
	  break;
	default:
	  err = -1;
	  break;
	}
    }
  return err;
}				/*lcdgoto */

int
lcdscroll (unsigned char position)
{
  int err;
  while (position-- > 0)
    {
      err = lcdputi (0x18);	/* scroll left by 1 */
      if (err != 0)
	return -1;
    }
  return 0;
}				/*lcdscroll */

int
lcdinit (void)
{				/*Set up lcd...run me first after lcdopen() */
  int err;
  err = lcdputi (0x38);
  usleep (4500);
  if (err != 0)
    return err;
  err = lcdputi (0x38);
  usleep (4500);
  if (err != 0)
    return err;
  err = lcdputi (0x38);
  usleep (4500);
  if (err != 0)
    return err;

  err = lcdputi (0x0F);
  if (err != 0)
    return err;
  err = lcdputi (0x01);
  if (err != 0)
    return err;
  err = lcdputi (0x06);
  if (err != 0)
    return err;
  err = lcdputi (0x80);
  if (err != 0)
    return err;
  return 0;
}				/*lcdinit */
