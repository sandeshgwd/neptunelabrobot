/************************************************************
 (c) Applied AI Systems, Inc. 2003

************************************************************/

#include "debug.h"
#define _GNU_SOURCE
#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<sys/un.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include "simpleserver.h"

/** Maximun number of characters that can be read for a single command
 * including all parameters and null-terminator */
#define MAX_PACKET_SIZE 257
#define MAX_DATA_SIZE (MAX_PACKET_SIZE - 23)

int
simpleServer (int socketType, int clientfd, SSCommandTable * ssCmdTable)
{
  unsigned char command[3];
  struct sockaddr_in from, *toPtr;;
  socklen_t fromlen, tolen;
  int cmdIndex = 0;
  fd_set readfds;
  int connected;
  int result;
  ssize_t bytesRead;
  char *packet = NULL;
  int done = 1;
  int maxfd;
  int dataSize;
  int packetSize;
  char dataIn[MAX_DATA_SIZE];
  char dataOut[MAX_DATA_SIZE];
  unsigned int clientSource = 0, clientDestination = 0;
  struct timeval timeout;
  int (*callbackFunc) (unsigned char *, unsigned char *) = NULL;

  if ((clientfd < 0) || !ssCmdTable)
    return 1;

  if (socketType == SOCK_DGRAM)
    {
      toPtr = &from;
      tolen = sizeof (from);
    }
  else
    {
      toPtr = NULL;
      tolen = 0;
    }

  packet = malloc (MAX_PACKET_SIZE);
  if (packet)
    {
      cmdIndex = 0;
      while (ssCmdTable[cmdIndex].cmd)
	cmdIndex++;
      if (ssCmdTable[cmdIndex].func)
	callbackFunc = ssCmdTable[cmdIndex].func;

      done = 0;
      connected = 1;
      while (connected)
	{
	  FD_ZERO (&readfds);
	  FD_SET (clientfd, &readfds);
	  maxfd = clientfd;

	  timeout.tv_sec = 0;
	  timeout.tv_usec = 100000;
	  if ((result =
	       select (maxfd + 1, &readfds, NULL, NULL, &timeout)) < 0)
	    {
	      debug_perror (minVerbLevel (2), "processRequests: select");
	      if (errno == EINTR)
		{
		  continue;
		}
	      connected = 0;
	      done = 1;
	    }
	  else if (result)
	    {
	      if (FD_ISSET (clientfd, &readfds))
		{
		  bzero (packet, MAX_PACKET_SIZE);
		  fromlen = sizeof (from);
		  bytesRead =
		    recvfrom (clientfd, packet, MAX_PACKET_SIZE - 1, 0,
			      (struct sockaddr *) &from, &fromlen);
		  if (bytesRead < 0)
		    {
		      debug_perror (minVerbLevel (2),
				    "processRequests: read");
		      done = 1;
		      connected = 0;
		    }
		  else
		    {
		      if (!bytesRead)	/* if no data, but fd is ready, then its EOF */
			{
			  debug_perror (minVerbLevel (2), "No Data");
			  connected = 0;
			}
		      else
			{
			  // NULL terminate the packet to make sure string processing doesn't overflow
			  packet[bytesRead] = '\0';
			  debug_print (minVerbLevel (4),
				       "Received (%d bytes): %s\n", bytesRead,
				       packet);

			  // Parse the packet
			  dataSize = MAX_DATA_SIZE;
			  parsePacket (packet, bytesRead, command, dataIn,
				       &dataSize);

			  command[2] = '\0';
			  dataIn[dataSize] = '\0';
			  debug_print (minVerbLevel (4),
				       "command = %c%c, dataSize = %d, data = %s\n",
				       command[0], command[1], dataSize,
				       dataIn);

			  // Find a command that matches this request
			  cmdIndex = 0;
			  while ((ssCmdTable[cmdIndex].cmd)
				 &&
				 ((ssCmdTable[cmdIndex].cmd[0] !=
				   command[0])
				  || (ssCmdTable[cmdIndex].cmd[1] !=
				      command[1])))
			    {
			      cmdIndex++;
			    }

			  if (!ssCmdTable[cmdIndex].cmd)
			    {
			      continue;
			    }

			  // Check if command has a function to handle the request
			  if (ssCmdTable[cmdIndex].func)
			    {
			      // result is number of bytes in dataOut to send back to requestor
			      result =
				ssCmdTable[cmdIndex].func (dataIn, dataOut);

			      if (result >= 0)
				{
				  packetSize =
				    buildPacket (packet, MAX_PACKET_SIZE - 1,
						 command, dataOut, result);

				  if ((result = sendto
				       (clientfd, packet, packetSize, 0,
					(const struct sockaddr *) &from,
					fromlen)) != packetSize)
				    {
				      debug_perror (minVerbLevel (3),
						    "Error Sending response");
				    }
				}
			    }
			}
		    }
		}
	    }
	  // If no error so far, and we have a callback function, call it.
	  if ((result >= 0) && callbackFunc)
	    {
	      // result is number of bytes (>= 2) in dataOut to send back to requestor
	      // if result is 1, we disconnect, don't send any data.
	      // if result is 0, continue, don't send any data out
	      result = callbackFunc (NULL, dataOut);
	      if (result < 0)
		{
		  // disconnect since we didn't get a Health request within specified time
		  connected = 0;
		  result = 0;
		  done = 1;
		}
	      else if (result >= 2)
		{
		  debug_print (minVerbLevel (1),
			       "sending event notification\n");
		  command[0] = dataOut[0];
		  command[1] = dataOut[1];
		  packetSize =
		    buildPacket (packet, MAX_PACKET_SIZE - 1, command,
				 dataOut + 2, result - 2);
		  if ((result =
		       sendto (clientfd, packet, packetSize, 0,
			       (const struct sockaddr *) toPtr,
			       tolen)) != packetSize)
		    {
		      debug_perror (minVerbLevel (1),
				    "Error Sending response");
		      debug_print (minVerbLevel (1),
				   "error sending response\n");
		    }
		}
	    }

	  if (result < 0)
	    {
	      connected = 0;
	      done = -1;
	    }
	}
      debug_print (minVerbLevel (2), "Client disconnected\n");
      free (packet);
    }
  return (done);
}

int
createNewSocket (int type, char *host, unsigned int port)
{
  int create_socket = -1;
  struct sockaddr_in inetAddress;
  struct sockaddr *address = NULL;
  unsigned int uintParam;
  int addressSize = 0;

  if (port < 1024 || port > 65535)
    return (-1);

  if (type != SOCK_DGRAM && type != SOCK_STREAM)
    return (-1);

  if ((create_socket = socket (PF_INET, type, 0)) > 0)
    {
      int haveAddress;
      bzero (&inetAddress, sizeof (inetAddress));
      inetAddress.sin_family = AF_INET;
      inetAddress.sin_port = htons (port);

      haveAddress = 0;
      if (host)
	{
	  if (inet_pton (AF_INET, host, &inetAddress.sin_addr) > 0)
	    haveAddress = 1;
	}

      if (!haveAddress)
	inetAddress.sin_addr.s_addr = INADDR_ANY;	// our own IP
      address = (struct sockaddr *) &inetAddress;
      addressSize = sizeof (inetAddress);

      uintParam = 1;
      if (type == SOCK_DGRAM)
	{
	  if (setsockopt
	      (create_socket, SOL_SOCKET, SO_REUSEADDR, &uintParam,
	       sizeof (uintParam)) < 0)
	    {
	      debug_perror (minVerbLevel (2), "reuseaddr setsockopt");
	      close (create_socket);
	      return (-1);
	    }
	}
      if (bind (create_socket, address, addressSize) < 0)
	{
	  debug_perror (minVerbLevel (2), "bind");
	  close (create_socket);
	  return (-1);
	}
    }
  return (create_socket);
}
