/*
 */

#include <unistd.h>		/*usleep(), read(), write(), etc... */
#include <stdio.h>
#include <sys/types.h>		/* Next 3 for open() */
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include "usermotor.h"
#include "motor.h"

static int motorfd;		/* file descriptor */
int error;

int
initMotors ()
{
  motorfd = open ("/dev/motor", O_RDWR);
  if (motorfd < 0)
    {
      perror ("Could not open motor device");
      return (-1);
    }
  return (0);
}

void
closeMotors ()
{
  if (motorfd >= 0)
    {
      close (motorfd);
      motorfd = -1;
    }
  setMotorPower (0);
}

int
setMotorPower (int powerstate)
{
  return (ioctl (motorfd, MOTOR_SET_POWER, &powerstate));
}

int
getMotorPower ()
{
  int error, power;
  error = ioctl (motorfd, MOTOR_GET_POWER, &power);
  if (error)
    return (-1);
  else
    return (power);
}

int
getButtons ()
{
  int error, buttons;
  error = ioctl (motorfd, MOTOR_GET_BUTTONS, &buttons);
  if (error)
    return (-1);
  else
    return (buttons);
}

void
disableMotors ()
{
}

void
enableMotors ()
{
}

void
setBrakes (int brakeStatus)
{
  error = ioctl (motorfd, MOTOR_SET_BRAKES, &brakeStatus);
  if (error)
    {
      perror ("Set brakes failed");
    }
}

void
configureSpeedProfile (int maxSpeed, int accel)
{
}

void
setPositionCounter (unsigned int motorNum, int position)
{
}

void
setDesiredPosition (unsigned int motorNum, int position)
{
}

int
getPosition (unsigned int motorNum)
{
  return (0);
}

void
setMotorSpeed (unsigned int motorNum, int speed)
{
  ONEMOTOR temp;
  temp.motorNum = motorNum;
  temp.value = (int) speed;
  error = ioctl (motorfd, MOTOR_SET_SPEED, &temp);
  if (error)
    {
      perror ("Set single motor desired speed failed");
    }
}

int
getMotorSpeed (unsigned int motorNum)
{
  int speeds[4];
  error = ioctl (motorfd, MOTOR_GET_ALL_SPEED, speeds);
  if (error)
    {
      perror ("Get single motor actual speed failed");
    }
  return ((int) speeds[motorNum]);
}

int
setMotorSpeedAll (int *values)
{
  return (ioctl (motorfd, MOTOR_SET_ALL_SPEED, values));
}

int
getMotorSpeedAll (int *values)
{
  return (ioctl (motorfd, MOTOR_GET_ALL_SPEED, values));
}

int
getDesiredSpeedAll (int *values)
{
  return (ioctl (motorfd, MOTOR_GET_DESIRED_SPEED, values));
}


int
getDesiredSpeed (unsigned int motorNum)
{
  int speeds[4];
  error = ioctl (motorfd, MOTOR_GET_DESIRED_SPEED, speeds);
  if (error)
    {
      perror ("Get single motor desired speed failed");
    }
  return ((int) speeds[motorNum]);
}

int
getEncoderAll (unsigned int *values)
{
  return (ioctl (motorfd, MOTOR_GET_ENCODER, values));
}

void
configureSpeedPID (unsigned int motorNum, int Kp, int Ki, int Kd)
{
  PIDVAL temp;
  temp.motorNum = motorNum;
  temp.kp = Kp;
  temp.ki = Ki;
  temp.kd = Kd;
  error = ioctl (motorfd, MOTOR_SET_SPEED_PID, &temp);
  if (error)
    {
      perror ("Set speed PID failed");
    }
}

void
configurePositionPID (unsigned int motorNum, int Kp, int Ki, int Kd)
{
}
