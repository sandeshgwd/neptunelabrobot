#include <posix/posix.h>
#include <rtai/heap.h>
#include "sensors.h"
#include "labo2.h"

MODULE_AUTHOR ("Patrick Maheral");
MODULE_DESCRIPTION ("Sensor ADC Handler");
MODULE_LICENSE ("GPL");

static void *sensorsSharedMem;
static SENSOR *sensors;

/* the PWM sginal for IR sensor is 40khz on the 80 to 20 duty cycle of
 * 100hz~500hz. To make this PWM siganl, to use two timer on the 8254 timer and
 * counter.
 *
 * There is 2Mhz OSC on the interface board.
*/

static pthread_t thread;
static RT_HEAP sensor_heap;

void *
sensors_routine (void *arg)
{
  struct sched_param p;
  struct timespec st, prd;
  int setting_baseHz, setting_modHz;

  p.sched_priority = 1;
  st.tv_sec = 0;
  st.tv_nsec = 0;
  clock_settime (CLOCK_PROCESS_CPUTIME_ID, &st);
  prd.tv_sec = 0;
  prd.tv_nsec = 8000000;
  pthread_setschedparam (pthread_self (), SCHED_FIFO, &p);
  pthread_make_periodic_np (pthread_self (), &st, &prd);

  sensors->baseHz = setting_baseHz = 0x0034;	/* 0x034 = 38Khz */
  sensors->modHz = setting_modHz = 0x4E20;

  outb (0x36, PWM3 + 3);	/* PWM3+3 is control address */
  outb (setting_baseHz, PWM3);	/* counter 0 LSB */
  outb (0x00, PWM3);		/* counter 0 MSB */
  outb (0x16, PWM3 + 3);	/* PWM3+3 is control address */
  outb (setting_baseHz, PWM3);	/* counter 0 LSB */

  outb (0x76, PWM3 + 3);	/* out0 and out1 are for IR fq */
  outb ((setting_modHz & 0xFF), PWM3 + 1);	/* LSB for counter 1 */
  outb (((setting_modHz >> 8) & 0xFF), PWM3 + 1);	/* MSB for counter 1 */

  printk ("sensors: IR Base Frequency : %d\n", inb (PWM3));
  printk ("sensors: IR Mod Frequency  : %d\n",
	  inb (PWM3 + 1) + inb (PWM3 + 1) * 256);

  while (1)
    {
      if (setting_baseHz != sensors->baseHz)
	{
	  setting_baseHz = sensors->baseHz;
	  outb (setting_baseHz, PWM3);
	  printk ("sensors: Setting Base IR Frequency :%d\n", setting_baseHz);
	}
      if (setting_modHz != sensors->modHz)
	{
	  setting_modHz = sensors->modHz;
	  outb ((setting_modHz & 0xFF), PWM3 + 1);	/* LSB for counter 1 */
	  outb (((setting_modHz >> 8) & 0xFF), PWM3 + 1);	/* MSB for counter 1 */
	  printk ("sensors: Setting mod IR Frequency :%d\n", setting_modHz);
	}

      outb (0x08, ADC_1);	/* ir6-connector */
      outb (0x08, ADC_2);	/* ir4-connector */
      pthread_wait_np ();
      sensors->irs[5] = inb (ADC_1) & 0xFF;
      sensors->irs[3] = inb (ADC_2) & 0xFF;

      outb (0x09, ADC_1);	/* ir7-connector */
      outb (0x09, ADC_2);	/* ir5-connector */
      pthread_wait_np ();
      sensors->irs[6] = inb (ADC_1) & 0xFF;
      sensors->irs[4] = inb (ADC_2) & 0xFF;

      outb (0x0A, ADC_1);	/* ir8-connector */
      outb (0x0A, ADC_2);	/*  */
      pthread_wait_np ();
      sensors->irs[7] = inb (ADC_1) & 0xFF;
      sensors->motorCurrent[0] = inb (ADC_2) & 0xFF;

      outb (0x0B, ADC_1);	/* ir9-connector */
      outb (0x0B, ADC_2);	/*  */
      pthread_wait_np ();
      sensors->irs[8] = inb (ADC_1) & 0xFF;
      sensors->motorCurrent[1] = inb (ADC_2) & 0xFF;

      outb (0x0C, ADC_1);	/* ir10-connector */
      outb (0x0C, ADC_2);	/*  */
      pthread_wait_np ();
      sensors->irs[9] = inb (ADC_1) & 0xFF;
      sensors->motorCurrent[2] = inb (ADC_2) & 0xFF;

      outb (0x0D, ADC_1);	/* ir1-connector */
      outb (0x0D, ADC_2);	/*  */
      pthread_wait_np ();
      sensors->irs[0] = inb (ADC_1) & 0xFF;
      sensors->motorCurrent[3] = inb (ADC_2) & 0xFF;

      outb (0x0E, ADC_1);	/* ir2-connector */
      outb (0x0E, ADC_2);	/* ir11-connector */
      pthread_wait_np ();
      sensors->irs[1] = inb (ADC_1) & 0xFF;
      sensors->irs[10] = inb (ADC_2) & 0xFF;

      outb (0x0F, ADC_1);	/* ir3-connector */
      outb (0x0F, ADC_2);	/* ir12-connector */
      pthread_wait_np ();
      sensors->irs[2] = inb (ADC_1) & 0xFF;
      sensors->irs[11] = inb (ADC_2) & 0xFF;

      sensors->bumper = inb (BUMPER) & 0xFF;
    }
  return 0;
}

static int __init
sensors_init (void)
{
  int error = 0;
  size_t heapsize = sizeof (SENSOR);

  printk (KERN_INFO "sensors: loading\n");
  error = rt_heap_create (&sensor_heap, "SENSORHEAP", heapsize, H_SHARED);
  if (error != 0)
    {
      printk (KERN_WARNING "Heap creation failed %d\n", error);
      return (-1);
    }

  error = rt_heap_alloc (&sensor_heap, 0, TM_NONBLOCK, &sensorsSharedMem);
  if (error != 0)
    {
      printk (KERN_WARNING "Heap allocation failed %d\n", error);
      rt_heap_delete (&sensor_heap);
      return (-1);
    }
  sensors = (SENSOR *) sensorsSharedMem;

  return pthread_create (&thread, NULL, sensors_routine, 0);
}

static void __exit
sensors_exit (void)
{
  pthread_cancel (thread);
  rt_heap_delete (&sensor_heap);
  printk (KERN_INFO "Sensors unloaded\n");
}

module_init (sensors_init);
module_exit (sensors_exit);
