#include <unistd.h>		/*usleep(), read(), write(), etc... */
#include <stdio.h>
#include <sys/types.h>		/* Next 3 for open() */
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include "../include/userir.h"
#include "../include/sensors.h"

static int IRsensorfd = -1;	/* file descriptor */

int
initIR ()
{
  IRsensorfd = open ("/dev/sensors", O_RDWR);
  if (IRsensorfd < 0)
    {
      perror ("Could not open IR sensor device");
      return (-1);
    }
  return (0);
}

void
closeIR ()
{
  if (IRsensorfd >= 0)
    {
      close (IRsensorfd);
      IRsensorfd = -1;
    }
}

int
getIRs (int *irs)
{
  SENSOR sensors;
  int error, i;

  error = ioctl (IRsensorfd, IR_GET_SENSOR_DATA, &sensors);
  if (error == 0)
    {
      for (i = 0; i < 12; i++)
	irs[i] = sensors.irs[i];
    }
  return error;
}

int
getBumper ()
{
  SENSOR sensors;
  int error;

  error = ioctl (IRsensorfd, IR_GET_SENSOR_DATA, &sensors);
  if (error)
    return (-1);
  else
    return (sensors.bumper);
}

int
getMotorCurrent (int *motorCurrents)
{
  SENSOR sensors;
  int error, i;

  error = ioctl (IRsensorfd, IR_GET_SENSOR_DATA, &sensors);
  if (error == 0)
    {
      for (i = 0; i < 4; i++)
	motorCurrents[i] = sensors.motorCurrent[i];
    }
  return error;
}
