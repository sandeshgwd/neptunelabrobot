#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "debug.h"

#define MAX_PACKET_SIZE (256+1)
#define MAX_DATA_SIZE (MAX_PACKET_SIZE - 23)

int
buildPacket (unsigned char *packet, unsigned int maxPacketSize,
	     unsigned char *command, unsigned char *data,
	     unsigned int dataSize)
{
  int index;
  int i, result;
  unsigned int checksum;

  // Packet consists of:
  //  1 start byte (STX)
  //  2 byte command
  //    variable length data (depends on command)
  //  1 end byte (ETX)
  //  2 Checksum bytes
  //  1 terminating NULL character

  if (maxPacketSize < 7 + dataSize)
    return (-1);

  if (dataSize && !data)
    return (-1);

  index = 0;
  packet[0] = 0x02;		// STX
  packet[1] = command[0];
  packet[2] = command[1];
  index = 3;
  maxPacketSize -= 3;

  if (dataSize)
    {
      memcpy (packet + index, data, dataSize);
      index += dataSize;
      maxPacketSize -= dataSize;
    }

  if (maxPacketSize < 4)	// 1 byte ETX + 2 byte Checksum + terminating NULL
    return (-1);

  packet[index++] = 0x03;	// ETX
  maxPacketSize--;

  checksum = 0;
  for (i = 0; i < index; i++)	// start at STX, include data and ETX
    {
      checksum ^= packet[i];
    }

  result = snprintf (packet + index, maxPacketSize, "%02X", checksum & 0xFF);
  if (result < 0 || result >= maxPacketSize)
    return (-1);
  index += result;
  maxPacketSize -= result;

  return (index);
}

// NOTE: packetSize does not include terminating NULL
// packet must be NULL terminated
// dataSize is a value (max data size) and result (actual data size)
// data is converted from ascii (HEX) to binary data
// command is 2 bytes long
int
parsePacket (unsigned char *packet, unsigned int packetSize,
	     unsigned char *command, unsigned char *data,
	     unsigned int *dataSize)
{
  unsigned char *packetStart;
  unsigned char *packetEnd;
  unsigned char buffer[9];
  int i, result;
  unsigned int checksum;

  // Packet consists of:
  //  1 start byte (STX)
  //  2 byte command
  //    variable length data (depends on command)
  //  1 end byte (ETX)
  //  2 Checksum bytes
  //  1 terminating NULL character

  result = 0;
  if (!data || !packet)
    return (-1);

  packetStart = index (packet, 0x02);	// STX
  packetEnd = index (packet, 0x03);	// ETX

  if (!packetStart || !packetEnd)	// STX or ETX not found
    return (-1);

  debug_print (minVerbLevel (4), "packet = %p\n", packet);
  debug_print (minVerbLevel (4), " Start = %p (%d)\n", packetStart,
	       packetStart - packet);
  debug_print (minVerbLevel (4), "   End = %p (%d)\n", packetEnd,
	       packetEnd - packetStart);

  if (packet + packetSize < packetEnd + 3)	// not enough room after ETX for checksum and NULL
    return (-1);

  // calculate actual packet size
  packetSize = packetEnd - packetStart + 1 + 2;	// add an extra 2 for checksum

  debug_print (minVerbLevel (4), "Packet Size = %d\n", packetSize);

  // packet not long enough
  if (packetSize < 6)
    return -1;

  memcpy (buffer, packetEnd + 1, 2);
  buffer[2] = '\0';
  errno = 0;
  checksum = strtoul (buffer, NULL, 16);
  for (i = 0; i < packetSize - 2; i++)	// start at STX, include data and ETX
    {
      checksum ^= packetStart[i];
    }

  debug_print (minVerbLevel (4), "Checksum = %02X\n", checksum);

  command[0] = packetStart[1];
  command[1] = packetStart[2];
  debug_print (minVerbLevel (4), "command = %c%c\n", command[0], command[1]);

  // Make sure there is enough room, or truncate the data and indicate failure
  if (*dataSize < packetSize - 3 - 3)
    result = -1;
  else
    *dataSize = packetSize - 3 - 3;

  memcpy (data, packetStart + 3, *dataSize);

  if (checksum)
    {
      debug_print (minVerbLevel (4), "CHECKSUM ERROR\n");
      result = -1;
    }

  return (result);
}
