typedef struct SSCommandTable
{
  char *cmd;
  int cmdDataLength;
  int responseLength;
  int (*func) (unsigned char *, unsigned char *);
} SSCommandTable;

int simpleServer (int socketType, int clientfd, SSCommandTable * ssCmdTable);
