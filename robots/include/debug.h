#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#define DISPLAY_MESSAGES 1

#if DISPLAY_MESSAGES
#define debug_print(c,x...) do { if(c) fprintf(stderr, x); } while (0)
#define debug_perror(c,x) do { if(c) perror(x); } while (0)
#else
#define debug_print(l,x...) do { }  while (0)
#define debug_perror(l,x...) do { }  while (0)
#endif

#define printLine(x) debug_print (1, "FILE: %s LINE: %d\n", __FILE__, __LINE__)


static int minVerboseLevel = 2;
#define minVerbLevel(x) (x<minVerboseLevel)


#define DmpPGM2(f,w,h,m,p) do {\
	int fd;\
	char header[22];\
	if(m < 256) {\
		fd = open(f, O_WRONLY | O_CREAT, 0666);\
		if (fd){\
			snprintf (header, 22, "P5\n%d\n%d\n%d\n",w,h,m);\
			write(fd,header,strlen(header));\
			write(fd, p, w*h);\
			close(fd);\
		}\
	}\
} while(0)

#define DmpPGM(f,w,h,m,p) do {\
	int fd;\
	char header[22];\
	if(m < 256) {\
		fd = open(f, O_WRONLY | O_CREAT, 0666);\
		if (fd){\
			snprintf (header, 22, "P5\n%d\n%d\n%d\n",w,h,m);\
			write(fd,header,strlen(header));\
			for(j = 0; j < h; j++)\
				write(fd, p+j*w, w);\
			close(fd);\
		}\
	}\
} while(0)

#define DmpPPM(f,w,h,m,p) do {\
	int fd,i;\
	char header[22];\
	if(m < 256) {\
		fd = open(f, O_WRONLY | O_CREAT, 0666);\
		if (fd){\
			snprintf (header, 22, "P6\n%d\n%d\n%d\n",w,h,m);\
			write(fd,header,strlen(header));\
			for(i = 0; i < h; i++)\
				write(fd, p+i*w*3, w*3);\
			close(fd);\
		}\
	}\
} while(0)
#endif
