#ifndef USERIR_H
#define USERIR_H

#include "sensors.h"

int initIR ();
void closeIR ();
int getIRs (int *irs);
int getBumper ();
int getMotorCurrent (int *motorCurrents);

#endif
