#ifndef USERMOTOR_H
#define USERMOTOR_H

#include "motor.h"

int stop_motors ();
// MOTOR* init_motors(void);

int initMotors ();
void closeMotors ();
int setMotorPower (int powerstate);
int getMotorPower ();
void disableMotors ();
void enableMotors ();
void setBrakes (int brakeStatus);
void configureSpeedProfile (int maxSpeed, int accel);
void setPositionCounter (unsigned int motorNum, int position);
void setDesiredPosition (unsigned int motorNum, int position);
int getPosition (unsigned int motorNum);
void setMotorSpeed (unsigned int motorNum, int speed);
int getMotorSpeed (unsigned int motorNum);
int setMotorSpeedAll (int *values);
int getMotorSpeedAll (int *values);
int getDesiredSpeedAll (int *values);
int getDesiredSpeed (unsigned int motorNum);
int getEncoderAll (unsigned int *values);
void configureSpeedPID (unsigned int motorNum, int Kp, int Ki,
			int Kd);
void configurePositionPID (unsigned int motorNum, int Kp, int Ki,
			   int Kd);

#endif
