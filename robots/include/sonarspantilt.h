#ifndef SONARSPANTILT_H
#define SONARSPANTILT_H

/* 
 * this is sonars and pantilt data structure 
 */

typedef struct SON_S
{
  volatile int status;
  volatile int pan;
  volatile int tilt;
  volatile int sonars[8];
} SONARSPANTILT;

#endif
