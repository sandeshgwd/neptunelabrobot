#ifndef LCD_H
#define LCD_H

#define LCD_CTRL	0x2A4
#define LCD_DATA	0x2A5

#define LCD_IOC_ID 'Z'
#define LCD_IOC_BASE 0
#define LCD_IOC_INIT	_IO(LCD_IOC_ID, LCD_IOC_BASE+0)
#define LCD_IOC_CLEAR	_IO(LCD_IOC_ID, LCD_IOC_BASE+1)
#define LCD_IOC_HOME	_IO(LCD_IOC_ID, LCD_IOC_BASE+2)
#define LCD_IOC_GOTO	_IOW(LCD_IOC_ID, LCD_IOC_BASE+3, struct LCDPOS_S)
#define LCD_IOC_CTRL	_IOW(LCD_IOC_ID, LCD_IOC_BASE+4, char)

typedef struct LCDPOS_S
{
  unsigned char row;
  unsigned char col;
} LCDPOS;

#endif
