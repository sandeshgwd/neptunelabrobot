#ifndef MOTOR_H
#define MOTOR_H

#include <sys/ioctl.h>

#define NUM_MOTORS 4
#define SPEED 0
#define POSITION 1
#define ON 1
#define OFF 0

/* 
  bit0		brake0 for MOTOR1
  bit1		brake1 for MOTOR1
  bit2		brake0 for MOTOR2
  bit3		brake1 for MOTOR2
  bit4		direction0 for MOTOR1
  bit5		direction1 for MOTOR1
  bit6		direction0 for MOTOR2
  bit7		direction1 for MOTOR2
*/

typedef struct MOTOR_S
{
  volatile int flag;
  volatile int status;
  volatile int brake;
  volatile int desiredSpeed[NUM_MOTORS];
  volatile int actualSpeed[NUM_MOTORS];
  volatile int desiredPosition[NUM_MOTORS];
  volatile int actualPosition[NUM_MOTORS];
  volatile int controlType[NUM_MOTORS];	//position or speed control
  volatile int limit;		//arm has reached its limit
  volatile int power;		//toggle power to motor
  volatile int buttons;
}MOTOR;

typedef struct ONEMOTOR_S
{
  unsigned int motorNum;
  int value;
}
ONEMOTOR;

typedef struct PIDVAL_S
{
	unsigned int motorNum;
	int kp;
	int ki;
	int kd;
}
PIDVAL;

#define MOTOR_IOC_ID 'Z'
#define MOTOR_IOC_BASE 32
#define MOTOR_SET_ALL_SPEED    _IO(MOTOR_IOC_ID, MOTOR_IOC_BASE + 0)
#define MOTOR_SET_SPEED    _IOW(MOTOR_IOC_ID, MOTOR_IOC_BASE + 1, struct ONEMOTOR_S)
#define MOTOR_GET_ALL_SPEED    _IO(MOTOR_IOC_ID, MOTOR_IOC_BASE + 2)
#define MOTOR_GET_DESIRED_SPEED    _IOWR(MOTOR_IOC_ID, MOTOR_IOC_BASE + 3, struct ONEMOTOR_S)
#define MOTOR_SET_POWER    _IOW(MOTOR_IOC_ID, MOTOR_IOC_BASE + 4, struct ONEMOTOR_S)
#define MOTOR_SET_BRAKES	_IOW(MOTOR_IOC_ID, MOTOR_IOC_BASE + 5, struct ONEMOTOR_S)
#define MOTOR_GET_POWER    _IOR(MOTOR_IOC_ID, MOTOR_IOC_BASE + 6, int)
#define MOTOR_GET_BUTTONS    _IOR(MOTOR_IOC_ID, MOTOR_IOC_BASE + 7, int)
#define MOTOR_SET_SPEED_PID	_IOW(MOTOR_IOC_ID, MOTOR_IOC_BASE + 8, int)
#define MOTOR_GET_ENCODER    _IO(MOTOR_IOC_ID, MOTOR_IOC_BASE + 9)
#endif
