#ifndef AAIPACKET_H
#define AAIPACKET_H

int buildPacket (unsigned char *packet, unsigned int maxPacketSize, unsigned char *command, unsigned char *data, unsigned int dataSize);
int parsePacket (unsigned char *packet, unsigned int packetSize, unsigned char *command, unsigned char *data, unsigned int *dataSize);

#endif
