#ifndef ANALOG_MOTOR_H
#define ANALOG_MOTOR_H

#define NUM_MOTORS 4
#define SPEED 0
#define POSITION 1
#define ON 1
#define OFF 0

typedef struct ANALOG_MOTOR_S
{
  volatile int flag;
  volatile int status;
  volatile int command;
  volatile int desiredSpeed[NUM_MOTORS];
  volatile int actualSpeed[NUM_MOTORS];
  volatile int desiredPosition[NUM_MOTORS];
  volatile int actualPosition[NUM_MOTORS];
  volatile int controlType[NUM_MOTORS];	//position or speed control
  volatile int desiredY;
  volatile int actualY;
  volatile int desiredX;
  volatile int actualX;
}
ANALOG_MOTOR;

#endif
