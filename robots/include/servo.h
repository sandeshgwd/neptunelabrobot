#ifndef SERVO_H
#define SERVO_H

#define NUM_SERVOS 4

typedef struct SERVO_S
{
  volatile unsigned int desiredPosition[NUM_SERVOS];
  volatile unsigned int actualPosition[NUM_SERVOS];
} SERVO;

#endif
