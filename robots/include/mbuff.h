/*
 */

#include <stdlib.h>

typedef struct SharedMemStruct
{
  struct SharedMemStruct *prev;
  struct SharedMemStruct *next;
  char *name;
  int count;
  int size;
  void *mem;

}
SharedMem;

static SharedMem sharedMemHead = { NULL, NULL, NULL, 0, 0, NULL };

void *
mbuff_attach (char *name, int size)
{
  SharedMem *ptr, *prevPtr, *nextPtr;;

  if (sharedMemHead.next == NULL)
    sharedMemHead.next = &sharedMemHead;

  if (sharedMemHead.prev == NULL)
    sharedMemHead.prev = &sharedMemHead;

  ptr = sharedMemHead.next;

  while (ptr->name && strcmp (ptr->name, name))
    {
      ptr = ptr->next;
    }
  if (ptr->name)
    {
      ptr->count++;
      return (ptr->mem);
    }

  if (!ptr)
    {
      ptr = (SharedMem *) malloc (sizeof (SharedMem));
      if (!ptr)
	return (NULL);
    }

  ptr->name = (char *) malloc (strlen (name) + 1);
  strcpy (ptr->name, name);
  ptr->count = 1;
  ptr->size = size;
  ptr->mem = (void *) malloc (size);
  if (!ptr->mem)
    {
      free (ptr->name);
      free (ptr);
      return (NULL);
    }

  nextPtr = &sharedMemHead;
  prevPtr = sharedMemHead.prev;

  ptr->next = nextPtr;
  ptr->prev = prevPtr;
  prevPtr->next = ptr;
  nextPtr->prev = ptr;

  return (ptr->mem);
}

int
mbuff_detach (char *name, void *memPtr)
{
  SharedMem *ptr;
  int count;

  ptr = sharedMemHead.next;

  while (ptr->name && strcmp (ptr->name, name))
    {
      ptr = ptr->next;
    }
  if (!ptr->name)		/* not found */
    return (-1);

  count = -1;
  if (ptr->mem == memPtr)
    {
      if (ptr->count)
	{
	  ptr->prev->next = ptr->next;
	  ptr->next->prev = ptr->prev;
	  ptr->count--;
	  count = ptr->count;
	  if (!count)
	    {
	      free (ptr->mem);
	      free (ptr->name);
	      free (ptr);
	    }
	}
    }
  return (count);
}
