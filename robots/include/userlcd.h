#ifndef USERLCD_H
#define USERLCD_H

int lcdopen (int dev);
int lcdclose (void);
int lcdputc (const unsigned char data);
int lcdputbyte(const unsigned int data);
int lcdputi (const unsigned char data);
int lcdputs (const unsigned char *data);
int lcdclr (void);
int lcdgoto (const unsigned char row, const unsigned char column);
int lcdscroll (const unsigned char position);
int lcdinit (void);

#endif
