#ifndef SENSORS_H
#define SENSORS_H
/* I/O */
/* 
 * ir1		ch6(ADC_1)
 * ir2		ch7(ADC_1)
 * ir3		ch8(ADC_1)
 * ir4		ch1(ADC_2)
 * ir5		ch2(ADC_2)
 * ir6		ch1(ADC_1)
 * ir7		ch2(ADC_1)
 * ir8		ch3(ADC_1)
 * ir9		ch4(ADC_1)
 * ir10		ch5(ADC_1)
 * ir11		ch7(ADC_2)
 * ir12		ch8(ADC_2) 
 *
 * ch3(ADC_2), ch4(ADC_2) for current sensor in the MTL1
 * ch5(ADC_2), ch6(ADC_2) for current sensor in the MTL2
 */

typedef struct SEN_S
{
  int flag;
  int status;
  int command;
  int baseHz;		/* about 40khz */
  int modHz;		/* duty cycle of modulation */
  int irs[12];
  unsigned int bumper;
  int sonars[8];
  int motorCurrent[4];
} SENSOR;

#define IR_IOC_ID 'Z'
#define IR_IOC_BASE 64
#define IR_GET_SENSOR_DATA    _IO(IR_IOC_ID, IR_IOC_BASE+0)
#endif
