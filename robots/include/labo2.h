#ifndef ROBOT_H
#define ROBOT_H
/* labo2 header file */

/* I/O */

#define DAC_1		0x2A6
#define DAC_2		0x2A7
#define MOTOR_ENABLE	0x2A8	/* Reset "kill" switch circuit */
#define MOTOR_DISABLE	0x2AA	/* Software "kill" switch */

#if 0				/* Old settings based on vesta board extention bus pin-out */
#define DAC_1		0x2A6	/* option in the extention bus */
#define DAC_2		0x2AC	/* option in the extention bus */
#define MOTOR_EN	0x2AA	/* option in the extention bus */
#endif

#define ADC_1		0x2D0	/* cs3 for U14 */
#define ADC_2		0x2D4	/* cs4 for U13 */
#define BUMPER		0x2DC
#define MOTOR_OUT	0x2A0	/* motor control bits for 1 and 2 */
#define MOTOR_IN	0x2A2
#define MTL		0x2D8	/* control for clock and motor power relay */
				/* bit0 is clock, bit7 is motor power relay */
#define PWM0		0x2C0	/* set puls counter */
#define PWM1		0x2C4	/* set 1 puls counter (out0), set 1 PWM (out2) */
#define PWM2		0x2C8	/* set 3 PWM (out0, out1 and out2 */
#define PWM3		0x2CC	/* base fq for set ir and MOTOR PWM (out3)
				   ir_fq (out0 and out1)  */

#define DIRECTION_ADC	DAC_1
#define SPEED_ADC	DAC_2
#define RESET 0
#define SET 1
#define ACTIVE 1
#define INACTIVE -1

#include "sensors.h"
#include "motor.h"
#include "lcd.h"
#include "userlcd.h"
#include "sonarspantilt.h"

#endif
