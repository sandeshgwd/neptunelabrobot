/*
   Linux Optrex DMC-50097 LCD character display driver
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include "lcd.h"

#define LCD_uDELAY 70
#ifndef DEBUG
#define DEBUG 0
#endif

#if ((LINUX_VERSION_CODE > 0x020111) && defined(MODULE))
MODULE_AUTHOR ("Patrick Maheral");
MODULE_DESCRIPTION ("Simple 16x2 line LCD driver");
MODULE_LICENSE ("GPL");
MODULE_PARM (lcd_major, "i");
MODULE_PARM_DESC (lcd_major, "Sets major device number for LCD");
#endif

static unsigned int lcd_in_use = 0;
static int lcd_major = 60;
static int override = 0;

static void
lcd_init_hw (void)
{
  outb (0x38, LCD_CTRL);
  //current->state = TASK_INTERRUPTIBLE;
  udelay (50 * LCD_uDELAY);
  //schedule_timeout (1);
  outb (0x38, LCD_CTRL);
  //current->state = TASK_INTERRUPTIBLE;
  udelay (50 * LCD_uDELAY);
  //schedule_timeout (1);
  outb (0x38, LCD_CTRL);
  //current->state = TASK_INTERRUPTIBLE;
  udelay (50 * LCD_uDELAY);
  //schedule_timeout (1);
  outb (0x0F, LCD_CTRL);
  //current->state = TASK_INTERRUPTIBLE;
  udelay (50 * LCD_uDELAY);
  //schedule_timeout (1);
  outb (0x01, LCD_CTRL);
  //current->state = TASK_INTERRUPTIBLE;
  udelay (50 * LCD_uDELAY);
  //schedule_timeout (1);
  outb (0x06, LCD_CTRL);
  //current->state = TASK_INTERRUPTIBLE;
  udelay (50 * LCD_uDELAY);
  //schedule_timeout (1);
  outb (0x80, LCD_CTRL);
  return;
}

static int
lcd_open (struct inode *inode, struct file *file)
{
  if (!lcd_in_use)
    lcd_init_hw ();
  lcd_in_use++;
  if (MINOR (inode->i_rdev) == 1)
    override = 1;
  return 0;
}

static int
lcd_release (struct inode *inode, struct file *file)
{
//  outb (0x08, LCD_CTRL);      /* Turn off the LCD display */

  if (lcd_in_use)
    lcd_in_use--;
  if (MINOR (inode->i_rdev) == 1)
    override = 0;
  return 0;
}

static ssize_t
lcd_write (struct file *file, const char *userdata, size_t len, loff_t * ppos)
{
  unsigned char data;
  /*  Can't seek (pwrite) on this device  */
  if (ppos != &file->f_pos)
    {
//    return -ESPIPE;
    }
  if (override && !(file->f_dentry->d_inode->i_rdev))
    {
      return -EBUSY;
    }
  if (copy_from_user (&data, userdata, 1))
    {
      return (-EFAULT);
    }
  if (len)
    {
      current->state = TASK_INTERRUPTIBLE;
      schedule_timeout (1);
      udelay (50 * LCD_uDELAY);
      outb (data, LCD_DATA);
      return 1;
    }
  return 0;
}

static long lcd_ioctl (struct file *file, unsigned int command, unsigned long addr)
{
  int result = 0;

  if (override)//&& MINOR ((inode->i_rdev < 1)))
    return -EBUSY;

  switch (command)
    {
    case LCD_IOC_INIT:
      lcd_init_hw ();
#if DEBUG
      printk (KERN_WARNING "lcd: Received init IOC\n");
#endif
      break;
    case LCD_IOC_CLEAR:
      udelay (LCD_uDELAY);
      outb (0x01, LCD_CTRL);
//      current->state = TASK_INTERRUPTIBLE;
//      schedule_timeout (1);
#if DEBUG
      printk (KERN_WARNING "lcd: Received clear IOC\n");
#endif
      break;
    case LCD_IOC_HOME:
      udelay (LCD_uDELAY);
      outb (0x02, LCD_CTRL);
//      current->state = TASK_INTERRUPTIBLE;
//      schedule_timeout (1);
#if DEBUG
      printk (KERN_WARNING "lcd: Received home IOC\n");
#endif
      break;
    case LCD_IOC_GOTO:
      {
	int pos = 0;
	LCDPOS lcdPos;

#if DEBUG
	printk (KERN_WARNING "lcd: Received goto IOC\n");
#endif
	if (copy_from_user (&lcdPos, (LCDPOS *) addr, sizeof (LCDPOS)))
	  return (-EFAULT);

	/* Calculate the row address bits */
	if (lcdPos.row & 1)
	  pos = 0xC0;
	else
	  pos = 0x80;
	if (lcdPos.row & 2)
	  pos |= 0x10;

	/* Add in the column address bits */
	pos |= lcdPos.col & 0x0F;

	udelay (LCD_uDELAY);
	outb (pos, LCD_CTRL);
      }
      break;
    case LCD_IOC_CTRL:
      {
	unsigned char ctrlData;

	if (copy_from_user (&ctrlData, (char *) addr, sizeof (char)))
	  return (-EFAULT);

#if DEBUG
	printk (KERN_WARNING "lcd: Received ctrl IOC (%X)\n",
		(unsigned) ctrlData);
#endif
	udelay (LCD_uDELAY);
	outb (ctrlData, LCD_CTRL);
	if (ctrlData < 4)
	  {
	    udelay (LCD_uDELAY);
//          current->state = TASK_INTERRUPTIBLE;
//          schedule_timeout (1);
	  }
      }
      break;
    default:
#if DEBUG
      printk (KERN_WARNING "lcd: Received invalid IOC (%X)\n",
	      (unsigned) command);
#endif
      result = -EINVAL;
    }
  return result;
}

#if 0
truct file_operations
{
  struct module *owner;
    loff_t (*llseek) (struct file *, loff_t, int);
    ssize_t (*read) (struct file *, char __user *, size_t, loff_t *);
    ssize_t (*aio_read) (struct kiocb *, char __user *, size_t, loff_t);
    ssize_t (*write) (struct file *, const char __user *, size_t, loff_t *);
    ssize_t (*aio_write) (struct kiocb *, const char __user *, size_t,
			  loff_t);
  int (*readdir) (struct file *, void *, filldir_t);
  unsigned int (*poll) (struct file *, struct poll_table_struct *);
  int (*ioctl) (struct inode *, struct file *, unsigned int, unsigned long);
  int (*mmap) (struct file *, struct vm_area_struct *);
  int (*open) (struct inode *, struct file *);
  int (*flush) (struct file *);
  int (*release) (struct inode *, struct file *);
  int (*fsync) (struct file *, struct dentry *, int datasync);
  int (*aio_fsync) (struct kiocb *, int datasync);
  int (*fasync) (int, struct file *, int);
  int (*lock) (struct file *, int, struct file_lock *);
    ssize_t (*readv) (struct file *, const struct iovec *, unsigned long,
		      loff_t *);
    ssize_t (*writev) (struct file *, const struct iovec *, unsigned long,
		       loff_t *);
    ssize_t (*sendfile) (struct file *, loff_t *, size_t, read_actor_t,
			 void *);
    ssize_t (*sendpage) (struct file *, struct page *, int, size_t, loff_t *,
			 int);
  unsigned long (*get_unmapped_area) (struct file *, unsigned long,
				      unsigned long, unsigned long,
				      unsigned long);
  int (*check_flags) (int);
  int (*dir_notify) (struct file * filp, unsigned long arg);
  int (*flock) (struct file *, int, struct file_lock *);
};
#endif

static struct file_operations lcd_fops = {
#if 0
  NULL,				/*owner */
  NULL,				/*llseek */
  NULL,				/*read */
  NULL,				/*aio_read */
  lcd_write,			/*write */
  NULL,				/*aio_write */
  NULL,				/*readdir */
  NULL,				/*poll */
  lcd_ioctl,			/*ioctl */
  NULL,				/*mmap */
  lcd_open,			/*open */
  NULL,				/*flush */
  lcd_release,			/*release */
#else
  .read = NULL,			/* Read */
  .write = lcd_write,		/* Write */
  .unlocked_ioctl = lcd_ioctl,	/* Ioctl */
  .open = lcd_open,		/* Open */
  .release = lcd_release,	/* Close/Release */
#endif // Alternative implementation of file_operations
};

static int __init
lcd_init (void)
{
  int result;

  result = register_chrdev (lcd_major, "lcd", &lcd_fops);
  if (result < 0)
    {
#if DEBUG
      printk (KERN_WARNING "lcd: can't get major %d\n", lcd_major);
#endif
      return result;
    }
  if (lcd_major == 0)
    lcd_major = result;

  override = 0;
  lcd_in_use = 0;

  return 0;
}

static void __exit
lcd_exit (void)
{
  unregister_chrdev (lcd_major, "lcd");
}

module_init (lcd_init);
module_exit (lcd_exit);
