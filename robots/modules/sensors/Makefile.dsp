ifndef ROBOT_HOME
ROBOT_HOME = $(HOME)/robots
endif

ROBOT_INCLUDE = -I$(ROBOT_HOME)/include -I/usr/include

EXTRA_CFLAGS += $(shell rtai-config --module-cflags) $(ROBOT_INCLUDE)

obj-m += sensor.o

sensor-objs := sensors.o
