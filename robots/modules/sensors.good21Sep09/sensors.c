#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/ioport.h>
#include <linux/highmem.h>
//#include <asm/io.h>

#include <rtai.h>
#include <rtai_fifos.h>
#include <rtai_sched.h>
#include "sensors.h"
#include "labo2.h"


MODULE_LICENSE("GPL");

#define TIMER_TO_CPU 3 // < 0 || > 1 to maintain a symmetric processed timer.

#define TICK_PERIOD 25000  //8000000   /*125Hz*/ //25000	/* 40 khz */
#define DIVISOR 5

#define RUNNABLE_ON_CPUS 3  // 1: on cpu 0 only, 2: on cpu 1 only, 3: on any.
#define RUN_ON_CPUS (num_online_cpus() > 1 ? RUNNABLE_ON_CPUS : 1)

#define STACK_SIZE 4000

static RT_TASK thread;


static SENSOR sensors;

/* the PWM sginal for IR sensor is 40khz on the 80 to 20 duty cycle of
 * 100hz~500hz. To make this PWM signal, to use two timer on the 8254 timer and
 * counter.
 *
 * There is 2Mhz OSC on the interface board.
*/

int irs_in_use = 0;
int IR_major = 62;

void
initSW (void)
{
}

static void sensors_routine(long t)
{
	char data, temp;
	int go=0;
	int divisor = DIVISOR;
 	int setting_baseHz, setting_modHz;
	
	sensors.baseHz = setting_baseHz = 0x0034;     /* 0x034 = 38Khz */
  	sensors.modHz = setting_modHz = 0x4E20;

  	outb (0x36, PWM3 + 3);        /* PWM3+3 is control address */
  	outb (setting_baseHz, PWM3);  /* counter 0 LSB */
  	outb (0x00, PWM3);            /* counter 0 MSB */
  	outb (0x16, PWM3 + 3);        /* PWM3+3 is control address */
  	outb (setting_baseHz, PWM3);  /* counter 0 LSB */

  	outb (0x76, PWM3 + 3);        /* out0 and out1 are for IR fq */
  	outb ((setting_modHz & 0xFF), PWM3 + 1);      /* LSB for counter 1 */
  	outb (((setting_modHz >> 8) & 0xFF), PWM3 + 1);  /* MSB for counter 1 */

  	printk ("sensors: IR Base Frequency : %d\n", inb (PWM3));
  	printk ("sensors: IR Mod Frequency  : %d\n",
        inb (PWM3 + 1) + inb (PWM3 + 1) * 256);

	while(1) {
		

		rt_task_wait_period();
		rt_task_wait_period();
     		if (setting_baseHz != sensors.baseHz){
          		setting_baseHz = sensors.baseHz;
          		outb (setting_baseHz, PWM3);
          		printk ("sensors: Setting Base IR Frequency :%d\n", setting_baseHz);
        	}
      		if (setting_modHz != sensors.modHz){
          		setting_modHz = sensors.modHz;
          		outb ((setting_modHz & 0xFF), PWM3 + 1);      /* LSB for counter 1 */
          		outb (((setting_modHz >> 8) & 0xFF), PWM3 + 1);  /* MSB for count
er 1 */
          		printk ("sensors: Setting mod IR Frequency :%d\n", setting_modHz);
        	}
      		outb (0x08, ADC_1);       /* ir6-connector */
      		outb (0x08, ADC_2);       /* ir4-connector */
		rt_task_wait_period();
		rt_task_wait_period();
      		sensors.irs[5] = inb (ADC_1) & 0xFF;
      		sensors.irs[3] = inb (ADC_2) & 0xFF;
		
		rt_task_wait_period();
		rt_task_wait_period();
		outb (0x09, ADC_1);       /* ir7-connector */
      		outb (0x09, ADC_2);       /* ir5-connector */
		rt_task_wait_period();
		rt_task_wait_period();
      		sensors.irs[6] = inb (ADC_1) & 0xFF;
      		sensors.irs[4] = inb (ADC_2) & 0xFF;

		rt_task_wait_period();
		rt_task_wait_period();
      		outb (0x0A, ADC_1);       /* ir8-connector */
      		outb (0x0A, ADC_2);       /*  */
		rt_task_wait_period();
		rt_task_wait_period();
      		sensors.irs[7] = inb (ADC_1) & 0xFF;
//      	sensors.motorCurrent[0] = inb (ADC_2) & 0xFF;

		rt_task_wait_period();
		rt_task_wait_period();
      		outb (0x0B, ADC_1);       /* ir9-connector */
      		outb (0x0B, ADC_2);       /*  */
		rt_task_wait_period();
		rt_task_wait_period();
      		sensors.irs[8] = inb (ADC_1) & 0xFF;
//      	sensors.motorCurrent[1] = inb (ADC_2) & 0xFF;

		rt_task_wait_period();
		rt_task_wait_period();
      		outb (0x0C, ADC_1);       /* ir10-connector */
      		outb (0x0C, ADC_2);       /*  */
		rt_task_wait_period();
		rt_task_wait_period();
      		sensors.irs[9] = inb (ADC_1) & 0xFF;
      		sensors.motorCurrent[2] = inb (ADC_2) & 0xFF;

		rt_task_wait_period();
		rt_task_wait_period();
      		outb (0x0D, ADC_1);       /* ir1-connector */
      		outb (0x0D, ADC_2);       /*  */
		rt_task_wait_period();
		rt_task_wait_period();
      		sensors.irs[0] = inb (ADC_1) & 0xFF;
      		sensors.motorCurrent[3] = inb (ADC_2) & 0xFF;

		rt_task_wait_period();
		rt_task_wait_period();
      		outb (0x0E, ADC_1);       /* ir2-connector */
      		outb (0x0E, ADC_2);       /* ir11-connector */
		rt_task_wait_period();
		rt_task_wait_period();
	      	sensors.irs[1] = inb (ADC_1) & 0xFF;
      		sensors.irs[10] = inb (ADC_2) & 0xFF;

		rt_task_wait_period();
		rt_task_wait_period();
      		outb (0x0F, ADC_1);       /* ir3-connector */
      		outb (0x0F, ADC_2);       /* ir12-connector */
		rt_task_wait_period();
		rt_task_wait_period();
      		sensors.irs[2] = inb (ADC_1) & 0xFF;
      		sensors.irs[11] = inb (ADC_2) & 0xFF;
      		sensors.bumper = inb (BUMPER) & 0xFF;
	}
}

static int IR_open (struct inode *inode, struct file *file)
{
  if (!irs_in_use)
    {
      initSW ();
    }
  else
    return (-EBUSY);

  irs_in_use++;
  return 0;
}

static int IR_release (struct inode *inode, struct file *file)
{
  if (irs_in_use)
    {
      irs_in_use--;
      initSW ();
    }
  return 0;
}

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
static int IR_ioctl (struct inode *inode, struct file *file, unsigned int command, unsigned long addr)
#else
static long IR_ioctl (struct file *file, unsigned int command, unsigned long addr)
#endif
{
  int result = 0;
  switch (command)
    {
    case IR_GET_SENSOR_DATA:
#if DEBUG
      printk (KERN_WARNING "IR: Received IR data request\n");
#endif

      if (copy_to_user ((void __user *) addr, &sensors, sizeof (sensors)))
        return (-EFAULT);
      break;

    default:
#if DEBUG
      printk (KERN_WARNING "IR: Received invalid IOC (%X)\n",
              (unsigned) command);
#endif
      result = -EINVAL;
    }
  return result;
}

static struct file_operations IR_fops = {
NULL,NULL,
  #if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
    .ioctl = IR_ioctl,
  #else
    .unlocked_ioctl = IR_ioctl,
  #endif
  .open = IR_open,
  .release = IR_release,
};

int init_module(void)
{
	RTIME now, tick_period;
	int result = 0;
#if DEBUG
  	printk (KERN_WARNING "IR: loading\n");
#endif

  	result = register_chrdev (IR_major, "sensors", &IR_fops);
  	if (result < 0)
    	{
      		printk (KERN_INFO "IR sensors: register_chrdev failed (%d)\n", result);
      		goto release_and_exit;
    	}
  	if (IR_major == 0)
    		IR_major = result;
  	printk (KERN_INFO "IR sensors: registered device as major %d\n", IR_major);
  	irs_in_use = 0;

	result = rt_task_init(&thread, sensors_routine, 0, STACK_SIZE, 0, 0, 0);
  	if (result){
    		printk (KERN_INFO "IR_sensors: pthread_create failed (%d)\n", result);
	goto release_and_exit;
	}
	rt_set_runnable_on_cpus(&thread, RUN_ON_CPUS);
//#ifdef ONESHOT
//	rt_set_oneshot_mode();
//#endif
	rt_assign_irq_to_cpu(TIMER_8254_IRQ, TIMER_TO_CPU);/// activated nov16
	tick_period = start_rt_timer(nano2count(TICK_PERIOD));
	now = rt_get_time() + 10*tick_period;
	rt_task_make_periodic(&thread, now, tick_period);

release_and_exit:
  return (result);

}


void cleanup_module(void)
{
	int cpuid;
//	rt_reset_irq_to_sym_mode(TIMER_8254_IRQ);
	stop_rt_timer();
	rt_busy_sleep(10000000);
	rt_task_delete(&thread);
  	unregister_chrdev (IR_major, "sensors");
  	printk (KERN_INFO "Sensors unloaded\n");

}


