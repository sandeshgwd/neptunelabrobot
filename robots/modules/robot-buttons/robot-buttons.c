#include <linux/module.h>
#include <rtai.h>
#include <rtai_sched.h>
#include "labo2.h"

MODULE_AUTHOR ("Patrick Maheral & Giles Babin");
MODULE_DESCRIPTION ("2 Button monitor for AAI robots");
MODULE_LICENSE ("GPL");

#define BUTTON_HOLD_TIME 18000	/* 3 seconds */
#define SHUTDOWN 1
#define DEBUG 0

#define TIMER_TO_CPU 3 // < 0 || > 1 to maintain a symmetric processed timer.

#define TICK_PERIOD 170000   //100000000   //       25000 /* 40 khz */

#define RUNNABLE_ON_CPUS 3  // 1: on cpu 0 only, 2: on cpu 1 only, 3: on any.
#define RUN_ON_CPUS (num_online_cpus() > 1 ? RUNNABLE_ON_CPUS : 1)

#define STACK_SIZE 4000

static RT_TASK thread;


void *
powerButton (void *arg)
{
  unsigned int powerState = BUTTON_HOLD_TIME;
  int i = 1;
  int buttonState;		/* internal temp var */
  powerState = BUTTON_HOLD_TIME;
  while (1)
    {
      rt_task_wait_period();
      buttonState = inb (MOTOR_IN) & 0xC0;	/*Read button status */
      if (!buttonState)
	{
	  powerState--;
	  outb (0x00, MOTOR_OUT);	/* brake on */
#if DEBUG
	  printk (KERN_WARNING "Button Pressed\n");
#endif
	}
      else
	powerState = BUTTON_HOLD_TIME;

      if (!powerState)
	{
	  powerState = BUTTON_HOLD_TIME;
	  outb (0x00, MOTOR_OUT);	/* brake on */
	  /* Shutdown */
	  printk (KERN_WARNING "Button: Time to shut down\n");
#if SHUTDOWN
	  /* Tell init that ctrl-alt-del was pressed to start a shutdown */
	 if (find_task_by_pid(1)){
	    printk (KERN_WARNING "task with pid 1 found\n"); 
	    kill_cad_pid(SIGINT, 1);
         }
#endif
	}
    }
  return 0;
}

int init_module(void)
{
  RTIME now, tick_period;
  int result = 0;

  result = rt_task_init(&thread, powerButton, 0, STACK_SIZE, 0, 0, 0);
  rt_set_oneshot_mode();
  start_rt_timer(0);
  rt_task_make_periodic_relative_ns(&thread, 10000000, TICK_PERIOD);

  return (result);

}
void cleanup_module(void)
{
        stop_rt_timer();
        rt_busy_sleep(10000000);
        rt_task_delete(&thread);
}
