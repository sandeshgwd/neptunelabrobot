/*
 */

#include <linux/kernel.h>
#include <linux/ioport.h>
#include <linux/mc146818rtc.h>
#include <rtl_fifo.h>
#include <rtl_core.h>
#include <rtl_time.h>
#include <rtl.h>
#include <time.h>
#include <pthread.h>
#include "mbuff.h"
#include "labo2.h"
#include "servo.h"

MODULE_AUTHOR ("Patrick Maheral");
MODULE_DESCRIPTION ("Servo motor controller");
//MODULE_LICENSE("GPL");

#define PWM_PERIOD 0x9BFF
#define PWM_SCALE 8
#define PWM_OFFSET 1400		/* 700 us */
#define POSITION_MIN 10
#define POSITION_MAX 246
#define STEP_SIZE 5

struct motordata
{
  /* hardware addresses */
  unsigned int counterAddr;	/* address for motor counter */
  unsigned int PWMAddr;		/* address for motor PWM */
};

static void initHW (void);

static struct motordata mot[NUM_SERVOS];
static SERVO *servo;
static pthread_t thread;

/* the contents of shared memory may change at any time, thus volatile */
static volatile char *servoSharedMem;

/* The PWM signals are located on PWM1-counter2, PWM2-counter0,
 * PWM2-counter1 and PWM2-counter2.
 * PWM1 and PWM2 are 82C54 address and defined in the labo2.h.
 * counter2 on PWM1 is left front motor.
 * counter0 on PWM2 is left back motor.
 * counter1 on PWM2 is right front motor.
 * counter2 on PWM2 is right back motor.
 * Those counters are set as mode 1 (One-shot).
 *
 * The base pwm frequency is generated by counter2 on PWM3.
 * counter2 on PWM3 is set as mode 2 (rate generator).
 */
void
initHW ()
{
  /* Setup programable counter 82C54 for motor PWM signal */
  /* setting counter2 on PWM3 as mode 2 */
  outb (0xB4, PWM3 + 3);	/* select counter 2, LSM, mode 2,binary */
  outb ((PWM_PERIOD & 0xFF), PWM3 + 2);	/* select counter 2, LSM, mode 2,binary */
  outb (((PWM_PERIOD >> 8) & 0xFF), PWM3 + 2);	/* select counter 2, LSM, mode 2,binary */

  /* setting each motor PWM */
  outb (0xB2, PWM1 + 3);	/* select counter 2, LSM, mode 1, binary */
  outb ((PWM_PERIOD & 0xFF), PWM1 + 2);	/* counter rate for STOP */
  outb (((PWM_PERIOD >> 8) & 0xFF), PWM1 + 2);	/* counter rate for STOP */

  outb (0x32, PWM2 + 3);	/* select counter 0, LSM, mode 1, binary */
  outb ((PWM_PERIOD & 0xFF), PWM2 + 0);	/* counter rate for STOP */
  outb (((PWM_PERIOD >> 8) & 0xFF), PWM2 + 0);	/* counter rate for STOP */

  outb (0x72, PWM2 + 3);	/* select counter 1, LSM, mode 1, binary */
  outb ((PWM_PERIOD & 0xFF), PWM2 + 1);	/* counter rate for STOP */
  outb (((PWM_PERIOD >> 8) & 0xFF), PWM2 + 1);	/* counter rate for STOP */

  outb (0xB2, PWM2 + 3);	/* select counter 2, LSM, mode 1, binary */
  outb ((PWM_PERIOD & 0xFF), PWM2 + 2);	/* counter rate for STOP */
  outb (((PWM_PERIOD >> 8) & 0xFF), PWM2 + 2);	/* counter rate for STOP */

  /*Position encoder counters */
  outb (0x30, PWM0 + 3);	/*Set up COUNTER0 */
  outb (0xFF, PWM0 + 0);	/*load LSB */
  outb (0xFF, PWM0 + 0);	/*load MSB..total = 65535 */

  outb (0x70, PWM0 + 3);	/*Set up COUNTER1 */
  outb (0xFF, PWM0 + 1);	/*load LSB */
  outb (0xFF, PWM0 + 1);	/*load MSB..total = 65535 */

  outb (0xB0, PWM0 + 3);	/*Set up COUNTER2 */
  outb (0xFF, PWM0 + 2);	/*load LSB */
  outb (0xFF, PWM0 + 2);	/*load MSB..total = 65535 */

  outb (0x30, PWM1 + 3);	/*Set up COUNTER3 */
  outb (0xFF, PWM1 + 0);	/*load LSB */
  outb (0xFF, PWM1 + 0);	/*load MSB..total = 65535 */

  outb (0x0F, MTL);		//turn off motor power (should be off already)
}

/*
 * This function should get called before any use of
 * global data.
 * Be sure to update this if you alter the motordata struct
 * The values here should stop the robot for safety reasons
 */
void
initSW (void)
{				/* Sets up the parameters */

#if NUM_SERVOS > 0
  mot[0].counterAddr = PWM0 + 0;
  mot[0].PWMAddr = PWM1 + 2;
#endif

#if NUM_SERVOS > 1
  mot[1].counterAddr = PWM0 + 1;
  mot[1].PWMAddr = PWM2 + 0;
#endif

#if NUM_SERVOS > 2
  mot[2].counterAddr = PWM0 + 2;
  mot[2].PWMAddr = PWM2 + 1;
#endif

#if NUM_SERVOS > 3
  mot[3].counterAddr = PWM1 + 0;
  mot[3].PWMAddr = PWM2 + 2;
#endif

}				/*initSW */

void *
servo_routine (void *arg)
{
  struct sched_param p;
  unsigned int actualPosition[NUM_SERVOS];
  unsigned int desiredPosition[NUM_SERVOS];
  unsigned int position;
  unsigned int i;

  p.sched_priority = 1;
  pthread_setschedparam (pthread_self (), SCHED_FIFO, &p);

  for (i = 0; i < NUM_SERVOS; i++)
    {
      actualPosition[i] = servo->actualPosition[i] = 128;
      desiredPosition[i] = servo->desiredPosition[i] = 128;
    }

  pthread_make_periodic_np (pthread_self (), gethrtime (), 20000000);
/*
  pthread_make_periodic_np (pthread_self (), gethrtime (), 2000000000);
*/

  while (1)
    {
      pthread_wait_np ();

/*
      rtl_printf ("SERVO:");
*/
      for (i = 0; i < 4; i++)
	{
	  desiredPosition[i] = servo->desiredPosition[i];
	  if (actualPosition[i] < POSITION_MIN)
	    actualPosition[i] = POSITION_MIN;
	  else if (actualPosition[i] > POSITION_MAX)
	    actualPosition[i] = POSITION_MAX;
	  else if (abs (actualPosition[i] - desiredPosition[i]) > STEP_SIZE)
	    {
	      if (actualPosition[i] > servo->desiredPosition[i])
		actualPosition[i] -= STEP_SIZE;
	      else
		actualPosition[i] += STEP_SIZE;
	    }
	  else
	    actualPosition[i] = desiredPosition[i];

	  position =
	    PWM_PERIOD - (actualPosition[i] * PWM_SCALE + PWM_OFFSET);
	  outb (position & 0xFF, mot[i].PWMAddr);	/* counter rate */
	  outb ((position >> 8) & 0xFF, mot[i].PWMAddr);	/* counter rate */

	  servo->actualPosition[i] = actualPosition[i];
/*
	  rtl_printf (" %04x (%04x)", actualPosition[i], position);
*/
	}
/*
      rtl_printf ("\n");
*/
    }
  return 0;
}

/*
 * init_module tells the kernel how to start
 * the module and it initializes the counters
 */
int
init_module (void)
{
  int err;

  if ((err = check_region (PWM0, 12)) < 0)
    return err;

  request_region (PWM0, 12, "servo");

  printk (KERN_INFO "servo: Revision \n");

  servoSharedMem = (volatile char *) mbuff_alloc ("servo", sizeof (SERVO));
  if (servoSharedMem == NULL)
    {
      rtl_printf ("servo: mbuff_alloc failed\n");
      return (-1);
    }
  servo = (SERVO *) servoSharedMem;

  /* Initialize the hardware */
  initHW ();

  /* Initialize the software --- data structures */
  initSW ();

  return pthread_create (&thread, NULL, servo_routine, 0);
}

void
cleanup_module (void)
{
  outb (0x00, MOTOR_OUT);	/* free run */
  outb (0xFF, PWM1 + 2);	/* counter rate for STOP */
  outb (0xFF, PWM2 + 0);	/* counter rate for STOP */
  outb (0xFF, PWM2 + 1);	/* counter rate for STOP */
  outb (0xFF, PWM2 + 2);	/* counter rate for STOP */

  outb (0x0F, MTL);		//turn off Motor Relay

  release_region (PWM0, 12);

  pthread_delete_np (thread);
  mbuff_free ("servo", (void *) servoSharedMem);
}
