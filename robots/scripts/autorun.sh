#!/bin/sh
#
# This script will be called *after* all the other init scripts by
# /etc/rc.local.
# BE CAREFUL!  This script and all programs in it are run as user
# robot using "sudo".  This may be a security issue if sudo or
# kernel level bugs are exploited

/home/robot/avoid.new > /dev/null 2>& 1 &
(while(true); do /home/robot/calibrate > /dev/null 2>& 1 ; done ) &
