/*
 * Uses sonar (ultrasonic) sensors to avoid collisions
 * By Cameron Morland, 2001-06-12
 */
#include <stdio.h>
#include <signal.h>
#include "sonar.h"
#include "userlcd.h"
#include "usermotor.h"

/* Function Prototype for our signal handler */
void sigHandler (int dummy);

/* for distance ranges */
#define COLLIDE 0x10
#define NEAR    0x28
#define MEDIUM  0x30
#define FAR     0x28

#define SCALE   3
#define MAX_SPEED 15

int
main (void)
{
  signed int Lspeed, Rspeed;
  signed int desiredSpeeds[4];
  int i;
  int c = 0;
  int sonar;			/* sonar serial device */
  unsigned char buffer[8];	/* space for the raw sonar readings */
  int power, enableMotorPower;
  int buttons;
  int buttonHoldTime;
  int sonarTimeOut = 20;
  unsigned int sonars[8];
  unsigned int fuz_sonar[8];
  unsigned int ramp_rate[8] = {
    0, 8, 8,
    4,
    6, 6, 6,
    4
  };

/*
 *               5
 *            _________
 *      6  _-~         ~-_ 4
 *      _-~               ~-_
 *     .                     . 
 *  7 .                       . 3
 *   .                         . 
 *  .                           .
 *  |                           |
 *  |           GAIA2           |
 *  |                           |
 *  |                           |
 *  |                           |
 *  |                           |
 *  |                           |
 *  |                           |
 *  +---------------------------+
 *      2                 1
 *
 */

  signed int lcoef[8] = {
    0,				/* unused, there is no US zero */
    -5,				/* back right */
    -5,				/* back left */
    8,				/* front far right */
    5,				/* front near right */
    12,				/* front */
    -7,				/* front near left */
    -8				/* front far left */
  };

  signed int rcoef[8];

  /* mirror images */
  rcoef[1] = lcoef[2];
  rcoef[2] = lcoef[1];
  rcoef[3] = lcoef[7];
  rcoef[4] = lcoef[6];
  rcoef[5] = lcoef[5];
  rcoef[6] = lcoef[4];
  rcoef[7] = lcoef[3];

  for (i = 0; i < 8; i++)
    sonars[i] = 0;

  lcdopen (0);
  lcdclr ();
  lcdputs ("Starting in 10s");
  sleep (5);
  lcdclr ();
  lcdputs ("Starting in 5s");
  sleep (5);
  lcdclr ();

  if (initMotors ())
    exit (-1);

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  sonar = openPort (NULL);
  setBrakes (0);

  buffer[0] = 1;
  buffer[1] = 2;
  buffer[2] = 3;
  buffer[3] = 4;
  buffer[4] = 5;
  buffer[5] = 6;
  buffer[6] = 7;
  buffer[7] = 8;

  setMotorPower (0);

  power = getMotorPower ();
  buttons = getButtons ();
  fprintf (stderr, "Motor Power is %d\n", power);
  fprintf (stderr, "Motor Buttons is %d\n", buttons);

  buttonHoldTime = 0;
  enableMotorPower = 0;

  lcdclr ();
  while (1)
    {
      power = getMotorPower ();
      if (OFF == power)
	{
	  buttons = getButtons ();
	  lcdgoto (0, 0);
	  lcdputc (buttons + 'A');

	  if ((1 == buttons) || (2 == buttons))
	    {
	      if (buttonHoldTime > 10)
		{
		  enableMotorPower = 1;
		}
	      else
		buttonHoldTime++;
	    }
	  else
	    buttonHoldTime = 0;
	  if (!buttons && enableMotorPower)
	    {
	      setMotorPower (1);
	      enableMotorPower = 0;
	    }
	}

      /* read and display sensors */
      if (0 == readSonar (sonar, buffer))
	{
	  sonarTimeOut = 20;
	  for (i = 1; i < 8; i++)
	    {
	      if (buffer[i] < sonars[i])
		sonars[i] = buffer[i];
	      else if (sonars[i] <= 256 - ramp_rate[i])
		sonars[i] += ramp_rate[i];

	      if (sonars[i] < COLLIDE)
		fuz_sonar[i] = COLLIDE;
	      else if (sonars[i] > FAR)
		fuz_sonar[i] = FAR;
	      else
		fuz_sonar[i] = sonars[i];
	    }

	  Lspeed = 20 << SCALE;
	  Rspeed = 20 << SCALE;

	  for (i = 1; i < 8; i++)
	    {
	      Lspeed += lcoef[i] * fuz_sonar[i];
	      Rspeed += rcoef[i] * fuz_sonar[i];
	    }

	  if (c++ >= 10)
	    {
	      /* display sonar values for debug */

	      lcdgoto (0, 1);
	      lcdputbyte (fuz_sonar[7]);
	      lcdgoto (0, 4);
	      lcdputbyte (fuz_sonar[6]);
	      lcdgoto (0, 7);
	      lcdputbyte (fuz_sonar[5]);
	      lcdgoto (0, 10);
	      lcdputbyte (fuz_sonar[4]);
	      lcdgoto (0, 13);
	      lcdputbyte (fuz_sonar[3]);
	      lcdgoto (1, 5);
	      lcdputbyte (fuz_sonar[2]);
	      lcdgoto (1, 9);
	      lcdputbyte (fuz_sonar[1]);

	      lcdgoto (1, 0);
	      lcdputbyte (Lspeed);
	      lcdgoto (1, 15);
	      lcdputbyte (Rspeed);
	      c = 0;
	    }

#ifdef GAIA_2A
	  /* The robot must stop if it is very close, but it has no bumpers. */
	  if (sonars[5] <= COLLIDE && Lspeed > 0 && Rspeed > 0)
	    {
	      Lspeed = 0;
	      Rspeed = 0;
	    }

	  if (sonars[4] <= COLLIDE && Rspeed > 0)
	    {
	      Lspeed = 0;
	    }

	  if (sonars[3] <= COLLIDE && Rspeed > 0)
	    {
	      Lspeed = 0;
	    }

	  if (sonars[6] <= COLLIDE && Lspeed > 0)
	    {
	      Rspeed = 0;
	    }
	  if (sonars[7] <= COLLIDE && Lspeed > 0)
	    {
	      Rspeed = 0;
	    }

	  if (sonars[1] <= COLLIDE && sonars[2] <= COLLIDE &&
	      Lspeed < 0 && Rspeed < 0)
	    {
	      Lspeed = 0;
	      Rspeed = 0;
	    }
#endif
	  /* then scale down by 2 */
	  Lspeed >>= SCALE;
	  Rspeed >>= SCALE;

	  /* put a maximum on the speeds */
	  if (Lspeed > MAX_SPEED)
	    Lspeed = MAX_SPEED;
	  else if (Lspeed < -MAX_SPEED)
	    Lspeed = -MAX_SPEED;
	  if (Rspeed > MAX_SPEED)
	    Rspeed = MAX_SPEED;
	  else if (Rspeed < -MAX_SPEED)
	    Rspeed = -MAX_SPEED;
	}
      else
	{
	  if (sonarTimeOut)
	    sonarTimeOut--;
	  else
	    {
	      Lspeed = 0;
	      Rspeed = 0;
	    }
	}
      desiredSpeeds[0] = Lspeed;
      desiredSpeeds[1] = Rspeed;
      desiredSpeeds[2] = Lspeed;
      desiredSpeeds[3] = Rspeed;

      setMotorSpeedAll (desiredSpeeds);

      {
	static int counter = 10;
	if (!--counter)
	  {
	    counter = 10;
	    fprintf (stderr, "Speeds: % 5d % 5d", Lspeed, Rspeed);
	    for (i = 0; i < 8; i++)
	      {
		fprintf (stderr, " % 4d", buffer[i]);
	      }
	    fprintf (stderr, "\n");
	  }
      }

      usleep (10000);
    }

  serClose (sonar);

  /* release the motor */

  return (0);
}

void
sigHandler (int dummy)
     /* On critical signals, we should detach from shared memory */
{

  closeMotors ();
  /*release the motor */

  exit (-1);
}
