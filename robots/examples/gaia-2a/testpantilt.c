/*
 */

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <math.h>
#include <time.h>

#define DEFAULT_SERIAL_PORT "/dev/ttyS0"

#define BAUDRATE B9600
#define _POSIX_SOURCE 1		/* POSIX compliant source */
#define FALSE 0
#define TRUE 1

static struct termios oldtio, newtio;

int serOpen (char *);
void serClose (int);
void setservos (unsigned char, unsigned char);

int fd;

/* Function Prototype for our signal handler */
void sigHandler (int dummy);

int
main (void)
{
  char xs = 50;
  char ys = 50;
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  fd = serOpen (DEFAULT_SERIAL_PORT);

  while (1)
    {
      setservos (xs, ys);
      usleep (1000000);
      setservos (xs, 255 - ys);
      usleep (1000000);
      setservos (255 - xs, 255 - ys);
      usleep (1000000);
      setservos (255 - xs, ys);
      usleep (1000000);
    }

  return 0;
}

void
setservos (unsigned char t, unsigned char p)
{
  unsigned char T[3];
  unsigned char P[3];

  T[0] = 'T';
  P[0] = 'P';
  T[1] = t;
  P[1] = p;
  T[2] = 0;
  P[2] = 0;
  write (fd, T, 2);
  write (fd, P, 2);
}

int
serOpen (char *device)
{
  int lfd;
  lfd = open (device, O_RDWR | O_NOCTTY);
  if (lfd < 0)

    {
      perror (device);
      return -1;
    }
  tcgetattr (lfd, &oldtio);	/* save current port settings */
  bzero (&newtio, sizeof (newtio));
  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR;
  newtio.c_oflag = 0;

  return lfd;
}

void
sigHandler (int dummy)
{
  setservos (128, 128);
  exit (-1);
}

void
serClose (int fd)
{
/*  tcsetattr (fd, TCSANOW, &oldtio); */
  close (fd);
}

#define DEFAULT_SERIAL_PORT "/dev/ttyS0"
