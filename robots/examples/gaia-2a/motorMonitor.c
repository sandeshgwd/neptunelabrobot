/*
 */

#include <stdio.h>
#include <signal.h>
#include "mbuff.h"		/* Required for shared memory interface */
#include "motor.h"		/* Contains MOTOR structure definition */

/* Function Prototype for our signal handler */
void sigHandler (int dummy);

/* the contents of shared memory may change at any time, thus volatile */
static volatile char *motorSharedMem;

int
main (int argc, char **argv)
{
  /* These are local copies of the shared variables
   * We use the local copies from all calculation and
   * update the shared data only as needed
   */
  int LFactualSpeed, LBactualSpeed, RFactualSpeed, RBactualSpeed;
  int LFdesiredSpeed, LBdesiredSpeed, RFdesiredSpeed, RBdesiredSpeed;

  MOTOR *motor;			/* This will point to the shared memory */
  int error;

  /* Get the shared memory for the motor controller */
  motorSharedMem =
    (volatile char *) mbuff_attach ("motor", sizeof (struct MOTOR_S));
  if (motorSharedMem == NULL)
    {
      printf ("mbuff_alloc failed\n");
      exit (-1);
    }

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly
   */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  /* The motor structure occupies the first (and only) part of shared memory */
  motor = (struct MOTOR_S *) motorSharedMem;

  error = 0;
  while (1)
    {
      /* synchronize local variables with the shared variables */
      LFdesiredSpeed = motor->desiredSpeed[0];
      LFactualSpeed = motor->actualSpeed[0];
      LBdesiredSpeed = motor->desiredSpeed[1];
      LBactualSpeed = motor->actualSpeed[1];
      RFdesiredSpeed = motor->desiredSpeed[2];
      RFactualSpeed = motor->actualSpeed[2];
      RBdesiredSpeed = motor->desiredSpeed[3];
      RBactualSpeed = motor->actualSpeed[3];
      if (LFactualSpeed != LFdesiredSpeed)
	{
	  printf ("%d %d %d %d\n", LFdesiredSpeed, LBdesiredSpeed,
		  RFdesiredSpeed, RBdesiredSpeed);
	  printf ("%d %d %d %d\n", LFactualSpeed, LBactualSpeed,
		  RFactualSpeed, RBactualSpeed);
	  error = 1;
	}
      else if (error)
	{
	  printf ("OK\n");
	  error = 0;
	}

      usleep (5000);
    }

  /* release the shared memory */
  mbuff_detach ("motor", (void *) motorSharedMem);
}

/*
 * sigHandler (int dummy)
 * Standard signal handler.
 * On critical signals, we should detach from shared memory
 */
void
sigHandler (int dummy)
{
  mbuff_detach ("motor", (void *) motorSharedMem);
  exit (-1);
}
