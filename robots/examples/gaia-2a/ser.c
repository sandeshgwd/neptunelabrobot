/*
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <math.h>
#include <time.h>

#define BAUDRATE B9600
#define _POSIX_SOURCE 1		/* POSIX compliant source */
#define FALSE 0
#define TRUE 1

static struct termios oldtio, newtio;

int
serOpen (char *device)
{
  int fd;

  fd = open (device, O_RDWR | O_NOCTTY);
  if (fd < 0)
    {
      perror (device);
      return -1;
    }

  tcgetattr (fd, &oldtio);	/* save current port settings */

  bzero (&newtio, sizeof (newtio));
  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR;
  newtio.c_oflag = 0;

  /* set input mode (non-canonical, no echo,...) */
  newtio.c_lflag = 0;

  newtio.c_cc[VTIME] = 0;	/* 1 second time out */
  newtio.c_cc[VMIN] = 0;	/* blocking read until 5 chars received */

  tcflush (fd, TCIFLUSH);
  tcsetattr (fd, TCSANOW, &newtio);

  return (fd);
}

void
serClose (fd)
{
/*  tcsetattr (fd, TCSANOW, &oldtio); */
  close (fd);
}

int
GPSsend (int fd, int command, int count, unsigned char *data)
{
  int i;
  int checksum;
  int bytesSent, result;
  static unsigned char header[8] =
    { 0x55, 0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  /* swap bytes in header */
  header[2] = command & 0xFF;
  header[3] = (command >> 8) & 0xFF;
  header[4] = count & 0xFF;
  header[5] = (count >> 8) & 0xFF;
  checksum = 0xD6;		/* 0x55 + 0x81 */

  for (i = 2; i < 6; i++)
    {
      checksum += header[i];
    }
  header[7] = -(checksum & 0xFF);
  if ((bytesSent = write (fd, header, 8)) != 8)
    return (-1);

  bytesSent = 0;
  if (count)
    {
      checksum = 0;
      for (i = 0; i < count; i++)
	{
	  checksum += data[i];
	}
      checksum = -(checksum & 0xFF);
      while (count)
	{
	  result = write (fd, data + bytesSent, count);
	  if (result <= 0)
	    {
	      fprintf (stderr, "Could not write data to GPS\n");
	      break;
	    }
	  else
	    {
	      bytesSent += result;
	      count -= result;
	    }
	}
      while (!write (fd, &checksum, 1))
	;
    }
  return (bytesSent);
}

int
GPSrecv (int fd, int *command, int *count, unsigned char *data)
{
  int i;
  int checksum;
  int bytesRecv, result;
  static unsigned char header[8];

  i = 20;
  bytesRecv = 0;
  while (i-- && (bytesRecv < 2))
    {
      if (bytesRecv)
	{
	  result = read (fd, header + 1, 1);
	  if (result == 1)
	    {
	      if (header[1] == 0x81)
		bytesRecv = 2;
	      else
		bytesRecv = 0;
	    }
	}
      else
	{
	  result = read (fd, header, 1);
	  if (result == 1)
	    {
	      if (header[0] == 0x55)
		bytesRecv = 1;
	      else
		bytesRecv = 0;
	    }
	}

    }
  i = 20;
  bytesRecv = 0;
  while (i && (bytesRecv < 6))
    {
      result = read (fd, header + bytesRecv + 2, 6 - bytesRecv);
      if (result >= 0)
	bytesRecv += result;
      else
	break;
    }
  {
    fprintf (stderr, "\nHeader %02X %02X %02X %02X %02X %02X %02X %02X\n",
	     (unsigned int) header[0], (unsigned int) header[1],
	     (unsigned int) header[2], (unsigned int) header[3],
	     (unsigned int) header[4], (unsigned int) header[5],
	     (unsigned int) header[6], (unsigned int) header[7]);
    if (bytesRecv < 6)
      return (-1);
  }
  /* swap bytes in header */
  *command = (header[3] << 8) | header[2];
  result = (header[5] << 8) | header[4];
  if (*count > result)
    *count = result + 1;

  checksum = 0;
  for (i = 0; i < 7; i++)
    {
      checksum += header[i];
    }
  checksum &= 0xFF;
  if ((checksum + header[7]) & 0xFF)
    {
      fprintf (stderr, "Checksum error %d, %d\n", checksum, header[7]);
      return (-1);
    }

  fprintf (stderr, "Header %02X %02X %02X %02X %02X %02X %02X %02X\n",
	   (unsigned int) header[0], (unsigned int) header[1],
	   (unsigned int) header[2], (unsigned int) header[3],
	   (unsigned int) header[4], (unsigned int) header[5],
	   (unsigned int) header[6], (unsigned int) header[7]);

  bytesRecv = 0;
  while (*count)
    {
      result = read (fd, data + bytesRecv, *count);
      if (result <= 0)
	{
	  fprintf (stderr, "Could not read data from GPS\n");
	  break;
	}
      else
	{
	  bytesRecv += result;
	  *count -= result;
	}
    }
  *count = bytesRecv;

  {
    unsigned char *dataPtr;
    dataPtr = data;


    while (bytesRecv--)
      fprintf (stderr, "%02X ", (unsigned int) *dataPtr++);
  }
  return (0);
}

#define DEFAULT_SERIAL_PORT "/dev/ttyS1"

int
main (int argc, char **argv)
{
  int i;
  int fd = -1;
  /*  int count; */
  char buf1[8];
  char buf2[8];
  int res1, res2;

  if (argc > 1)
    fd = serOpen (argv[1]);
  else
    fd = serOpen (DEFAULT_SERIAL_PORT);

  if (fd < 0)
    {
      fprintf (stderr, "Could not open serial port\n");
      exit (-1);
    }
  fprintf (stderr, "Opened serial port\n");

  buf2[0] = '0';
  for (i = 1; i < 6; i++)
    buf2[i] = '-';
  buf2[6] = '\n';
  buf2[7] = 0xD;

  res2 = write (fd, buf2, 8);
  while (1)
    {
      res1 = read (fd, buf1, 1);
      if (res1 != 0)
	printf ("%d", res1);
    }

  /* Use:
   *     result = read(fd, buffer, size);
   * or
   *     result = write(fd, buffer, size);
   * to read from or write to the serial port
   */

  serClose (fd);

  return 0;
}
