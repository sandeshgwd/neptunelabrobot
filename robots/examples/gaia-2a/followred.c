/*
 */

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <math.h>
#include <time.h>
#include "usermotor.h"
//#include "mbuff.h"
#include "labo2.h"

#include <pthread.h>

#define SERVOS_SERIAL "/dev/ttyS0"
#define VISION_SERIAL "/dev/ttyS5"

#define BAUDRATE B9600
#define _POSIX_SOURCE 1
#define FALSE 0
#define TRUE 1

signed int Lspeed;
signed int Rspeed;

/* stuff for talking to vision board.
 * ensure the vision board is running first! 
 * this program works with follow.x0 */
#define FRAMING_CHARACTER 0xFF
#define VIS_PACKET_SIZE 3	/* frame, X,Y,size */

volatile int newVisData;
volatile unsigned char visData[4];
int panning;

static struct termios soldtio, snewtio;
static struct termios voldtio, vnewtio;

int init_servos (void);
int init_vision (void);
void close_servos (void);
void close_vision (void *arg);
void setservos (unsigned char, unsigned char);
void trackred (void);

int sserial, vserial;
unsigned int x, y, size;	/* these are for the viewed red object */

/* Function Prototype for our signal handler */
void sigHandler (int dummy);
void *visHandler (void *port);
void *panHandler (void *port);
void *eyeHandler (void *port);

/* the contents of shared memory may change at any time, thus volatile */
volatile char *sensorsSharedMem;

#define MIN_BLOBSIZE 40
#define XCENTRE 72
#define YCENTRE 70
#define CENTRESIZE 20
#define FIXEDSIZE 10

#define XMAX 0xD0
#define XMIN 0x30
#define YMAX 0xE0
#define YMIN 0x20

#define SERVO_XC 140
#define SERVO_YC 128

#define NAV_CENTRE 40

int xs, ys;

MOTOR *motor;
SENSOR *sensors;

/******************************** main
 * main function */
int
main (void)
{
  pthread_t visThread, panThread, eyeThread;
  int err;
  int sonar;			/* sonar serial device */
  unsigned char buffer[8];	/* space for the raw sonar readings */
  unsigned int sonars[8];
  unsigned int fuz_sonar[8];

  panning = 1;
  xs = SERVO_XC;
  ys = SERVO_YC;

  motor = init_motors ();

  sensorsSharedMem =
    (volatile char *) mbuff_attach ("sensors", sizeof (SENSOR));
  if (sensorsSharedMem == NULL)
    {
      printf ("mbuff_alloc failed\n");
      exit (-1);
    }

  lcdopen (0);

  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  sensors = (SENSOR *) sensorsSharedMem;

  stop_motors (motor);
  motor->command = 0x33;	/* brake off */

  if ((err = pthread_create (&visThread, NULL, visHandler, VISION_SERIAL)))
    {
      perror ("thread_create");
      exit (-1);
    }

  if ((err = pthread_create (&panThread, NULL, panHandler, SERVOS_SERIAL)))
    {
      perror ("thread_create");
      exit (-1);
    }

  if ((err = pthread_create (&eyeThread, NULL, eyeHandler, SERVOS_SERIAL)))
    {
      perror ("thread_create");
      exit (-1);
    }

  sserial = init_servos ();

  while (1)
    {
      if (newVisData)
	{
	  panning = 0;
	  Lspeed = 0;
	  Rspeed = 0;
	  trackred ();

	  /* rotate robot to keep camera straight */
	  if (size >= MIN_BLOBSIZE)
	    {
	      if (xs < (SERVO_XC - NAV_CENTRE))
		{
		  Lspeed = 100;
		  Rspeed = -100;
		}
	      else if (xs > (SERVO_XC + NAV_CENTRE))
		{
		  Lspeed = -100;
		  Rspeed = 100;
		}
	      else
		{
		  /* the object is fairly straight ahead; drive forward. 
		   * this should be avoid. */
		  Lspeed = 50;
		  Rspeed = 50;
		}
	    }

/* do trivial bumper avoid */
	  if ((Lspeed > 0) && ((sensors->bumper & 0x03) != 0x03))
	    {
	      Lspeed = 0;
	    }

	  if ((Rspeed > 0) && ((sensors->bumper & 0x0C) != 0x0C))
	    {
	      Rspeed = 0;
	    }

	  if ((Lspeed < 0) && ((sensors->bumper & 0x10) != 0x10))
	    {
	      Lspeed = 0;
	    }

	  if ((Rspeed < 0) && ((sensors->bumper & 0x20) != 0x20))
	    {
	      Rspeed = 0;
	    }

	  motor->desiredSpeed[0] = Lspeed;
	  motor->desiredSpeed[1] = Lspeed;
	  motor->desiredSpeed[2] = Rspeed;
	  motor->desiredSpeed[3] = Rspeed;
	}

      usleep (10000);
    }
  return 0;
}

/******************************** trackred
 * keep the red object in the centre of view */
void
trackred (void)
{


  x = visData[0];
  y = visData[1];
  size = visData[2];

  /*  printf ("(%d,%d):%d\n", x, y, size); */
  newVisData = 0;

  if (size > MIN_BLOBSIZE)
    {
      if (x < XCENTRE)
	{
	  if (x < XCENTRE - CENTRESIZE)
	    xs += 6;
	  else if (x < XCENTRE - FIXEDSIZE)
	    xs += 1;
	}
      else if (x > XCENTRE)
	{
	  if (x > XCENTRE + CENTRESIZE)
	    xs -= 6;
	  else if (x > XCENTRE + FIXEDSIZE)
	    xs -= 1;
	}

      if (y < YCENTRE)
	{
	  if (y < YCENTRE - CENTRESIZE)
	    ys -= 6;
	  else if (y < YCENTRE - FIXEDSIZE)
	    ys -= 1;
	}
      else if (y > YCENTRE)
	{
	  if (y > YCENTRE + CENTRESIZE)
	    ys += 6;
	  else if (y > YCENTRE + FIXEDSIZE)
	    ys += 1;
	}

      if (xs > XMAX)
	xs = XMAX;
      if (xs < XMIN)
	xs = XMIN;
      if (ys > YMAX)
	ys = YMAX;
      if (ys < YMIN)
	ys = YMIN;
    }
  else
    {
      panning = 1;
    }
}

/******************************** setservos
 * set the pan/tilt servos as instructed. */
void
setservos (unsigned char t, unsigned char p)
{
  unsigned char T[3];
  unsigned char P[3];

  T[0] = 'T';
  P[0] = 'P';
  T[1] = t;
  P[1] = p;
  T[2] = 0;
  P[2] = 0;

  if ((t >= YMIN) && (t <= YMAX))
    write (sserial, T, 3);
  else
    printf ("Invalid tilt: %X (%X,%X)\t", t, YMIN, YMAX);

  if ((p >= XMIN) && (p <= XMAX))
    write (sserial, P, 3);
  else
    printf ("Invalid pan: %X (%X,%X)", p, XMIN, XMAX);

  printf (".\n");
}

/******************************** init_servos
 * open the serial port, etc. Note that this serial
 * port also handles the sonars. */
int
init_servos (void)
{
  int lfd;
  lfd = open (SERVOS_SERIAL, O_RDWR | O_NOCTTY);
  if (lfd < 0)
    {
      perror (SERVOS_SERIAL);
      return -1;
    }
  tcgetattr (lfd, &soldtio);	/* save current port settings */
  bzero (&snewtio, sizeof (snewtio));
  snewtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  snewtio.c_iflag = IGNPAR;
  snewtio.c_oflag = 0;

  tcflush (lfd, TCIFLUSH);
  tcsetattr (lfd, TCSANOW, &snewtio);

  return lfd;
}


/******************************** sigHandles
 * deal with signals such as ^C */
void
sigHandler (int dummy)
{
  stop_motors (motor);
  mbuff_detach ("motor", (void *) motor);
  mbuff_detach ("sensors", (void *) sensorsSharedMem);
  close_servos ();
  close_vision ("");
  exit (-1);
}

/******************************** close_servos
 * put the serial port back. */
void
close_servos (void)
{
  tcsetattr (sserial, TCSANOW, &soldtio);
  close (sserial);
}

/******************************** panHandler
 * handle panning of the camera */
void *
panHandler (void *port)
{
  int xl = SERVO_XC;

  while (1)
    {
      pthread_testcancel ();

      if (panning)
	{
	  if (xl == XMIN)
	    xl = XMAX;
	  else
	    xl = XMIN;
	  ys = SERVO_YC;
	  xs = xl;
	}
      sleep (1);
    }
}

/******************************** visHandler
 * deal with info from the vision board */
void *
visHandler (void *port)
{
  int res;
  int timeout;
  int bytes;
  unsigned char buf[32];

  vserial = init_vision ();
  if (vserial < 0)
    {
      pthread_exit (NULL);
    }

  pthread_cleanup_push (close_vision, NULL);
  /* At thread exit, close port and restore settings */

  while (1)
    {
      pthread_testcancel ();	/* Allow this thread to be cancelled here */
      timeout = 0;
      buf[0] = 0;
      write (vserial, "G", 1);	/* Request the data */

      /* get framing character */
      do
	{
	  res = read (vserial, buf, 1);	/* get one character */
	}
      while (buf[0] != FRAMING_CHARACTER && res >= 0 && ++timeout < 4);

      if (buf[0] != FRAMING_CHARACTER || res < 0 || timeout == 4
	  || newVisData < 0)
	{
	  continue;
	}

      bytes = 0;
      timeout = 4;
      while (bytes < VIS_PACKET_SIZE && --timeout)
	/* get all data or timeout */
	{
	  res = read (vserial, buf + bytes, VIS_PACKET_SIZE - bytes);
	  if (res >= 0)
	    bytes += res;
	}
      if (!newVisData)
	{
	  if (!timeout)
	    newVisData = -1;
	  else
	    newVisData = 1;
	  memcpy ((void *) visData, buf, VIS_PACKET_SIZE);
	  visData[bytes] = 0;
	}
    }
  pthread_cleanup_pop (1);
}

/******************************** eyeHandler
 * talk to the cameras periodically. No one else should. */
void *
eyeHandler (void *port)
{
  int ly, lx;
  int r = 50;
  ly = ys;
  lx = xs;
  while (1)
    {
      pthread_testcancel ();

/* ramp the motors. */
      if (ys > ly + r)
	ly += r;
      else if (ys < ly - r)
	ly -= r;
      else
	ly = ys;

      if (xs > lx + r)
	lx += r;
      else if (xs < lx - r)
	lx -= r;
      else
	lx = xs;

      setservos (ly, lx);
      usleep (100000);
    }
}

/******************************** init_vision
 * set up the serial port, etc */
int
init_vision (void)
{
  int lfd;

  lfd = open (VISION_SERIAL, O_RDWR | O_NOCTTY);
  if (lfd < 0)
    {
      perror (VISION_SERIAL);
      return (lfd);
    }

  tcgetattr (lfd, &voldtio);	/* save current port settings */

  bzero (&vnewtio, sizeof (vnewtio));
  vnewtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  vnewtio.c_iflag = IGNPAR;
  vnewtio.c_oflag = 0;

  /* set input mode (non-canonical, no echo,...) */
  vnewtio.c_lflag = 0;

  vnewtio.c_cc[VTIME] = 1;	/* time out value */
  vnewtio.c_cc[VMIN] = 0;	/* no minimum */

  tcflush (lfd, TCIFLUSH);
  tcsetattr (lfd, TCSANOW, &vnewtio);
  return (lfd);
}

/******************************** close_vision
 * clean up serial port */
void
close_vision (void *arg)
{
  tcsetattr (vserial, TCSANOW, &voldtio);
  close (vserial);
}
