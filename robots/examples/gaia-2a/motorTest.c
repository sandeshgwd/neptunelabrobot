/*
 */

#include <stdio.h>
#include <signal.h>
#include "usermotor.h"		/* Contains MOTOR structure definition */

/* Function Prototype for our signal handler */
void sigHandler (int dummy);

/* the contents of shared memory may change at any time, thus volatile */
static volatile char *motorSharedMem;
MOTOR *motor;			/* This will point to the shared memory */

int
main ()
{
  /* These are local copies of the shared variables
   * We use the local copies from all calculation and
   * update the shared data only as needed
   */
  int LFactualSpeed, LBactualSpeed, RFactualSpeed, RBactualSpeed;
  int LFdesiredSpeed, LBdesiredSpeed, RFdesiredSpeed, RBdesiredSpeed;
  int ramp;
  int actualSpeeds[4];
  int desiredSpeeds[4];
  int i;
  int error;

  /* Get the shared memory for the motor controller */
  if (initMotors ())
    exit (-1);

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly
   */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  /* The motor structure occupies the first (and only) part of shared memory */
  motor = (struct MOTOR_S *) motorSharedMem;

  for (i = 0; i < 4; i++)
    {
      desiredSpeeds[i] = 1;
      actualSpeeds[i] = 2;
    }

  setMotorSpeedAll (desiredSpeeds);
  getMotorSpeedAll (actualSpeeds);

  /* synchronize local variables with the shared variables */
  getDesiredSpeedAll (desiredSpeeds);
  LFdesiredSpeed = desiredSpeeds[0];
  LBdesiredSpeed = desiredSpeeds[1];
  RFdesiredSpeed = desiredSpeeds[2];
  RBdesiredSpeed = desiredSpeeds[3];

  error = 0;
  while (1)
    {

      getMotorSpeedAll (actualSpeeds);

      LFactualSpeed = actualSpeeds[0];
      LBactualSpeed = actualSpeeds[1];
      RFactualSpeed = actualSpeeds[2];
      RBactualSpeed = actualSpeeds[3];

      LFdesiredSpeed += ramp;
      if (LFdesiredSpeed > 254)
	{
	  LFdesiredSpeed = 254;
	  ramp = -8;
	}
      else if (LFdesiredSpeed < -254)
	{
	  LFdesiredSpeed = -254;
	  ramp = 8;
	}

      LBdesiredSpeed += ramp;
      if (LBdesiredSpeed > 254)
	{
	  LBdesiredSpeed = 254;
	  ramp = -8;
	}
      else if (LBdesiredSpeed < -254)
	{
	  LBdesiredSpeed = -254;
	  ramp = 8;
	}

      RFdesiredSpeed += ramp;
      if (RFdesiredSpeed > 254)
	{
	  RFdesiredSpeed = 254;
	  ramp = -8;
	}
      else if (RFdesiredSpeed < -254)
	{
	  RFdesiredSpeed = -254;
	  ramp = 8;
	}

      RBdesiredSpeed += ramp;
      if (RBdesiredSpeed > 254)
	{
	  RBdesiredSpeed = 254;
	  ramp = -8;
	}
      else if (RBdesiredSpeed < -254)
	{
	  RBdesiredSpeed = -254;
	  ramp = 8;
	}

      /* Only update the shared variables _AFTER_ all necessary calcultions */
      setMotorSpeedAll (desiredSpeeds);
      /* wait a second */
      sleep (1);
    }
  desiredSpeeds[0] = 0;
  desiredSpeeds[1] = 0;
  desiredSpeeds[2] = 0;
  desiredSpeeds[3] = 0;
  setMotorSpeedAll (desiredSpeeds);

  closeMotors ();
}

/* On critical signals, we should detach from shared memory */
void
sigHandler (int dummy)
{
  int desiredSpeeds[4];
  desiredSpeeds[0] = 0;
  desiredSpeeds[1] = 0;
  desiredSpeeds[2] = 0;
  desiredSpeeds[3] = 0;
  setMotorSpeedAll (desiredSpeeds);
  closeMotors ();

  exit (-1);
}
