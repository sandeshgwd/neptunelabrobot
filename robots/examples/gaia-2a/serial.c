/*
 */

/*
include <sys/types.h>
include <sys/stat.h>
include <fcntl.h>
include <termios.h>
include <unistd.h>
*/
#define _POSIX_SOURCE 1		/* POSIX compliant source */

/* waits for a specific byte on the input, discarding all preceding bytes. */
void
waitbyte (int fd, char byte)
{
  char buf[1];

  while (1)
    {
      while (!read (fd, buf, 1));
      if (buf[0] == byte)
	return;
    }
}
