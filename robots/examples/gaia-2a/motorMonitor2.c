/*
 */

#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <usermotor.h>
#include "labo2.h"		/* Contains MOTOR structure definition */

/* Function Prototype for our signal handler */
void sigHandler (int dummy);

/* the contents of shared memory may change at any time, thus volatile */
static int motorfd = -1;


int
main (int argc, char **argv)
{
  /* These are local copies of the shared variables
   * We use the local copies from all calculation and
   * update the shared data only as needed
   */
  int LFactualSpeed, LBactualSpeed, RFactualSpeed, RBactualSpeed;
  int LFdesiredSpeed, LBdesiredSpeed, RFdesiredSpeed, RBdesiredSpeed;
  int actualSpeeds[4];
  int desiredSpeeds[4];
  int error;
  int i;
  int display;
  int counter = 20;
  ONEMOTOR temp;

  /* Open the motor controller device */

  if (initMotors ())
    exit (-1);
  setMotorPower (1);
// Test code for motor control relay
#if 0
  while (1)
    {
      setMotorPower (1);
      printf ("Motor ON\n");
      usleep (1000000);
      setMotorPower (0);
      printf ("Motor OFF\n");
      usleep (1000000);
    }
#endif

  setBrakes (0);

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly
   */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  for (i = 0; i < 4; i++)
    {
      desiredSpeeds[i] = 0;
      actualSpeeds[i] = 0;
    }
  desiredSpeeds[0] = 0;
  desiredSpeeds[1] = 0;
  desiredSpeeds[2] = 15;
  desiredSpeeds[3] = 15;

  setMotorSpeedAll (desiredSpeeds);
  getMotorSpeedAll (actualSpeeds);

  error = 0;
  while (1)
    {
      /* synchronize local variables with the shared variables */
      if (getDesiredSpeedAll (desiredSpeeds))
	perror ("getDesiredSpeedAll failed");

      LFdesiredSpeed = desiredSpeeds[0];
      LBdesiredSpeed = desiredSpeeds[1];
      RFdesiredSpeed = desiredSpeeds[2];
      RBdesiredSpeed = desiredSpeeds[3];

      getMotorSpeedAll (actualSpeeds);

      LFactualSpeed = actualSpeeds[0];
      LBactualSpeed = actualSpeeds[1];
      RFactualSpeed = actualSpeeds[2];
      RBactualSpeed = actualSpeeds[3];

      display = 0;
      for (i = 0; i < 4; i++)
	{
	  if (desiredSpeeds[i] != actualSpeeds[i])
	    display = 1;
	}
      if (!display)
	{
	  if (!--counter)
	    display = 1;
	}
      else
	counter = 20;
      if (display)
	{
	  printf ("%d %d %d %d : ", LFdesiredSpeed, LBdesiredSpeed,
		  RFdesiredSpeed, RBdesiredSpeed);
	  printf ("%d %d %d %d\n", LFactualSpeed, LBactualSpeed,
		  RFactualSpeed, RBactualSpeed);
	  error = 1;

//        for (i = 0; i < 4; i++)
//          desiredSpeeds[i] = 00;
	}

      if (setMotorSpeedAll (desiredSpeeds))
	perror ("setDesiredSpeedAll failed");
      usleep (50000);
    }

  /* close the device */
  closeMotors ();
}

/*
 * sigHandler (int dummy)
 * Standard signal handler.
 * On critical signals, we should detach from shared memory
 */
void
sigHandler (int dummy)
{
  int desiredSpeeds[4];
  if (motorfd >= 0)
    {
      desiredSpeeds[0] = 0;
      desiredSpeeds[1] = 0;
      desiredSpeeds[2] = 0;
      desiredSpeeds[3] = 0;
      setMotorSpeedAll (desiredSpeeds);
      close (motorfd);
      motorfd = -1;
    }
  exit (-1);
}
