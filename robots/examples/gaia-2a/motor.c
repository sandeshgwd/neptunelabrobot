/*
 */

#include <stdio.h>
#include <mbuff.h>
#include "motor.h"

int
stop_motors (MOTOR * motor)
{
  motor->desiredSpeed[0] = 0;
  motor->desiredSpeed[1] = 0;
  motor->desiredSpeed[2] = 0;
  motor->desiredSpeed[3] = 0;
  motor->command = 0x00;
  return (0);
}

MOTOR *
init_motors (void)
{
  MOTOR *motor;

  /* Get the shared memory for the motor controller */
  motor = (struct MOTOR_S *) mbuff_attach ("motor", sizeof (struct MOTOR_S));
  if (motor == NULL)
    {
      printf ("mbuff_alloc failed\n");
    }

  /* The motor structure occupies the first (and only) part of shared memory */
  return (motor);
}
