/*
 *  Quick tests for RT-Linux lcd driver and user-space interface.
 */

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include "userlcd.h"

int done;

void
handler (int arg)
{
  done = 1;
}

int
main (void)
{
  int err;
  char i;

  err = lcdopen (0);
  if (err < 0)
    perror ("lcdopen() failed: ");

  err = lcdinit ();

  if (err < -1)			/* "Success" is -1; this is not a failure. */
    perror ("lcdinit() failed: ");

  done = 0;
  signal (SIGINT, handler);

  for (i = 10; i >= 0; i--)
    {
      lcdgoto (0, 7);
      lcdputbyte (i);
      sleep (1);
    }

  lcdclr ();

  err = lcdclose ();
  if (err != 0)
    perror ("lcdclose() failed: ");

  return 0;
}
