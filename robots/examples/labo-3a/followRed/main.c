/*
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include "mbuff.h"		/* Required for shared memory interface */
#include "motor.h"		/* Contains MOTOR structure definition */

#define MODEMDEVICE "/dev/ttyS0"
#define _POSIX_SOURCE 1		/* POSIX compliant source */

/* the contents of shared memory may change at any time, thus volatile */
static volatile char *motorSharedMem;

extern volatile int newVisData;
extern volatile unsigned char *visData;

/* Function Prototype for our signal handler */
void sigHandler (int dummy);
void *visHandler (void *arg);
void *followRed (void *arg);

int
main (argc, argv)
{
  pthread_t visThread, trackingThread;
  int err;
  MOTOR *motor;			/* This will point to the shared memory */

  /* Get the shared memory for the motor controller */
  motorSharedMem =
    (volatile char *) mbuff_attach ("motor", sizeof (struct MOTOR_S));
  if (motorSharedMem == NULL)
    {
      printf ("mbuff_alloc failed\n");
      exit (-1);
    }

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly
   */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  /* The motor structure occupies the first (and only) part of shared memory */
  motor = (struct MOTOR_S *) motorSharedMem;

  newVisData = 0;
  if ((err = pthread_create (&visThread, NULL, visHandler, MODEMDEVICE)))
    {
      perror ("thread_create");
      exit (-1);
    }

  if ((err =
       pthread_create (&trackingThread, NULL, followRed, motorSharedMem)))
    {
      perror ("thread_create");
      exit (-1);
    }

  printf ("starting main loop");
  while (1)
    {
      sleep (1);
    }
}

/* On critical signals, we should detach from shared memory */
void
sigHandler (int dummy)
{
  mbuff_detach ("motor", (void *) motorSharedMem);
  exit (-1);
}
