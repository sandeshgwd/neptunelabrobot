/*
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

/**************** Definitions *****************/
#define BAUDRATE B9600
#define _POSIX_SOURCE 1		/* POSIX compliant source */

#define FRAMING_CHARACTER '#'
#define VIS_PACKET_SIZE 19

/**************** Global Variables *****************/
volatile int newVisData;
volatile unsigned char visData[20];

/**************** Local Variables *****************/
static struct termios oldtio;
static int serialFD;

/**************** Prototypes *****************/
void serialClose (void *arg);
int serialOpen (char *port);

void *
visHandler (void *port)
{
  int res;
  int timeout;
  int bytes;
  char buf[32];

  serialFD = serialOpen (port);	/* Open and configure the serial port */
  if (serialFD < 0)
    {
      pthread_exit (NULL);
    }

  pthread_cleanup_push (serialClose, NULL);	/* At thread exit, close port and restore settings */
  while (1)
    {
      pthread_testcancel ();	/* Allow this thread to be cancelled here */
      timeout = 0;
      buf[0] = 0;

      write (serialFD, "G", 1);	/* Request the data */

      /* get framing character */
      do
	{
	  res = read (serialFD, buf, 1);	/* get one character */
	}
      while (buf[0] != FRAMING_CHARACTER && res >= 0 && ++timeout < 4);

      if (buf[0] != FRAMING_CHARACTER || res < 0 || timeout == 4
	  || newVisData < 0)
	{
	  continue;
	}

      bytes = 0;
      timeout = 4;
      while (bytes < VIS_PACKET_SIZE && --timeout)	/* get all data or timeout */
	{
	  res = read (serialFD, buf + bytes, VIS_PACKET_SIZE - bytes);
	  if (res >= 0)
	    bytes += res;
	}
      if (!newVisData)
	{
	  if (!timeout)
	    newVisData = -1;
	  else
	    newVisData = 1;
	  memcpy (visData, buf, VIS_PACKET_SIZE);
	  visData[bytes] = 0;
	}
    }
  pthread_cleanup_pop (1);
}


int
serialOpen (char *port)
{
  int fd;
  struct termios newtio;

  fd = open ((char *) port, O_RDWR | O_NOCTTY);
  if (fd < 0)
    {
      perror ((char *) port);
      return (fd);
    }

  tcgetattr (fd, &oldtio);	/* save current port settings */

  bzero (&newtio, sizeof (newtio));
/*  newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD; */
  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR;
  newtio.c_oflag = 0;

  /* set input mode (non-canonical, no echo,...) */
  newtio.c_lflag = 0;

  newtio.c_cc[VTIME] = 1;	/* time out value */
  newtio.c_cc[VMIN] = 0;	/* no minimum */

  tcflush (fd, TCIFLUSH);
  tcsetattr (fd, TCSANOW, &newtio);
  return (fd);
}


void
serialClose (void *arg)
{
  tcsetattr (serialFD, TCSANOW, &oldtio);
  close (serialFD);
}
