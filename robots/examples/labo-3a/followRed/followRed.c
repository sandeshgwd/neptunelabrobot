/*
 */

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#if 0
#include "mbuff.h"		/* Required for shared memory interface */
#endif
#include "motor.h"		/* Contains MOTOR structure definition */


#define RED_X 16
#define RED_Y 17
#define RED_SIZE 18

#define MIN_RED 10
#define SPEED_MED 158

#define CENTRE_RIGHT 100
#define MIDDLE_RIGHT 120
#define FAR_RIGHT 140

#define CENTRE_LEFT 80
#define MIDDLE_LEFT 60
#define FAR_LEFT 40

#define SPEED_SLOW 32

#define DIR_SLOW_RIGHT 10
#define DIR_MED_RIGHT 20
#define DIR_RIGHT 30

#define DIR_SLOW_LEFT -10
#define DIR_MED_LEFT -20
#define DIR_LEFT -30

#define SPEED_STOP 120
#define DIR_CENTRE 120


/* the contents of shared memory may change at any time, thus volatile */
extern volatile unsigned char *visData;
extern volatile int newVisData;


void *
followRed (void *arg)
{
  /* These are local copies of the shared variables
   * We use the local copies from all calculation and
   * update the shared data only as needed
   */
  int actualSpeed, actualDirection;
  int desiredSpeed, desiredDirection;
  MOTOR *motor;			/* This will point to the shared memory */

  if (!arg)
    pthread_exit (NULL);

  /* The motor structure occupies the first (and only) part of shared memory */
  motor = (struct MOTOR_S *) arg;

  /* Use local variables to do calculations */
  desiredSpeed = 128;
  desiredDirection = 128;

  /* synchronize shared and local variables */
  actualSpeed = motor->actualSpeed;
  actualDirection = motor->actualDirection;
  motor->desiredSpeed = desiredSpeed;
  motor->desiredDirection = desiredDirection;

  motor->command = 0;

  while (1)
    {
      if (newVisData > 0)
	{
	  fprintf (stderr, "Good data: %s\n", visData);
	  newVisData = 0;
	  desiredSpeed = SPEED_STOP;
	  desiredDirection = DIR_CENTRE;

	  if (visData[RED_SIZE] > MIN_RED)
	    {
	      desiredSpeed = SPEED_MED;

	      if (visData[RED_X] > CENTRE_RIGHT)
		desiredDirection = DIR_SLOW_RIGHT;
	      if (visData[RED_X] > MIDDLE_RIGHT)
		desiredDirection = DIR_MED_RIGHT;
	      if (visData[RED_X] > FAR_RIGHT)
		{
		  desiredSpeed = SPEED_SLOW;
		  desiredDirection = DIR_RIGHT;
		}

	      else if (visData[RED_X] < CENTRE_LEFT)
		desiredDirection = DIR_SLOW_LEFT;
	      if (visData[RED_X] < MIDDLE_LEFT)
		desiredDirection = DIR_MED_LEFT;
	      if (visData[RED_X] < FAR_LEFT)
		{
		  desiredSpeed = SPEED_SLOW;
		  desiredDirection = DIR_LEFT;
		}
	    }
	  else
	    {
	      desiredSpeed = 0;
	      desiredDirection = DIR_SLOW_RIGHT;
	    }

	  desiredSpeed += SPEED_STOP;
	  desiredDirection += DIR_CENTRE;


	  /* Only update the shared variables _AFTER_ all necessary calcultions */
	  motor->desiredSpeed = desiredSpeed;
	  motor->desiredDirection = desiredDirection;
	}
      else if (newVisData < 0)
	{
	  fprintf (stderr, "Bad data: %s\n", visData);
	  newVisData = 0;
	}

      /* wait a tenth of a second */
      usleep (100);
    }
}
