/*
 * Uses IR sensors to avoid collisions
 * By Giles Babin, 2005-03-20
 */
#include <stdio.h>
#include <signal.h>
#include <userlcd.h>
#include <usermotor.h>
#include <userir.h>
#include "debug.h"
#include "setSpeedVar.h"

#define SCALE   8
#define MAX_SPEED 20
#define MIN_SPEED 4
#define MAX_DISTANCE 0x10   //was 0x18
#define RAMP_RATE 6  //4
#define AVOID_PRIORITY 8

void *
irAvoid (void *arg)
{
  int Lspeed, Rspeed;
  int i, c = 0;
  unsigned int irs[12];
  unsigned int rampedIRs[12];
  unsigned int hits;
  VAR *speed = arg;

/*
 *             5     0
 *            _________
 *      6  _-~         ~-_ 1
 *      _-~               ~-_
 *     .                     . 
 *  7 .                       . 2
 *   .                         . 
 *  .                           .
 *  |                           |
 *  |           LABO            |
 *  |                           |
 *  |                           |
 *  |                           |
 *  |                           |
 *   .                         .
 *    .                       . 
 *  8  +---------------------+  3
 *           9         4
 *
 */

  signed int lcoef[12] = {
    -48,			/* 0 - front right */
    -72,			/* 1 - mid front right */
    -24,			/* 2 - left right */
    -6,				/* 3 - right rear */
    8,				/* 4 - rear right */

    -48,			/* 5 - front left */
    16,				/* 6 - mid front left */
    12,				/* 7 - right left */
    6,				/* 8 - left rear */
    8,				/* 9 - rear left */

    0,				/* unused */
    0				/* unused */
  };

  signed int rcoef[12];
  signed int offset;

  /* mirror images */
  rcoef[0] = lcoef[5];
  rcoef[1] = lcoef[6];
  rcoef[2] = lcoef[7];
  rcoef[3] = lcoef[8];
  rcoef[4] = lcoef[9];
  rcoef[5] = lcoef[0];
  rcoef[6] = lcoef[1];
  rcoef[7] = lcoef[2];
  rcoef[8] = lcoef[3];
  rcoef[9] = lcoef[4];
  rcoef[10] = lcoef[11];
  rcoef[11] = lcoef[10];

  offset = 0;
  for (i = 0; i < 12; i++)
    {
      irs[i] = 0;
      rampedIRs[i] = 0;
      offset += lcoef[i];
    }
  lcdclr ();

  while (1)
    {
      getIRs (irs);
      hits = 0;
      for (i = 0; i < 12; i++)
	{
	  if (irs[i] > rampedIRs[i])
	    rampedIRs[i] = irs[i];
	  else if (rampedIRs[i] > RAMP_RATE)
	    rampedIRs[i] -= RAMP_RATE;
	  else
	    rampedIRs[i] = 0;

	  if (rampedIRs[i] > MAX_DISTANCE)
	    {
	      Lspeed += lcoef[i] * (rampedIRs[i] - MAX_DISTANCE);
	      Rspeed += rcoef[i] * (rampedIRs[i] - MAX_DISTANCE);
	      hits++;
	    }
	}

      if (c++ >= 100)
	{
	  /* display sonar values for debug */
	  printf ("ir: ");
	  for (i = 0; i < 5; i++)
	    {
	      lcdgoto (0, 1 + i * 3);
	      lcdputbyte (irs[i]);
	    }
	  for (i = 0; i < 5; i++)
	    {
	      lcdgoto (1, 1 + i * 3);
	      lcdputbyte (irs[i + 5]);
	    }
	  for (i = 0; i < 12; i++)
	    printf ("%3X ",
		    irs[i] > MAX_DISTANCE ? irs[i] - MAX_DISTANCE : 0);
	  printf ("\n");
	  c = 0;
	}
      /* scale down */
      Lspeed >>= SCALE;
      Rspeed >>= SCALE;

      Lspeed += 10;		// Default (wander) Speed
      Rspeed += 10;		// Default (wander) Speed

      /* put a maximum on the speeds */
      if (Lspeed > MAX_SPEED)
	Lspeed = MAX_SPEED;
      else if (Lspeed < -MAX_SPEED)
	Lspeed = -MAX_SPEED;

      if (Rspeed > MAX_SPEED)
	Rspeed = MAX_SPEED;
      else if (Rspeed < -MAX_SPEED)
	Rspeed = -MAX_SPEED;
      
      if ((Rspeed <MIN_SPEED) && (Rspeed > -MIN_SPEED) && (Lspeed < MIN_SPEED) && (Lspeed > -MIN_SPEED))
       {
	  Rspeed = Rspeed*5;
	  Lspeed = Lspeed*5;		
       }	

      if (hits)
	{
	  setSpeedVars (speed, Lspeed, Rspeed, 250, AVOID_PRIORITY);
	}

      usleep (10000);
    }

  /* release the motor */

  return (0);
}
