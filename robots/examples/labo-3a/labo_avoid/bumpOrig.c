/************************************************************
(c) Applied AI Systems, Inc. 1999

 $Header: /usr/local/cvsroot/mc68332/mtKernel/usercode/bump.c,v 1.5 2002/08/13 12:19:49 pmaheral Exp $
************************************************************/
static const char rcsId[] =
  "$Id: bump.c,v 1.5 2002/10/19 12:19:49 pmaheral Exp $";
static const char rcsName[] = "$Name:  $";
static const char rcsRevision[] = "$Revision: 1.5 $";
static const char rcsDate[] = "$Date: 2002/08/13 12:19:49 $";

#include "labo2.h"
#include "setSpeedVar.h"

#define BUMP_PRIORITY 16
#define BUMP_TIME 2000

/* Global variables */

void *
bump (void *arg)
{
  VAR *speed = arg;
  unsigned int bump;
  int stuck;
//  int = 0;

  while (1)
    {
      bump = ~getBumper ();
/*
      if (bump & 0x7F)	
	{
	  setSpeedVars (speed, 0, 0, BUMP_TIME, BUMP_PRIORITY);
	  sleep(1);
	  stuck = 1;
	}
      else 
*/
      if (bump & 0x1F)		/* Front bumper hit */
	{
	  if (bump & 0x03)
	    setSpeedVars (speed, -8, -10, BUMP_TIME, BUMP_PRIORITY);
	  if (bump & 0x18)
	    setSpeedVars (speed, -10, -8, BUMP_TIME, BUMP_PRIORITY);
	  if (bump & 0x04)
	    setSpeedVars (speed, -10, -10, BUMP_TIME, BUMP_PRIORITY);
	}
      else if (bump & 0x60)	/* Back bumper hit */
	{
	  if (bump & 0x20)
	    setSpeedVars (speed, 8, 10, BUMP_TIME, BUMP_PRIORITY);
	  if (bump & 0x40)
	    setSpeedVars (speed, 10, 8, BUMP_TIME, BUMP_PRIORITY);
	}
      else if (stuck)		/* No more hits */
	{
	  setSpeedVars (speed, 0, 0, 2000, BUMP_PRIORITY);
	  sleep (1);
	  stuck = 0;
	}
      // Sleep for 100000us (0.1 seconds)
      usleep (100000);
    }
}
