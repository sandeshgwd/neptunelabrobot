/*
 * Uses IR sensors to avoid collisions
 * By Giles Babin, 2005-03-20
 */
#include <stdio.h>
#include <signal.h>
#include <userlcd.h>
#include <usermotor.h>
#include <userir.h>
#include "debug.h"
#include "setSpeedVar.h"

/* Function Prototype for our signal handler */
void sigHandler (int dummy);
void *irAvoid (void *arg);
void *bump (void *arg);
void *joystickServer (void *arg);

#define SCALE   8
#define MAX_SPEED 20
#define MAX_DISTANCE 0x18
#define RAMP_RATE 4
#define AVOID_PRIORITY 8

int
main (int argc, char **argv)
{
  VAR speed;
  int buttons, buttonHoldTime, power, enableMotorPower;
  pthread_t *irAvoidThread = NULL;
  pthread_t *bumpAvoidThread = NULL;
  pthread_t *joystickThread = NULL;
  signed int desiredSpeeds[4];
  sigset_t signals;

  pthread_mutex_init (&speed.lock, NULL);
  speed.left = 0;
  speed.right = 0;
  speed.priority = 0;
  speed.time.tv_sec = 0;
  speed.time.tv_usec = 0;

  lcdopen (0);
  lcdclr ();
//  lcdputs ("Starting in 5sec");
  sleep (5);
  lcdclr ();

  if (initMotors ())
    exit (-1);
  if (initIR ())
    exit (-1);

  configureSpeedPID (0, 2048, 4, 512);
  configureSpeedPID (1, 2048, 4, 512);
  configureSpeedPID (2, 2048, 4, 512);
  configureSpeedPID (3, 2048, 4, 512);

  setBrakes (0);

  sigemptyset (&signals);
  sigaddset (&signals, SIGALRM);
  sigaddset (&signals, SIGTERM);
  sigaddset (&signals, SIGQUIT);
  sigaddset (&signals, SIGINT);
  sigaddset (&signals, SIGHUP);
  pthread_sigmask (SIG_BLOCK, &signals, NULL);

  irAvoidThread = calloc (1, sizeof (pthread_t));
  if (irAvoidThread)
    {
      if (pthread_create (irAvoidThread, NULL, irAvoid, &speed))
	{
	  debug_print (minVerbLevel (2), "Could not create thread\n");
	  exit (1);
	}
    }

  bumpAvoidThread = calloc (1, sizeof (pthread_t));
  if (bumpAvoidThread)
    {
      if (pthread_create (bumpAvoidThread, NULL, bump, &speed))
	{
	  debug_print (minVerbLevel (2), "Could not create thread\n");
	  exit (1);
	}
    }

  joystickThread = calloc (1, sizeof (pthread_t));
  if (joystickThread)
    {
      if (pthread_create (joystickThread, NULL, joystickServer, &speed))
	{
	  debug_print (minVerbLevel (2), "Could not create thread\n");
	  exit (1);
	}
    }

  /* Install our own signal handler to catch these signals so we can
   * release the resources before exiting.  
   */

  sigemptyset (&signals);
  sigaddset (&signals, SIGTERM);
  sigaddset (&signals, SIGQUIT);
  sigaddset (&signals, SIGINT);
  sigaddset (&signals, SIGHUP);
  pthread_sigmask (SIG_UNBLOCK, &signals, NULL);
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  setMotorPower (3);

  power = getMotorPower ();
  buttons = getButtons ();
  fprintf (stderr, "Motor Power is %d\n", power);
  fprintf (stderr, "Motor Buttons is %d\n", buttons);

  buttonHoldTime = 0;
  enableMotorPower = 0;

  while (1)
    {
      power = getMotorPower ();
      if (ON != power)
	{
	  buttons = getButtons ();

	  if ((1 == buttons) || (2 == buttons))
	    {
	      buttonHoldTime++;
	    }
	  else
	    {
	      if (!buttons && buttonHoldTime > 10)
		{
		  enableMotorPower = 1;
		  setMotorPower (1);
		}
	      buttonHoldTime = 0;
	    }
	}

      if (enableMotorPower)
	{
	  if (!pthread_mutex_lock (&speed.lock))
	    {
	      desiredSpeeds[0] = speed.left;
	      desiredSpeeds[1] = speed.right;
	      pthread_mutex_unlock (&speed.lock);
	    }
	}
      else
	{
	  desiredSpeeds[0] = 0;
	  desiredSpeeds[1] = 0;
	}
      desiredSpeeds[2] = desiredSpeeds[0];
      desiredSpeeds[3] = desiredSpeeds[1];
      setMotorSpeedAll (desiredSpeeds);
      usleep (100000);
    }
}

void
sigHandler (int dummy)
     /* On critical signals, we should detach from shared memory */
{

  closeMotors ();
  /*release the motor */

  exit (-1);
}
