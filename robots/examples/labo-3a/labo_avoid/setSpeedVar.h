#ifndef SETSPEEDVAR_H
#define SETSPEEDVAR_H

#include <pthread.h>
#include <sys/time.h>
#include <time.h>

typedef struct
  {
    pthread_mutex_t lock;
    unsigned int priority;
    struct timeval time;
    int left;
    int right;
  }
VAR;

#endif
