#include <pthread.h>
#include "setSpeedVar.h"

int
setSpeedVars (VAR * var, int left, int right, unsigned int msHoldTime,
	      unsigned int priority)
{
  short status;
  status = -1;
  struct timeval systemTime;

  if (var)
    {
      status = 1;
      pthread_mutex_lock (&var->lock);
      gettimeofday (&systemTime, NULL);

      if ((priority >= var->priority)
	  || (var->time.tv_sec < systemTime.tv_sec)
	  || (var->time.tv_sec == systemTime.tv_sec
	      && var->time.tv_usec <= systemTime.tv_usec))
	{
	  var->time.tv_usec =
	    (systemTime.tv_usec + (msHoldTime * 1000)) % 1000000;
	  var->time.tv_sec =
	    systemTime.tv_sec + (msHoldTime / 1000) + (systemTime.tv_usec +
						       (msHoldTime * 1000)) /
	    1000000;
	  var->priority = priority;
	  var->left = left;
	  var->right = right;
	  status = 0;
	}
      pthread_mutex_unlock (&var->lock);
    }
  return (status);
}
