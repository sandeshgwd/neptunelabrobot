/************************************************************
 (c) Applied AI Systems, Inc. 2003

************************************************************/

#include "debug.h"
#define _GNU_SOURCE
#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<sys/un.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <signal.h>
#include <userlcd.h>
#include <usermotor.h>
#include <userir.h>

#include "simpleserver.h"
#include "setSpeedVar.h"

#define JOYSTICK_PRIORITY 32

/** Maximun number of characters that can be read for a single command
 * including all parameters and null-terminator */
#define MAX_PACKET_SIZE 257
#define MAX_DATA_SIZE (MAX_PACKET_SIZE - 23)

#define BAUDRATE B9600
#define MODEMDEVICE "/dev/ttyS0"
#define _POSIX_SOURCE 1		/* POSIX compliant source */

static int joystickSetMotorSpeeds (int left, int right);

static int joy_absoluteSpeedControl (unsigned char *dataIn,
				     unsigned char *dataOut);
static int joy_stopMotors (unsigned char *dataIn, unsigned char *dataOut);
static int joy_disableMotors (unsigned char *dataIn, unsigned char *dataOut);
static int joy_enableMotors (unsigned char *dataIn, unsigned char *dataOut);
static int joy_watchdog (unsigned char *dataIn, unsigned char *dataOut);

/* Local Variables */
static VAR *speedVar = NULL;
static time_t lastCommandTime = 0;
static unsigned int currentButtons = 0;
static int currentX = 0;
static int currentY = 0;

#define DEFAULT_PORT 16000

static SSCommandTable joyCommandTable[] = {
//  {"LR", 6, 1, absoluteSpeedControl}, // Left/Right Absolute Speed Control
  {"SD", 8, 1, joy_absoluteSpeedControl},	// Speed/Direction Absolute Speed Control
  {"ST", 0, 1, joy_stopMotors},	// Stop
  {"DM", 0, 1, joy_disableMotors},	// Disable Motors
  {"ON", 0, 1, joy_enableMotors},	// Disable Motors
  {NULL, 0, 0, joy_watchdog}
};

/** A wrapper to open a socket then call ptzController
 * \param arg unused. BUG: this should be used to specify (at least) the port.
 */
void *
joystickServer (void *arg)
{
  int create_socket, clientSocket, addrlen;
  unsigned int port, socketType;
  int done;

  speedVar = arg;

  port = DEFAULT_PORT;
  socketType = SOCK_DGRAM;

  create_socket = createNewSocket (socketType, NULL, port);

  if (socketType == SOCK_STREAM)
    listen (create_socket, 1);

  addrlen = sizeof (struct sockaddr_in);
  done = 0;
  while (done >= 0)
    {
      if (socketType == SOCK_STREAM)
	{
	  clientSocket = accept (create_socket, NULL, NULL);
	  debug_print (minVerbLevel (2), "Accepting connection\n");
	}
      else
	{
	  clientSocket = create_socket;
	}

      done = simpleServer (socketType, clientSocket, joyCommandTable);

      if (socketType == SOCK_STREAM)
	{
	  debug_print (minVerbLevel (2), "Closing connection\n");
	  shutdown (clientSocket, SHUT_RDWR);
	  close (clientSocket);
	}
    }
  close (create_socket);
  debug_print (minVerbLevel (2), "Exiting server\n");

  return (NULL);
}

static int
joy_absoluteSpeedControl (unsigned char *dataIn, unsigned char *dataOut)
{
  char buffer[7];
  char convbuff[3];
  signed int left, right;
  int position;
  int tiltAngle, panAngle, angle, zoom;
  int i;
  unsigned int value;
  int ptChanged = 0;

  lastCommandTime = time (NULL);

  // Convert from Ascii to binary data
  convbuff[2] = '\0';
  for (i = 0; i < 3; i++)
    {
      convbuff[0] = dataIn[i * 2];
      convbuff[1] = dataIn[i * 2 + 1];
      value = strtoul (convbuff, NULL, 16);
      buffer[i] = value;
    }

  currentButtons = buffer[0];
  currentY = buffer[1];
  currentX = buffer[2];

  debug_print (minVerbLevel (1), "Set X Y to %4d, %4d (buttons = %02X)\n",
	       currentX, currentY, currentButtons);
  left = (2 * currentY + currentX) / 16;
  right = (2 * currentY - currentX) / 16;

  if (currentButtons & 0x01)
    joystickSetMotorSpeeds (left, right);

  if (dataOut)
    {
      dataOut[0] = '0';
      dataOut[1] = '0';
      return (2);
    }
  else
    return (0);
}

static int
joy_stopMotors (unsigned char *dataIn, unsigned char *dataOut)
{
  lastCommandTime = time (NULL);

  joystickSetMotorSpeeds (0, 0);
  if (dataOut)
    {
      dataOut[0] = '0';
      dataOut[1] = '0';
      return (2);
    }
  else
    return (0);
}

static int
joy_disableMotors (unsigned char *dataIn, unsigned char *dataOut)
{
  lastCommandTime = time (NULL);

  debug_print (minVerbLevel (1), "Disabling Motor Power\n");
  joystickSetMotorSpeeds (0, 0);
  setMotorPower (0);
  if (dataOut)
    {
      dataOut[0] = '0';
      dataOut[1] = '0';
      return (2);
    }
  else
    return (0);
}


static int
joy_enableMotors (unsigned char *dataIn, unsigned char *dataOut)
{
  lastCommandTime = time (NULL);

  debug_print (minVerbLevel (1), "Enabling Motor Power\n");

  joystickSetMotorSpeeds (0, 0);
  currentX = 0;
  currentY = 0;

  setMotorPower (1);
  if (dataOut)
    {
      dataOut[0] = '0';
      dataOut[1] = '0';
      return (2);
    }
  else
    return (0);
}

static int
joy_watchdog (unsigned char *dataIn, unsigned char *dataOut)
{
  time_t currentTime;
  int result = 0;
  int power, buttons;
  static int buttonHoldTime = 0;

  currentTime = time (NULL);
  if (lastCommandTime)
    {
      if ((currentTime - lastCommandTime) > 1)
	{
	  debug_print (minVerbLevel (1),
		       "CONNECTION TIMED OUT: Setting X Y to 0 0\n");
	  joystickSetMotorSpeeds (0, 0);
	  setMotorPower (0);
	  lastCommandTime = 0;
	  result = -1;		// its been too long since the last check, disconnect
	}
    }
  return (result);
}

int
joystickSetMotorSpeeds (int left, int right)
{
  return (setSpeedVars (speedVar, left, right, 1000, JOYSTICK_PRIORITY));
}
