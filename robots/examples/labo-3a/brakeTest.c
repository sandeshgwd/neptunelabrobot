/*
 */

#include <stdio.h>
#include <signal.h>
#include "mbuff.h"		/* Required for shared memory interface */
#include "labo2.h"		/* Contains MOTOR structure definition */

/* Function Prototype for our signal handler */
void sigHandler (int dummy);

/* the contents of shared memory may change at any time, thus volatile */
static volatile char *motorSharedMem;

int
main ()
{
  /* These are local copies of the shared variables
   * We use the local copies from all calculation and
   * update the shared data only as needed
   */
  int actualSpeed[NUM_MOTORS];
  int desiredSpeed[NUM_MOTORS];
  int ramp[NUM_MOTORS];
  int i;
  int command;
  MOTOR *motor;			/* This will point to the shared memory */

  /* Get the shared memory for the motor controller */
  motorSharedMem =
    (volatile char *) mbuff_attach ("motor", sizeof (struct MOTOR_S));
  if (motorSharedMem == NULL)
    {
      printf ("mbuff_alloc failed\n");
      exit (-1);
    }

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly
   */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  /* The motor structure occupies the first (and only) part of shared memory */
  motor = (struct MOTOR_S *) motorSharedMem;

  /* Use local variables to do calculations */
  for (i = 0; i < NUM_MOTORS; i++)
    {
      desiredSpeed[i] = 0;

      motor->controlType[i] = SPEED;

      /* synchronize shared and local variables */
      actualSpeed[i] = motor->actualSpeed[i];
      motor->desiredSpeed[i] = desiredSpeed[i];
    }

  motor->command = 0x00;
  usleep (10000);
  motor->power = 1;
  while (1)
    {
      motor->command = 0x33;	/* brake off all 4 motors */
      if (!command)
	command = 1;
      else
	command <<= 1;
      usleep (100000);
    }
}

/* On critical signals, we should detach from shared memory */
void
sigHandler (int dummy)
{
  int i;
  MOTOR *motor;
  motor = (struct MOTOR_S *) motorSharedMem;

  for (i = 0; i < NUM_MOTORS; i++)
    motor->desiredSpeed[i] = 0;	/* stop motor */
  motor->command = 0x00;	/* brake on */
  motor->power = 0x00;		/* turn off relay */
  sleep (1);
  mbuff_detach ("motor", (void *) motorSharedMem);
  exit (-1);
}
