/*
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <math.h>
#include <time.h>

#define BAUDRATE B9600
#define _POSIX_SOURCE 1		/* POSIX compliant source */
#define FALSE 0
#define TRUE 1

static struct termios oldtio, newtio;

int
serOpen (char *device)
{
  int fd;

  fd = open (device, O_RDWR | O_NOCTTY);
  if (fd < 0)
    {
      perror (device);
      return -1;
    }

  tcgetattr (fd, &oldtio);	/* save current port settings */

  bzero (&newtio, sizeof (newtio));
  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR;
  newtio.c_oflag = 0;

  /* set input mode (non-canonical, no echo,...) */
  newtio.c_lflag = 0;

  newtio.c_cc[VTIME] = 1;	/* 1 second time out */
  newtio.c_cc[VMIN] = 0;	/* blocking read until 5 chars received */

  tcflush (fd, TCIFLUSH);
  tcsetattr (fd, TCSANOW, &newtio);

  return (fd);
}

void
serClose (fd)
{
/*  tcsetattr (fd, TCSANOW, &oldtio); */
  close (fd);
}


#define DEFAULT_SERIAL_PORT "/dev/ttyS0"

int
main (int argc, char **argv)
{
  int i;
  char sonar[8], *incoming;
  int fd = -1;
  int count;

  if (argc > 1)
    fd = serOpen (argv[1]);
  else
    fd = serOpen (DEFAULT_SERIAL_PORT);

  if (fd < 0)
    {
      fprintf (stderr, "Could not open serial port\n");
      exit (-1);
    }
  fprintf (stderr, "Opened serial port\n");
  count = 0;
  for (i = 0; i < 8; i++)
    sonar[i] = 0;
  while (1)
    {
      if (read (fd, sonar, 1) != 0)
	{
	  if (sonar[0] == 0)
	    {
	      read (fd, sonar, 8);
	    }
	}
      if (count++ > 4)
	{
	  count = 0;
	  printf ("%d %d %d %d %d %d %d\n", sonar[1], sonar[2], sonar[3],
		  sonar[4], sonar[5], sonar[6], sonar[7]);
	}
      usleep (40000);
    }
  serClose (fd);

  return 0;
}
