/************************************************************
(c) Applied AI Systems, Inc. 1998

 Original version based on khep v1.26
************************************************************/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include "userlcd.h"
#include "userir.h"
#include "usermotor.h"
#include "labo2.h"

void sigHandler (int dummy);
void setSpeed (int left, int right);

/* Threads */
void *avoid (void *arg);
void *bump (void *arg);
void *buttons (void *arg);

short enableAvoid;

int
get_key ()
{
  return (motor->buttons);
}

void
initMotors (MOTOR * motor)
{
  int i;

  motor->command = 0;
  motor->power = 0;
  for (i = 0; i < NUM_MOTORS; i++)
    {
      motor->controlType[i] = SPEED;	//position or speed control
      motor->desiredSpeed[i] = 0;
      motor->actualSpeed[i] = 0;
      motor->desiredPosition[i] = 0;
      motor->actualPosition[i] = 0;
    }
}

void *
buttons (void *arg)
{
  short key, lastKey;

  lastKey = get_key ();
  while (1)
    {
      key = get_key ();
      if (key != lastKey)
	{
	  putc (key + '0', stderr);
	  if (key == 0)
	    {
	      if (lastKey == 1)	/* Left button pressed */
		{
		  enableAvoid = 1;
		  lcdclr ();
		  lcdputs ("Avoiding");
		}
	      else if (lastKey == 2)	/* right button pressed */
		{
		  enableAvoid = -1;
		  lcdclr ();
		  lcdputs ("Waiting");
		  setSpeed (0, 0);
		}
	      else if (lastKey == 3)	/* both buttons pressed */
		{
		}
	    }
	  lastKey = key;
	}
      usleep (100000);
    }
  return (NULL);
}

void *
display (void *arg)
{
  volatile int *mode;
  char lcdString[33];

  mode = (volatile int *) arg;

  bzero (lcdString, 33);
  while (1)
    {
      switch (*mode)
	{
	case 0:
	  lcdclr ();
	  snprintf (lcdString, 33, "%3d %3d", motor->desiredSpeed[0],
		    motor->desiredSpeed[1]);
	  lcdputs (lcdString);
	  break;
	default:
	  break;
	}
      sleep (1);
    }
}

int
main (int argc, char **argv)
{
  pthread_t avoidThread, bumpThread, buttonsThread, displayThread;
  int err;
  int displayMode;

  if ((err = lcdopen (0)) < 0)
    exit (0);
  lcdclr ();

  lcdgoto (1, 15);

  lcdclr ();
  lcdputs ("Labo 3");
  lcdgoto (0, 13);
  lcdputs ("AAI");
  lcdgoto (1, 0);
  lcdputs ("Avoid");

// Old shared memory intialization
#if 0
  sensorsSharedMem = (char *) mbuff_attach ("sensors", sizeof (SENSOR));
  if (sensorsSharedMem == NULL)
    {
      printf ("mbuff_alloc failed: sensors\n");
      exit (-1);
    }
  sensors = (SENSOR *) sensorsSharedMem;

  motorSharedMem = (char *) mbuff_attach ("motor", sizeof (MOTOR));
  if (motorSharedMem == NULL)
    {
      printf ("mbuff_alloc failed: motor\n");
      exit (-1);
    }
  motor = (MOTOR *) motorSharedMem;
#endif

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly
   */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  if (initMotors ())
	exit(-1);
  if (initIR ())
	exit(-1);
  if (lcdinit ())
	exit(-1);

  usleep (250000);

  setMotorPower (0)
  setBrakes (0)		/* brake off all 4 motors */

  puts ("Adding Avoid task\r\n");
  if ((err = pthread_create (&avoidThread, NULL, avoid, sensors->irs)))
    {
      perror ("sensor thread creation failed");
      exit (-1);
    }
  if ((err = pthread_create (&bumpThread, NULL, bump, &(sensors->bumper))))
    {
      perror ("bumper thread creation failed");
      exit (-1);
    }
  if ((err =
       pthread_create (&buttonsThread, NULL, buttons, &(motor->buttons))))
    {
      perror ("button thread creation failed");
      exit (-1);
    }

  displayMode = 0;
  if ((err = pthread_create (&displayThread, NULL, display, &displayMode)))
    {
      perror ("lcd thread creation failed");
      exit (-1);
    }

  enableAvoid = 1;
  while (1)
    {
      sleep (4);
    }
}

void *
avoid (void *arg)
{
  short i;
  volatile int *ir;
  int irMax[10];
  int leftCoefficient[10];
  int rightCoefficient[10];
  int leftSpeed, rightSpeed;
  int counter;

  puts ("Avoid Started\r\n");

  ir = (volatile int *) arg;

  for (i = 0; i < 10; i++)
    {
      irMax[i] = 0;
      leftCoefficient[i] = 0;
      rightCoefficient[i] = 0;
    }

  leftCoefficient[0] = 6;
  leftCoefficient[1] = 3;
  leftCoefficient[2] = -3;
  leftCoefficient[5] = 6;
  leftCoefficient[6] = 9;
  leftCoefficient[7] = 3;

  leftCoefficient[3] = -2;
  leftCoefficient[4] = -2;
  leftCoefficient[9] = -4;
  leftCoefficient[8] = -4;

  rightCoefficient[0] = 6;
  rightCoefficient[1] = 9;
  rightCoefficient[2] = 3;
  rightCoefficient[5] = 6;
  rightCoefficient[6] = 3;
  rightCoefficient[7] = -3;

  rightCoefficient[3] = -4;
  rightCoefficient[4] = -4;
  rightCoefficient[9] = -2;
  rightCoefficient[8] = -2;


  leftSpeed = 0;
  rightSpeed = 0;
  enableAvoid = 0;

  counter = 32;

  while (1)
    {
      leftSpeed = 0;
      rightSpeed = 0;

      for (i = 0; i < 10; i++)
	{
	  leftSpeed += ir[i] * leftCoefficient[i];
	  rightSpeed += ir[i] * rightCoefficient[i];
	}
      leftSpeed >>= 6;
      rightSpeed >>= 6;

      if (enableAvoid > 0)
	{
	  setSpeed (leftSpeed, rightSpeed);
	}
      usleep (10000);
    }
  return (NULL);
}

void *
bump (void *arg)
{
  short previousSetting;
  volatile int *bumper;

  puts ("Bump Started\r\n");

  bumper = (volatile int *) arg;
  while (1)
    {
      if ((*bumper & 0xFF) != 0xFF)
	{
	  previousSetting = enableAvoid;
	  enableAvoid = 0;
	  setSpeed (0, 0);
	  motor->command &= 0xCC;	/* disable the motors */
	  while ((*bumper & 0xFF) != 0xFF)
	    ;
	  sleep (2);
	  motor->command |= 0x33;	/* enable the motors */
	  if (enableAvoid < 0)
	    enableAvoid = 0;
	  else
	    enableAvoid = previousSetting;
	}
      usleep (100000);
    }
  return (NULL);
}

void
setSpeed (int left, int right)
{
#if NUM_MOTORS > 0
  motor->controlType[0] = SPEED;
  motor->desiredSpeed[0] = left;
#endif
#if NUM_MOTORS > 1
  motor->controlType[1] = SPEED;
  motor->desiredSpeed[1] = right;
#endif
}

/* On critical signals, we should detach from shared memory */
void
sigHandler (int dummy)
{
  int i;

  fprintf (stderr, "Signal caught, stopping...\n");
  lcdclose ();

  for (i = 0; i < NUM_MOTORS; i++)
    motor->desiredSpeed[i] = 0;	/* stop motor */
  motor->command = 0x00;	/* brake on */

  mbuff_detach ("motor", (void *) motorSharedMem);
  mbuff_detach ("sensors", (void *) sensorsSharedMem);

  exit (-1);
}
