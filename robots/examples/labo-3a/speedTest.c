/*
 */

#include <stdio.h>
#include <signal.h>
#include "mbuff.h"		/* Required for shared memory interface */
#include "labo2.h"		/* Contains MOTOR structure definition */

/* Function Prototype for our signal handler */
void sigHandler (int dummy);

/* the contents of shared memory may change at any time, thus volatile */
static volatile char *motorSharedMem;

#define STEP_SIZE 5

int
main ()
{
  /* These are local copies of the shared variables
   * We use the local copies from all calculation and
   * update the shared data only as needed
   */
  int actualSpeed[NUM_MOTORS];
  int desiredSpeed[NUM_MOTORS];
  int ramp[NUM_MOTORS];
  int i;
  MOTOR *motor;			/* This will point to the shared memory */

  /* Get the shared memory for the motor controller */
  motorSharedMem =
    (volatile char *) mbuff_attach ("motor", sizeof (struct MOTOR_S));
  if (motorSharedMem == NULL)
    {
      printf ("mbuff_alloc failed\n");
      exit (-1);
    }

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly
   */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  /* The motor structure occupies the first (and only) part of shared memory */
  motor = (struct MOTOR_S *) motorSharedMem;

  /* Use local variables to do calculations */
  for (i = 0; i < NUM_MOTORS; i++)
    {
      desiredSpeed[i] = 0;

      motor->controlType[i] = SPEED;

      /* synchronize shared and local variables */
      actualSpeed[i] = motor->actualSpeed[i];
      motor->desiredSpeed[i] = desiredSpeed[i];
    }

  motor->brake = 1;
  usleep (10000);
  motor->power = 1;
  motor->brake = 0;	/* brake off all 4 motors */

#if NUM_MOTORS > 0
  ramp[0] = STEP_SIZE;
  desiredSpeed[0] = 0;
#endif
#if NUM_MOTORS > 1
  ramp[1] = STEP_SIZE;
  desiredSpeed[1] = 0;
#endif
#if NUM_MOTORS > 2
  ramp[2] = STEP_SIZE;
  desiredSpeed[2] = 0;
#endif
#if NUM_MOTORS > 3
  ramp[3] = STEP_SIZE;
  desiredSpeed[3] = 0;
#endif

  while (1)
    {
      printf ("Speed:");
      for (i = 0; i < NUM_MOTORS; i++)
	{
	  desiredSpeed[i] += ramp[i];
	  if (desiredSpeed[i] > 50)
	    {
	      desiredSpeed[i] = 50;
	      ramp[i] = -STEP_SIZE;
	    }
	  if (desiredSpeed[i] < -50)
	    {
	      desiredSpeed[i] = -50;
	      ramp[i] = STEP_SIZE;
	    }

	  /* Only update the shared variables _AFTER_ all necessary calcultions */

	  motor->desiredSpeed[i] = desiredSpeed[i];
	  printf (" %d", desiredSpeed[i]);
	}
      printf ("\n");
      getchar ();
      usleep (400000);
    }

  /* release the shared memory */
  motor->desiredSpeed[i] = 0;	/* stop motor */
  motor->brake = 1;	/* brake on */
  sleep (1);
  mbuff_detach ("motor", (void *) motorSharedMem);
}

/* On critical signals, we should detach from shared memory */
void
sigHandler (int dummy)
{
  int i;
  MOTOR *motor;
  motor = (struct MOTOR_S *) motorSharedMem;

  for (i = 0; i < NUM_MOTORS; i++)
    motor->desiredSpeed[i] = 0;	/* stop motor */
  motor->brake = 1;	/* brake on */
  sleep (1);
  mbuff_detach ("motor", (void *) motorSharedMem);
  exit (-1);
}
