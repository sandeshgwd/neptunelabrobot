/*
 */

#include <stdio.h>
#include <signal.h>
#include "mbuff.h"		/* Required for shared memory interface */
#include "labo2.h"		/* Contains MOTOR structure definition */

/* Function Prototype for our signal handler */
void sigHandler (int dummy);

/* the contents of shared memory may change at any time, thus volatile */
static volatile char *motorSharedMem;
int i = 0;

int
main ()
{
  /* These are local copies of the shared variables
   * We use the local copies from all calculation and
   * update the shared data only as needed
   */
  int actualPosition[NUM_MOTORS];
  int desiredPosition[NUM_MOTORS];
  int position;
  MOTOR *motor;			/* This will point to the shared memory */

  /* Get the shared memory for the motor controller */
  motorSharedMem =
    (volatile char *) mbuff_attach ("motor", sizeof (struct MOTOR_S));
  if (motorSharedMem == NULL)
    {
      printf ("mbuff_alloc failed\n");
      exit (-1);
    }

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly
   */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  /* The motor structure occupies the first (and only) part of shared memory */
  motor = (struct MOTOR_S *) motorSharedMem;

  /* Use local variables to do calculations */
  desiredPosition[i] = 0;

  motor->controlType[0] = POSITION;
  motor->controlType[1] = POSITION;

  /* synchronize shared and local variables */
  actualPosition[i] = motor->actualPosition[i];
  motor->desiredPosition[i] = desiredPosition[i];
  position = motor->actualPosition[i];
  motor->brake = 1;
  usleep(10000);
  motor->brake = 0;	/* brake off all 4 motors */

  motor->desiredPosition[i] = 0;	//position;

  /* release the shared memory */
  motor->desiredSpeed[i] = 0;	/* stop motor */
  motor->brake = 1;	/* brake on */
  sleep (1);
  mbuff_detach ("motor", (void *) motorSharedMem);
}

/* On critical signals, we should detach from shared memory */
void
sigHandler (int dummy)
{
  MOTOR *motor;
  motor = (struct MOTOR_S *) motorSharedMem;

  motor->desiredSpeed[i] = 0;	/* stop motor */
  motor->brake = 1;	/* brake on */
  sleep (1);
  mbuff_detach ("motor", (void *) motorSharedMem);
  exit (-1);
}
