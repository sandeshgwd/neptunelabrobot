/*
 */

#include <stdio.h>
#include <signal.h>
#include "mbuff.h"		/* Required for shared memory interface */
#include "motor.h"		/* Contains MOTOR structure definition */

/* Function Prototype for our signal handler */
void sigHandler (int dummy);

/* the contents of shared memory may change at any time, thus volatile */
static volatile char *motorSharedMem;

int
main ()
{
  /* These are local copies of the shared variables
   * We use the local copies from all calculation and
   * update the shared data only as needed
   */
  int desiredSpeed[NUM_MOTORS];
  int desiredPosition[NUM_MOTORS];
  int actualSpeed[NUM_MOTORS];
  int actualPosition[NUM_MOTORS];
  int lastActualSpeed[NUM_MOTORS];
  int lastActualPosition[NUM_MOTORS];
  int change;
  int i = 0;

  MOTOR *motor;			/* This will point to the shared memory */
  int error;

  /* Get the shared memory for the motor controller */
  motorSharedMem =
    (volatile char *) mbuff_attach ("motor", sizeof (struct MOTOR_S));
  if (motorSharedMem == NULL)
    {
      printf ("mbuff_alloc failed\n");
      exit (-1);
    }

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly
   */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  /* The motor structure occupies the first (and only) part of shared memory */
  motor = (struct MOTOR_S *) motorSharedMem;

  for (i = 0; i < NUM_MOTORS; i++)
    {
      lastActualPosition[i] = 0;
    }
  error = 0;
  change = 1;
  while (1)
    {
      /* synchronize local variables with the shared variables */

      for (i = 0; i < NUM_MOTORS; i++)
	{
	  desiredSpeed[i] = motor->desiredSpeed[i];
	  actualSpeed[i] = motor->actualSpeed[i];
	  desiredPosition[i] = motor->desiredPosition[i];
	  actualPosition[i] = motor->actualPosition[i];
	  if (actualPosition[i] != lastActualPosition[i])
	    change++;
	  lastActualPosition[i] = actualPosition[i];
	}
      i = 0;


      if (change)
	{
	  printf ("Des/Act %6d %6d \t\tDes/Act %6d %6d\n", desiredPosition[0],
		  actualPosition[0], desiredPosition[1], actualPosition[1]);
	}
      change = 0;

//    printf ("lastcount %d %d\n ", motor->govalue[i], motor->govalue[i+1]);
//    printf ("nowcount %d %d\n\n", motor->fbcontroltype[i], motor->fbcontroltype[i+1]);
      usleep (10000);
    }

  /* release the shared memory */
  mbuff_detach ("motor", (void *) motorSharedMem);
}

/*
 * sigHandler (int dummy)
 * Standard signal handler.
 * On critical signals, we should detach from shared memory
 */
void
sigHandler (int dummy)
{
  mbuff_detach ("motor", (void *) motorSharedMem);
  exit (-1);
}
