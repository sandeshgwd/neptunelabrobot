/*
 */

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include "userlcd.h"
#include <lcd.h>

void sigHandler (int dummy);

int
main ()
{
  if (lcdopen (0))
    printf ("can't open lcd\n");

  while (1)
    {
      lcdputs ("Hello World!");
      sleep (1);
    }

  lcdclose ();
  exit (0);
}

void
sigHandler (int dummy)
{
  lcdclose ();
  exit (-1);
}
