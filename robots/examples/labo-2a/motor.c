/*
 */

#include <stdio.h>
#include <signal.h>
#include "mbuff.h"		/* Required for shared memory interface */
#include "analogMotor.h"	/* Contains MOTOR structure definition */

/* Function Prototype for our signal handler */
static void sigHandler (int dummy);
static void cleanup (void);

/* the contents of shared memory may change at any time, thus volatile */
static volatile char *motorSharedMem = NULL;

int
initMotors ()
{
  ANALOG_MOTOR *motor;		/* This will point to the shared memory */
  /* Get the shared memory for the motor controller */
  if (!motorSharedMem)
    {
      motorSharedMem =
	(volatile char *) mbuff_attach ("analogMotor",
					sizeof (struct ANALOG_MOTOR_S));
      if (motorSharedMem == NULL)
	{
	  printf ("mbuff_alloc failed\n");
	  return (-1);
	}
      atexit (cleanup);

      /* The motor structure occupies the first (and only) part of shared memory */
      motor = (struct ANALOG_MOTOR_S *) motorSharedMem;
      motor->command = 0xFF;	/* X-Y control rather than L,R control */
    }
  return (0);
}

void
disableMotors (void)
{
  ANALOG_MOTOR *motor;		/* This will point to the shared memory */
  motor = (struct ANALOG_MOTOR_S *) motorSharedMem;
  motor->command = 0xFF;	/* X-Y control rather than L,R control */
  motor->command = 0;
}

void
enableMotors (void)
{
  ANALOG_MOTOR *motor;		/* This will point to the shared memory */
  motor = (struct ANALOG_MOTOR_S *) motorSharedMem;
  motor->command = 0xFF;	/* X-Y control rather than L,R control */
  motor->command = 0xFF;
}

void
releaseMotor ()
{
  ANALOG_MOTOR *motor;		/* This will point to the shared memory */
  motor = (struct ANALOG_MOTOR_S *) motorSharedMem;
  motor->command = 0;
  mbuff_detach ("analogMotor", (void *) motorSharedMem);
}

int
setMotorSpeed (int motorNum, int speed)
{
  ANALOG_MOTOR *motor;		/* This will point to the shared memory */
  motor = (struct ANALOG_MOTOR_S *) motorSharedMem;
  if (motorNum < NUM_MOTORS)
    {
//      motor->controlMode[motorNum] = 0;       /* Speed control */
      motor->desiredSpeed[motorNum] = speed;
      return (0);
    }
  else
    return (-1);
}

int
getMotorSpeed (int motorNum, int speed)
{
  ANALOG_MOTOR *motor;		/* This will point to the shared memory */
  int actualSpeed = 0;

  motor = (struct ANALOG_MOTOR_S *) motorSharedMem;
  if (motorNum < NUM_MOTORS)
    {
      actualSpeed = motor->actualSpeed[motorNum];
    }
  return (actualSpeed);
}

/* On critical signals, we should detach from shared memory */
static void
cleanup (void)
{
  mbuff_detach ("analogMotor", (void *) motorSharedMem);
  motorSharedMem = NULL;
}
