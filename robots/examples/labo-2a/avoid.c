/*
 * Wheelchair Controller
 * Initial coding started 1998.01.21 - by Rob McConnell
 */

#include <stdio.h>
#include <signal.h>
#include <userir.h>
#include <usermotor.h>
#include <userlcd.h>

#include "labo2.h"

#define FULL_FORWARD   0xC0
#define MID_FORWARD    0xB0
#define HALF_FORWARD   0xA0
#define STOP	       0x80
#define HALF_REVERSE   0x65
#define MID_REVERSE    0x55
#define FULL_REVERSE   0x40

#define FAR		0xE0
#define HIT		0xA0
#define BACKUP  40		/* irAvoid, bumpAvoid, setWheel are called every 0.01 sec */
					/* BACKUP: go backward about 1 sec */
/*
 * Global variables
 */
struct MOTORS
{
  unsigned int desiredLeftSpeed;
  unsigned int desiredRightSpeed;
  unsigned int left_motor;
  unsigned int right_motor;
}
motors;

int ir_temp[12];
unsigned int bumper = 0;

void sigHandler (int dummy);

void
irAvoid (char *arg)
{

/*
	The positions of infrared sensors
        reading values are in readData->irs[x]   , x = 0 ~ 11;
 	    2    1   0   5    6   7
   3  ____ -----------------____   8
    /                            \
   |                              |
   |                              |
   |                              |
   |                              |
   |                              |
   |  rightSpeed        leftSpeed |
   |                              |
   |                              |
   |                              |
  4|                              |9 
   |                              |
    |                            |
     |                          |
      |                        |
       \______________________/
         11 			    10
*/

  short a, irstatus;		/* for debugging */
  static short leftSpeed = STOP, rightSpeed = STOP, backup = 0;
  static short turnflag;

  irstatus = 0;
  getIRs(ir_temp);

  if ((readData->irs[0] > FAR) && (readData->irs[1] > FAR)
      && (readData->irs[5] > FAR) && (readData->irs[6] > FAR))
    {
      leftSpeed = MID_FORWARD;
      rightSpeed = MID_FORWARD;
    }
  else
    {
      leftSpeed = HALF_FORWARD;	/* default value */
      rightSpeed = HALF_FORWARD;
    }
  for (a = 10; a > 4; a--)
    {
      if (readData->irs[a] <= HIT)
	{
	  ir_temp[a] = ACTIVE;
	  lcdgoto (0, a + 1);
	  lcdputc ('1');
	}
      else
	{
	  ir_temp[a] = INACTIVE;
	  lcdgoto (0, a + 1);
	  lcdputc ('0');
	}
    }
  for (a = 0; a < 5; a++)
    {
      if (readData->irs[a] <= HIT)
	{
	  ir_temp[a] = ACTIVE;
	  lcdgoto (0, 5 - a);
	  lcdputc ('1');
	}
      else
	{
	  ir_temp[a] = INACTIVE;
	  lcdgoto (0, 5 - a);
	  lcdputc ('0');
	}
    }

  if (readData->irs[11] <= HIT)
    {				/* back #12 IR */
      ir_temp[11] = ACTIVE;
      lcdgoto (0, 0);
      lcdputc ('1');
    }
  else
    {
      ir_temp[11] = INACTIVE;
      lcdgoto (0, 0);
      lcdputc ('0');
    }
  if ((ir_temp[10] | ir_temp[11]) == ACTIVE)
    {
      leftSpeed = FULL_FORWARD;
      rightSpeed = FULL_FORWARD;
      irstatus = 1;
    }
  if (ir_temp[3] == ACTIVE)
    {
      rightSpeed = MID_FORWARD;
      leftSpeed = STOP;
      irstatus = 2;
    }
  if (ir_temp[2] == ACTIVE)
    {
      rightSpeed = MID_FORWARD;
      leftSpeed = STOP;
      irstatus = 3;
    }

  if (ir_temp[8] == ACTIVE)
    {
      rightSpeed = STOP;
      leftSpeed = MID_FORWARD;
      irstatus = 5;
    }
  if (ir_temp[7] == ACTIVE)
    {
      rightSpeed = STOP;
      leftSpeed = MID_FORWARD;
      irstatus = 6;
    }

  if ((ir_temp[6] | ir_temp[5]) == ACTIVE)
    {
      leftSpeed = HALF_REVERSE;
      rightSpeed = HALF_FORWARD;
      turnflag = -1;
    }
  if ((ir_temp[1] | ir_temp[0]) == ACTIVE)
    {
      leftSpeed = HALF_FORWARD;
      rightSpeed = HALF_REVERSE;
      turnflag = 1;
    }

  if ((ir_temp[0] & ir_temp[5]) == ACTIVE)
    {
      backup = BACKUP + BACKUP / 2;
      irstatus = 4;
    }

  if (backup > BACKUP)
    {
      leftSpeed = HALF_REVERSE;
      rightSpeed = HALF_REVERSE;
      backup--;
      irstatus = 7;
    }
  if ((backup > 0) && (backup <= BACKUP))
    {
      if (turnflag == 1)
	{
	  leftSpeed = MID_REVERSE;
	  rightSpeed = MID_FORWARD;
	  irstatus = 8;
	}
      else
	{
	  leftSpeed = MID_FORWARD;
	  rightSpeed = MID_REVERSE;
	  irstatus = 9;
	}
      backup--;
    }
  if (turnflag != 0)
    {
      if ((ir_temp[9] | ir_temp[4]) == ACTIVE)
	{
	  backup = 0;
	  turnflag = 0;
	}
      if ((ir_temp[10] | ir_temp[11]) == ACTIVE)
	{
	  backup = 0;
	  turnflag = 0;
	}
    }


/* set wheel commands */
  motors.desiredLeftSpeed = leftSpeed;
  motors.desiredRightSpeed = rightSpeed;

  lcdgoto (1, 0);
  lcdputbyte (irstatus);
}

void
bumpAvoid (char *arg)
{
/*
	The positions of bumper sensors
        reading values are in readData.bumpers1;
	The number indicates bit position.

	3           2         1
   4  ____ -----------------____  0
    /                            \
   |  ____ -----------------____  | 
    /                            \
   |                              |
   |                              |
   |                              |
   |                              |
   |                              |
   |                              |
   |                              |
   |                              |
   |                              |
   |                              |  
   |                              |
    |                            |
     |                          |
      |                        |
       \______________________/
       \______________________/
         6         7        5

*/
  short right, rightFront, front, leftFront, left;
  short leftBack, back, rightBack;
  static short goBackward = 0, goForward = 0;
  static short backwardLeft = 0, backwardRight = 0;
  static short forwardLeft = 0, forwardRight = 0;
  short bp;
  bp = 0;			/* for debugging */
  /* set reading bumper values */
  if ((readData->bumper & 0x01) != 0x01)
    {
      right = 1;
      lcdgoto (1, 3);
      lcdputc ('1');
    }
  else
    {
      right = 0;
      lcdgoto (1, 3);
      lcdputc ('0');
    }
  if ((readData->bumper & 0x02) != 0x02)
    {
      rightFront = 1;
      lcdgoto (1, 4);
      lcdputc ('1');
    }
  else
    {
      rightFront = 0;
      lcdgoto (1, 4);
      lcdputc ('0');
    }
  if ((readData->bumper & 0x04) != 0x04)
    {
      front = 1;
      lcdgoto (1, 5);
      lcdputc ('1');
    }
  else
    {
      front = 0;
      lcdgoto (1, 5);
      lcdputc ('0');
    }
  if ((readData->bumper & 0x08) != 0x08)
    {
      leftFront = 1;
      lcdgoto (1, 6);
      lcdputc ('1');
    }
  else
    {
      leftFront = 0;
      lcdgoto (1, 6);
      lcdputc ('0');
    }
  if ((readData->bumper & 0x10) != 0x10)
    {
      left = 1;
      lcdgoto (1, 7);
      lcdputc ('1');
    }
  else
    {
      left = 0;
      lcdgoto (1, 7);
      lcdputc ('0');
    }
  if ((readData->bumper & 0x20) != 0x20)
    {
      rightBack = 1;
      lcdgoto (1, 8);
      lcdputc ('1');
    }
  else
    {
      rightBack = 0;
      lcdgoto (1, 8);
      lcdputc ('0');
    }
  if ((readData->bumper & 0x80) != 0x80)
    {
      back = 1;
      lcdgoto (1, 9);
      lcdputc ('1');
    }
  else
    {
      back = 0;
      lcdgoto (1, 9);
      lcdputc ('0');
    }
  if ((readData->bumper & 0x40) != 0x40)
    {
      leftBack = 1;
      lcdgoto (1, 10);
      lcdputc ('1');
    }
  else
    {
      leftBack = 0;
      lcdgoto (1, 10);
      lcdputc ('0');
    }
/* front bumpers hit  */
  if ((rightFront | front | leftFront) == 1)
    {
      goBackward = BACKUP;
    }
  if (right == 1)
    {
      backwardRight = BACKUP;
    }
  if (left == 1)
    {
      backwardLeft = BACKUP;
    }

/* back bumpers hit */
  if (back == 1)
    goForward = BACKUP;
  if (rightBack == 1)
    forwardLeft = BACKUP;
  if (leftBack == 1)
    forwardRight = BACKUP;

/* set wheels command */
  if (goBackward > 0)
    {
      motors.desiredLeftSpeed = HALF_REVERSE;
      motors.desiredRightSpeed = HALF_REVERSE;
      goBackward--;
      bp = 1;
    }
  if (backwardRight > 0)
    {
      motors.desiredLeftSpeed = HALF_REVERSE;
      motors.desiredRightSpeed = FULL_REVERSE;
      backwardRight--;
      bp = 2;
    }
  if (backwardLeft > 0)
    {
      motors.desiredLeftSpeed = FULL_REVERSE;
      motors.desiredRightSpeed = HALF_REVERSE;
      backwardLeft--;
      bp = 3;
    }
  if (goForward > 0)
    {
      motors.desiredLeftSpeed = MID_FORWARD;
      motors.desiredRightSpeed = MID_FORWARD;
      goForward--;
      bp = 6;
    }
  if (forwardRight > 0)
    {
      motors.desiredLeftSpeed = MID_FORWARD;
      motors.desiredRightSpeed = HALF_FORWARD;
      forwardRight--;
      bp = 7;
    }
  if (forwardLeft > 0)
    {
      motors.desiredLeftSpeed = HALF_FORWARD;
      motors.desiredRightSpeed = MID_FORWARD;
      forwardLeft--;
      bp = 8;
    }
}


void
setWheels (char *arg)
{
  short x, y;
  if (arg == RESET)
    return;

  if (abs (motors.desiredRightSpeed - motors.right_motor) > 30)
    {
      if (motors.desiredRightSpeed > motors.right_motor)
	motors.right_motor++;
      if (motors.desiredRightSpeed < motors.right_motor)
	motors.right_motor--;
    }
  if (abs (motors.desiredRightSpeed - motors.right_motor) > 15)
    {
      if (motors.desiredRightSpeed > motors.right_motor)
	motors.right_motor++;
      if (motors.desiredRightSpeed < motors.right_motor)
	motors.right_motor--;
    }
  if (abs (motors.desiredRightSpeed - motors.right_motor) > 7)
    {
      if (motors.desiredRightSpeed > motors.right_motor)
	motors.right_motor++;
      if (motors.desiredRightSpeed < motors.right_motor)
	motors.right_motor--;
    }
  if (motors.desiredRightSpeed > motors.right_motor)
    motors.right_motor++;
  if (motors.desiredRightSpeed < motors.right_motor)
    motors.right_motor--;

  if (abs (motors.desiredLeftSpeed - motors.left_motor) > 30)
    {
      if (motors.desiredLeftSpeed > motors.left_motor)
	motors.left_motor++;
      if (motors.desiredLeftSpeed < motors.left_motor)
	motors.left_motor--;
    }
  if (abs (motors.desiredLeftSpeed - motors.left_motor) > 15)
    {
      if (motors.desiredLeftSpeed > motors.left_motor)
	motors.left_motor++;
      if (motors.desiredLeftSpeed < motors.left_motor)
	motors.left_motor--;
    }
  if (abs (motors.desiredLeftSpeed - motors.left_motor) > 7)
    {
      if (motors.desiredLeftSpeed > motors.left_motor)
	motors.left_motor++;
      if (motors.desiredLeftSpeed < motors.left_motor)
	motors.left_motor--;
    }
  if (motors.desiredLeftSpeed > motors.left_motor)
    motors.left_motor++;
  if (motors.desiredLeftSpeed < motors.left_motor)
    motors.left_motor--;


  x = (motors.left_motor + motors.right_motor) / 2;
  y = (motors.right_motor - motors.left_motor) + STOP;
  lcdgoto (0, 14);
  lcdputbyte (x);
  lcdgoto (1, 14);
  lcdputbyte (y);
  wheel->desiredDirection = x;
  wheel->desiredSpeed = y;

}



int
main ()
{
  short done, counter = 0;
  char command;

/* initialization */
  if(initIR())
	exit(-1);
  if(initMotors())
	exit(-1);
  setMotorPower (1);
  lcdopen (0);

  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

/* motor speed set to STOP */
  motors.desiredLeftSpeed = STOP;
  motors.desiredRightSpeed = STOP;
  motors.left_motor = STOP;
  motors.right_motor = STOP;

/* stop the wheels */
  wheel->desiredSpeed = STOP;
  wheel->desiredDirection = STOP;
  wheel->actualSpeed = STOP;
  wheel->actualDirection = STOP;
  sleep (1);
  lcdclr ();
  lcdputs ("AAI LABO-2 ");
  lcdgoto (1, 0);
  lcdputs ("ready");

  wheel->command = 0x00;	/* OUTPUT enable signal for MOTORS */
  usleep (100000);		/* 0.1 sec sleep */
  wheel->command = 0xFF;	/* OUTPUT kill sw enable for MOTORS */
  done = 0;
  printf ("set motor enable \n");
  lcdclr ();

  command = RESET;

/* end initialization */

  sleep (1);			/* wait 1 second */

  command = SET;

  while (!done)
    {
      irAvoid (&command);
      bumpAvoid (&command);
      setWheels (&command);

#if 0
      if (counter++ > 4)
	{
	  counter = 0;
	  printf ("\n");
	  printf ("%3d ", readData->irs[9]);
	  printf ("%3d ", readData->irs[8]);
	  printf ("%3d ", readData->irs[7]);
	  printf ("%3d ", readData->irs[6]);
	  printf ("%3d ", readData->irs[5]);
	  printf ("%3d ", readData->irs[0]);
	  printf ("%3d ", readData->irs[1]);
	  printf ("%3d ", readData->irs[2]);
	  printf ("%3d ", readData->irs[3]);
	  printf ("%3d ", readData->irs[4]);
	  printf ("%3d ", readData->irs[11]);
	  printf ("%3d ", readData->irs[10]);
	  printf (": %3d ", wheel->desiredSpeed);
	  printf ("%3d  :", wheel->desiredDirection);
	  printf ("%3d\n", readData->bumper);
	}
#endif

      usleep (10000);		/* 1 usec * 10000 = 0.01 sec  sleep */
    }				/* irAvoid, bumpAvoid, setWheels are called every 0.01 second */

  wheel->desiredSpeed = STOP;
  wheel->desiredDirection = STOP;
  wheel->actualSpeed = STOP;
  wheel->actualDirection = STOP;
  sleep (1);
  exit (0);
}

void
sigHandler (int dummy)
{
  wheel->desiredSpeed = STOP;
  wheel->desiredDirection = STOP;
  wheel->actualSpeed = STOP;
  wheel->actualDirection = STOP;
  sleep (1);
  exit (-1);
}
