/*
 * By Cameron Morland, 2001-06-12
 * Uses sonar (ultrasonic) sensors to avoid collisions
 */

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include "sonar.h"
#include "userlcd.h"

/* Function Prototype for our signal handler */
void sigHandler (int dummy);

int
main (void)
{
  int i;
  int sonar;			/* sonar serial device */
  unsigned char buffer[8];	/* space for the raw sonar readings */
  unsigned int sonars[8];

  /*
   *                5
   *            _________
   *       6 _-~         ~-_ 4
   *      _-~               ~-_
   *     .                     . 
   *  7 .                       . 3
   *   .                         . 
   *  .                           .
   *  |                           |
   *  |           GAIA2           |
   *  |                           |
   *  |                           |
   *  |                           |
   *  |                           |
   *  |                           |
   *  |                           |
   *  +---------------------------+
   *       2                 1
   */

  lcdopen (0);

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  sonar = openPort ();


  while (1)
    {
      /* read and display sensors */
      readSonar (sonar, buffer);
      for (i = 0; i < 8; i++)
	{
	  sonars[i] = (sonars[i] + buffer[i]) / 2;
	}

      lcdgoto (0, 1);
      lcdputbyte (sonars[7]);
      lcdgoto (0, 4);
      lcdputbyte (sonars[6]);
      lcdgoto (0, 7);
      lcdputbyte (sonars[5]);
      lcdgoto (0, 10);
      lcdputbyte (sonars[4]);
      lcdgoto (0, 13);
      lcdputbyte (sonars[3]);
      lcdgoto (1, 5);
      lcdputbyte (sonars[2]);
      lcdgoto (1, 9);
      lcdputbyte (sonars[1]);

      for (i = 1; i < 8; i++)
	printf ("%X\t", buffer[i]);

      printf ("\n");

      usleep (10000);
    }

  serClose (sonar);

  return (0);
}

void
sigHandler (int dummy)
{
  exit (-1);
}
