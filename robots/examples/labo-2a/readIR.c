/*
 */

#include <stdio.h>
#include <signal.h>

#include "mbuff.h"
#include "labo2.h"
/* 
  bit0		brake0 for MOTOR1
  bit1		brake1 for MOTOR1
  bit2		brake0 for MOTOR2
  bit3		brake1 for MOTOR2
  bit4		direction0 for MOTOR1
  bit5		direction1 for MOTOR1
  bit6		direction0 for MOTOR2
  bit7		direction1 for MOTOR2
*/

/* the contents of shared memory may change at any time, thus volatile */
volatile char *sensorsSharedMem;

SENSOR *sensors;

void sigHandler (int dummy);

int
main ()
{
  int ir_baseHz, ir_modHz, i;

  sensorsSharedMem =
    (volatile char *) mbuff_attach ("sensors", sizeof (SENSOR));
  if (sensorsSharedMem == NULL)
    {
      printf ("mbuff_alloc failed\n");
      exit (-1);
    }
  lcdopen (0);

  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  sensors = (SENSOR *) sensorsSharedMem;

  ir_baseHz = 0x34;
  ir_modHz = 0x1c0;

  while (1)
    {
      printf ("ir: ");
      for (i = 0; i < 5; i++)
	{
	  lcdgoto (0, i * 3);
	  lcdputbyte (sensors->irs[i]);
	}
      for (i = 0; i < 5; i++)
	{
	  lcdgoto (1, i * 3);
	  lcdputbyte (sensors->irs[i + 5]);
	}
      lcdgoto (0, 15);
      lcdputc (((sensors->bumper >> 4) & 0x0F) >
	       9 ? ((sensors->bumper >> 4) & 0x0F) + 'A' -
	       10 : ((sensors->bumper >> 4) & 0x0F) + '0');
      lcdgoto (1, 15);
      lcdputc (((sensors->bumper) & 0x0F) >
	       9 ? ((sensors->bumper) & 0x0F) + 'A' -
	       10 : ((sensors->bumper) & 0x0F) + '0');

      for (i = 0; i < 12; i++)
	{
	  printf ("%3d ", sensors->irs[i]);

	}
      printf ("\nbumper: %2x\n", sensors->bumper);
      usleep (300000);
    }
  lcdclose ();
  mbuff_detach ("sensors", (void *) sensorsSharedMem);
}

void
sigHandler (int dummy)
{
  mbuff_detach ("sensors", (void *) sensorsSharedMem);
  exit (-1);
}
