/*
 */

#include <stdio.h>
#include <signal.h>
#include "mbuff.h"		/* Required for shared memory interface */
#include "analogMotor.h"	/* Contains MOTOR structure definition */

#define MAX_FORWARD_SPEED  20
#define MAX_BACKWARD_SPEED -10
#define RAMPL 2
#define RAMPR 2

void sigHandler (int dummy);

int
main ()
{
  /* These are local copies of the shared variables
   * We use the local copies from all calculation and
   * update the shared data only as needed
   */
  int actualL, actualR;
  int desiredL, desiredR;
  int rampL, rampR;

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly
   */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);
  initMotors ();

  /* The motor structure occupies the first (and only) part of shared memory */
  /* Use local variables to do calculations */
  desiredL = 0;
  desiredR = 0;

  rampL = RAMPL;
  rampR = 0;

  sleep (2);
  while (1)
    {
      desiredL += rampL;
      if (desiredL > MAX_FORWARD_SPEED)
	{
	  desiredL = MAX_FORWARD_SPEED;	// - (RAMPL + 1) / 2;
	  rampL = -RAMPL;
	}
      else if (desiredL < MAX_BACKWARD_SPEED)
	{
	  desiredL = MAX_BACKWARD_SPEED;
	  rampL = RAMPL;
	}
      else if (rampL && !desiredL)
	{
	  rampL = 0;
	  rampR = RAMPR;
	}

      desiredR += rampR;
      if (desiredR > MAX_FORWARD_SPEED)
	{
	  desiredR = MAX_FORWARD_SPEED;	// - (RAMPR + 1) / 2;
	  rampR = -RAMPR;
	}
      else if (desiredR < MAX_BACKWARD_SPEED)
	{
	  desiredR = MAX_BACKWARD_SPEED;
	  rampR = RAMPR;
	}
      else if (rampR && !desiredR)
	{
	  rampR = 0;
	  rampL = RAMPL;
	}

      /*
         setMotorSpeed (0, desiredL);
         setMotorSpeed (1, desiredR);
       */

      setMotorSpeed (0, 40);
      setMotorSpeed (1, 0);

      /* wait a second */
      sleep (2);
    }
}

/* On critical signals, we should detach from shared memory */
void
sigHandler (int dummy)
{
  setMotorSpeed (0, 0);
  setMotorSpeed (1, 0);
  disableMotors ();
  exit (0);
}

#if 0

/* Function Prototype for our signal handler */
void sigHandler (int dummy);

/* the contents of shared memory may change at any time, thus volatile */
static volatile char *motorSharedMem;
int
main ()
{
  /* These are local copies of the shared variables
   * We use the local copies from all calculation and
   * update the shared data only as needed
   */
  int actualY, actualX;
  int desiredY, desiredX;
  int rampY, rampX;
  ANALOG_MOTOR *motor;		/* This will point to the shared memory */

  /* Get the shared memory for the motor controller */
  motorSharedMem =
    (volatile char *) mbuff_attach ("analogMotor",
				    sizeof (struct ANALOG_MOTOR_S));
  if (motorSharedMem == NULL)
    {
      printf ("mbuff_alloc failed\n");
      exit (-1);
    }

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly
   */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  /* The motor structure occupies the first (and only) part of shared memory */
  motor = (struct ANALOG_MOTOR_S *) motorSharedMem;

  /* Use local variables to do calculations */
  desiredY = 128;
  desiredX = 128;

  /* synchronize shared and local variables */
  actualY = motor->actualY;
  actualX = motor->actualX;
  motor->desiredY = desiredY;
  motor->desiredX = desiredX;

  motor->command = 0;
#define RAMPY 16
#define RAMPX 16
  rampY = RAMPY;
  rampX = RAMPX;

  motor->command = 0xFF;	/* OUTPUT kill sw enable for MOTORS */
  sleep (2);
  while (1)
    {
      desiredX += rampX;
      if (desiredX > 255)
	{
	  desiredX = 256 - (RAMPX + 1) / 2;
	  rampX = -RAMPX;
	  desiredY += rampY;
	}
      else if (desiredX < 0)
	{
	  desiredX = 0;
	  rampX = RAMPX;
	  desiredY += rampY;
	}

      if (desiredY >= 255)
	{
	  desiredY = 256 - (RAMPY + 1) / 2;
	  rampY = -RAMPY;
	}
      else if (desiredY < 0)
	{
	  desiredY = 0;
	  rampY = RAMPY;
	}
      /* Only update the shared variables _AFTER_ all necessary calcultions */
      motor->desiredY = desiredY;
      motor->desiredX = desiredX;

      /* wait a second */
      sleep (2);
    }

  /* release the shared memory */
  mbuff_detach ("analogMotor", (void *) motorSharedMem);
}
#endif
