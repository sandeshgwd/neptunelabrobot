/*
 */

#include <stdio.h>
#include <signal.h>
#include "mbuff.h"		/* Required for shared memory interface */
#include "analogMotor.h"	/* Contains MOTOR structure definition */

/* the contents of shared memory may change at any time, thus volatile */
static volatile char *motorSharedMem;
void sigHandler (int dummy);

int
main ()
{
  /* These are local copies of the shared variables
   * We use the local copies from all calculation and
   * update the shared data only as needed
   */
  int actualY, actualX;
  int actualSpeed[4];
  int error, i;
  ANALOG_MOTOR *motor;

  initMotors ();

  motorSharedMem =
    (volatile char *) mbuff_attach ("analogMotor",
				    sizeof (struct ANALOG_MOTOR_S));
  if (motorSharedMem == NULL)
    {
      printf ("mbuff_alloc failed\n");
      return (-1);
    }

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly
   */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  /* The motor structure occupies the first (and only) part of shared memory */
  motor = (struct ANALOG_MOTOR_S *) motorSharedMem;
  lcdopen (0);

  printf ("%8s %8s %8s %8s %8s %8s\n", "Y", "X", "speed 0", "speed 1",
	  "speed 2", "speed 3");
  error = 0;
  while (1)
    {
      /* synchronize local variables with the shared variables */
      actualY = motor->actualY;
      actualX = motor->actualX;
      for (i = 0; i < 4; i++)
	{
	  actualSpeed[i] = getMotorSpeed (i);
	}
      printf ("%8d %8d %8d %8d %8d %8d\n", actualY, actualX, actualSpeed[0],
	      actualSpeed[1], actualSpeed[2], actualSpeed[3]);
      lcdgoto (0, 0);
      lcdputbyte (actualY);
      lcdgoto (0, 4);
      lcdputbyte (actualSpeed[0]);
      lcdgoto (0, 8);
      lcdputbyte (actualSpeed[1]);
      lcdgoto (1, 0);
      lcdputbyte (actualX);
      lcdgoto (1, 4);
      lcdputbyte (actualSpeed[2]);
      lcdgoto (1, 8);
      lcdputbyte (actualSpeed[3]);

      usleep (5000);
    }
  lcdclose ();

  /* release the shared memory */
  mbuff_detach ("analogMotor", (void *) motorSharedMem);
}

/*
 * sigHandler (int dummy)
 * Standard signal handler.
 * On critical signals, we should detach from shared memory
 */
void
sigHandler (int dummy)
{
  mbuff_detach ("analogMotor", (void *) motorSharedMem);
  lcdclose ();
  exit (0);
}
