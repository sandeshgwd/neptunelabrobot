#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include "debug.h"
#include "aaipacket.h"

#define MAX_DATA_SIZE 64
#define MAX_PACKET_SIZE 257

static pthread_mutex_t commandMutex = PTHREAD_MUTEX_INITIALIZER;
static sem_t newJoyStickValue;
static int desiredButtons = 0;
static int desiredX = 0;
static int desiredY = 0;
static int disableMotors = 0;

static unsigned char DefaultAddress[] = "192.168.2.140:16000u";	// hard code an IP for now
static int joyStop = 0;

void setJoyStickPosition (int newButtons, int newX, int newY);
void getJoyStickPosition (int *currentButtons, int *currentX, int *currentY);

void
stopJoyControlThread ()
{
  pthread_mutex_lock (&commandMutex);
  desiredButtons = 0;
  desiredX = 0;
  desiredY = 0;
  disableMotors = 5;
  joyStop = 1;
  pthread_mutex_unlock (&commandMutex);
  sem_post (&newJoyStickValue);
}

/** 
 * Sends control commands to robot
 * \param arg address and port as a string eg. "127.0.0.1:16000"
 * \return NONE 
 */
void *
joystickClient (void *arg)
{
  char *addressPort;
  int fd;
  int newButtons = 0, newX = 0, newY = 0;
  int newDisableMotors = 0;
  int result, writeRetries, readRetries;
  int packetSize, dataSize;
  unsigned int panCommand, tiltCommand, zoomCommand;
  unsigned char *cmd;
  unsigned char data[MAX_DATA_SIZE];
  unsigned char packet[MAX_PACKET_SIZE];
  unsigned char response[MAX_PACKET_SIZE];
  struct timeval timeout;
  fd_set readfds, writefds;
  static int sequenceNum = 0;
  int panPosition;
  int tiltPosition;
  int zoomPosition;
  int maxfd;

  sem_init (&newJoyStickValue, 0, 0);

  if (arg)
    addressPort = (char *) arg;
  else
    addressPort = DefaultAddress;

  debug_print (minVerbLevel (2), "Joy Connecting to %s\n", addressPort);

  fd = connectToServer (PF_INET, addressPort);

  if (fd >= 0)
    {
      setJoyStickPosition (0, 0, 0);
      debug_print (minVerbLevel (2), "Joy starting main loop\n");
      while (!joyStop)
	{
	  do
	    {
	      result = sem_wait (&newJoyStickValue);
	    }
	  while (result && (errno == EINTR));	/* sem_wait can return -1/errno=EINTR */

	  pthread_mutex_lock (&commandMutex);
	  newButtons = desiredButtons;
	  newX = desiredX;
	  newY = desiredY;
	  newDisableMotors = disableMotors;
	  if (disableMotors > 0)
	    disableMotors--;
	  else
	    disableMotors = 0;

	  pthread_mutex_unlock (&commandMutex);

	  // Clear the data bytes we plan to use 
	  memset (data, 0, 5);

	  if (newDisableMotors)
	    {
	      if (newDisableMotors > 0)
		{
		  debug_print (minVerbLevel (1),
			       "Sending Disable Motors Command (DM%d)\n",
			       newDisableMotors);
		  packetSize =
		    buildPacket (packet, MAX_PACKET_SIZE, "DM", data, 0);
		}
	      else
		{
		  debug_print (minVerbLevel (1),
			       "Sending Enable Motors Command (ON)\n");
		  packetSize =
		    buildPacket (packet, MAX_PACKET_SIZE, "ON", data, 0);
		}
	      debug_print (minVerbLevel (1), "sending packet %s\n", data);
	    }
	  else
	    {
	      debug_print (minVerbLevel (2), "Desired X, Y =  %d, %d\n", newX,
			   newY);
	      snprintf (data, MAX_DATA_SIZE, "%02X%02X%02X",
			newButtons & 0xFF, newY & 0xFF, newX & 0xFF);

	      // "SD" is the Speed and Direction command
	      packetSize =
		buildPacket (packet, MAX_PACKET_SIZE, "SD", data, 6);
	      debug_print (minVerbLevel (2), "sending packet %s\n", data);
	    }

	  // Clear out any packets waiting to be read before issuing new command
	  do
	    {
	      result = 0;
	      errno = 0;
	      FD_ZERO (&readfds);
	      FD_SET (fd, &readfds);
	      maxfd = fd;
	      timeout.tv_sec = 0;
	      timeout.tv_usec = 0;	// timeout immediately if no data waiting
	      result = select (maxfd + 1, &readfds, NULL, NULL, &timeout);
	      if (result < 0)
		{
		  if (errno == EINTR)
		    {
		      result = 1;
		      continue;
		    }
		  debug_perror (minVerbLevel (2),
				"Error waiting for write fd");
		  break;	// Socket probably closed
		}
	      else
		{
		  if (FD_ISSET (fd, &readfds))
		    {
		      result = read (fd, response, MAX_PACKET_SIZE);
		      if (result < 0)
			{
			  break;	// Socket probably closed, or
			  // no packet received yet on UDP connection
			}
		    }
		}
	    }
	  while (result);	// received data


	  // If we have a valid packet
	  if (packetSize > 0)
	    {

	      // Send the packet this many times
	      writeRetries = 1;
	      while (writeRetries--)
		{
		  FD_ZERO (&writefds);
		  FD_SET (fd, &writefds);
		  maxfd = fd;
		  timeout.tv_sec = 0;
		  timeout.tv_usec = 1000000;	// 0.1 seconds timeout

		  result =
		    select (maxfd + 1, NULL, &writefds, NULL, &timeout);
		  if (!result)
		    continue;	// Timed out
		  if (result < 0)
		    {
		      if (errno == EINTR)
			continue;
		      debug_perror (minVerbLevel (2),
				    "Error waiting for write fd");
		      break;	// Socket probably closed
		    }
		  else
		    {
		      if (FD_ISSET (fd, &writefds))
			{
			  result = write (fd, packet, packetSize);
			  //                        debug_print (minVerbLevel(2), "sent packet %s\n", packet);
			  if (result < 0)
			    {
			      debug_perror (minVerbLevel (2),
					    "Error sending command packet");
			      break;	// Socket probably closed
			    }
			}
		    }
		}

	      result = 0;
	      errno = 0;
	      readRetries = 1;
	      while (readRetries--)
		{
		  FD_ZERO (&readfds);
		  FD_SET (fd, &readfds);
		  maxfd = fd;
		  timeout.tv_sec = 0;
		  timeout.tv_usec = 100000;	// 0.1 seconds timeout
		  result = select (maxfd + 1, &readfds, NULL, NULL, &timeout);
		  if (!result)
		    continue;	// Timed out
		  if (result < 0)
		    {
		      if (errno == EINTR)
			continue;
		      debug_perror (minVerbLevel (2),
				    "Error waiting for write fd");
		      break;	// Socket probably closed
		    }
		  else
		    {
		      if (FD_ISSET (fd, &readfds))
			{
			  data[0] = 0;
			  result = read (fd, response, MAX_PACKET_SIZE);
			  //                                    debug_print (minVerbLevel(2), "received packet %s\n", response);
			  if (result < 0)
			    {
			      debug_perror (minVerbLevel (3),
					    "Error reading response packet");
			      break;	// Socket probably closed, or
			      // no packet received yet on UDP connection
			    }
			  // Store the response
			  // BUG: Are all successful response msgs ASCII "00"
			  if ((response[1] == '0') || (response[2] == '0'))
			    {
			      result = 0;
			      // Break, or keep trying?
			      break;	// we may still need to clear remaining reads before next write
			    }
			  else
			    {
			      debug_print (minVerbLevel (2),
					   "Joy reports that command failed %c%c (%02X%02X)\n",
					   response[23],
					   response[24],
					   response[23], response[24]);
			      break;
			    }
			}
		    }
		}

	    }
	  if (result)		// non-zero...function failed.
	    {
	      debug_print (minVerbLevel (2),
			   "Joy command failed (result = %d)\n", result);
//                break;
	    }
	}
    }

  sem_destroy (&newJoyStickValue);

  debug_print (minVerbLevel (2), "Joy thread exiting\n");

  disconnectFromServer (fd);
  pthread_exit (NULL);
}

void
setJoyStickPosition (int newButtons, int newX, int newY)
{
  pthread_mutex_lock (&commandMutex);
  desiredButtons = newButtons;
  desiredX = newX;
  desiredY = newY;
  pthread_mutex_unlock (&commandMutex);
  sem_post (&newJoyStickValue);
}

void
joyStickDisableMotors (int value)
{
  debug_print (minVerbLevel (1), "joyStickDisableMotors %d\n", value);
  pthread_mutex_lock (&commandMutex);
  desiredButtons = 0;
  desiredX = 0;
  desiredY = 0;
  disableMotors = value;
  pthread_mutex_unlock (&commandMutex);
  sem_post (&newJoyStickValue);
}

void
joyStickEnableMotors (int value)
{
  debug_print (minVerbLevel (1), "joyStickEnableMotors %d\n", value);
  pthread_mutex_lock (&commandMutex);
  desiredButtons = 0;
  desiredX = 0;
  desiredY = 0;
  disableMotors = -1;
  pthread_mutex_unlock (&commandMutex);
  sem_post (&newJoyStickValue);
}

void
getJoyStickPosition (int *currentButtons, int *currentX, int *currentY)
{
  pthread_mutex_lock (&commandMutex);
  *currentButtons = desiredButtons;
  *currentX = desiredX;
  *currentY = desiredY;
  pthread_mutex_unlock (&commandMutex);
}
