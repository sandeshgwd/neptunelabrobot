#include <pthread.h>
#include "SDL/SDL.h"

#if 0
int
main (int argc, char **argv)
{

}
#endif

void *
stopRobot (void *arg)
{
  setJoyStickPosition (1, 0, 0);
}

void *
handleJoystick (void *arg)
{
  SDL_Event test_event;
  SDL_Joystick *joy;
  Sint16 x_move, y_move;
  Sint16 previousX, previousY, previousZ;
  Sint16 currentX, currentY, currentZ;
  Sint16 previousButtons, currentButtons;
  int moved;
  int done;
  int controlRobot = 0;
  int startMotors = 0;
  SDL_Init (SDL_INIT_VIDEO | SDL_INIT_JOYSTICK);	// | SDL_INIT_EVENTTHREAD);
  if (!SDL_SetVideoMode (400, 400, 0, 0))
    {
      fprintf (stderr, "Could not set video mode: %s\n", SDL_GetError ());
      SDL_Quit ();
      exit (-1);
    }

  // Check for joystick
  if (SDL_NumJoysticks () > 0)
    {
      // Open joystick
      joy = SDL_JoystickOpen (0);

      if (joy)
	{

	  previousButtons = 0;
	  previousX = 0;
	  previousY = 0;
	  previousZ = 0;
	  currentButtons = 0;
	  currentX = 0;
	  currentY = 0;
	  currentZ = 0;
	  setJoyStickPosition (currentButtons, currentX, currentY);
	  printf ("Opened Joystick 0\n");
	  printf ("Name: %s\n", SDL_JoystickName (0));
	  printf ("Number of Axes: %d\n", SDL_JoystickNumAxes (joy));
	  printf ("Number of Buttons: %d\n", SDL_JoystickNumButtons (joy));
	  printf ("Number of Balls: %d\n", SDL_JoystickNumBalls (joy));

	  SDL_JoystickEventState (SDL_ENABLE);
	  //      SDL_JoyAxisEvent
	  //      SDL_JoyButtonEvent
	  //      SDL_Event();

	  pthread_cleanup_push (stopRobot, NULL);
	  done = 0;
	  while (!done)
	    {
	      if (SDL_PollEvent (&test_event))
		{
		  switch (test_event.type)
		    {
		    case SDL_KEYUP:
		    case SDL_KEYDOWN:
		      printf ("Keyboard Event!\n");
		      if (test_event.key.keysym.sym == SDLK_q)
			done = 1;
		      break;
		    case SDL_JOYAXISMOTION:
		      if (test_event.jaxis.axis == 0)
			currentX = test_event.jaxis.value >> 8;
		      else if (test_event.jaxis.axis == 1)
			currentY = -test_event.jaxis.value >> 8;
		      else if (test_event.jaxis.axis == 2)
			currentZ = test_event.jaxis.value >> 8;
		      printf ("Joystick Motion Event (%d, %d, %d)!\n",
			      currentX, currentY, currentZ);
		      break;
		    case SDL_JOYBUTTONUP:
		      if (test_event.jbutton.which == 0)
			{
			  currentButtons &= ~(1 << test_event.jbutton.button);
			  if (test_event.jbutton.button == 0)
			    {
			      if (controlRobot)
				printf ("Releasing control of Robot\n");
			      controlRobot = 0;
			    }
			  if (test_event.jbutton.button == 10)
			    {
			      if (startMotors > 20)
				joyStickEnableMotors ();
			    }
			  startMotors = 0;
			}
		      break;
		    case SDL_JOYBUTTONDOWN:
		      if (test_event.jbutton.which == 0)
			{
			  currentButtons |= 1 << test_event.jbutton.button;
			  if (test_event.jbutton.button == 10)
			    startMotors = 1;
			  else
			    startMotors = 0;

			  if (test_event.jbutton.button == 0)
			    {
			      if (!controlRobot)
				printf ("Control of Robot enabled\n");
			      controlRobot = 1;
			    }
			  else if (test_event.jbutton.button != 10)
			    {
			      joyStickDisableMotors (5);
			      if (controlRobot)
				printf ("Disabling motors on Robot\n");
			      controlRobot = 0;
			    }
			}
		      break;
		    default:
		      continue;
		      break;
		    }
		}
	      else
		{
		  if (startMotors)
		    startMotors++;
		  SDL_Delay (100);
		}

	      if (controlRobot)
		setJoyStickPosition (currentButtons, currentX, currentY);
	      else
		setJoyStickPosition (0, 0, 0);

	      previousButtons = currentButtons;
	      previousX = currentX;
	      previousY = currentY;
	      previousZ = currentZ;
	    }
	  pthread_cleanup_pop (1);
	}
      else
	printf ("Couldn't open Joystick 0\n");

      // Close if opened
      if (SDL_JoystickOpened (0))
	SDL_JoystickClose (joy);
    }
  else
    printf ("No joysticks available\n");

  SDL_Quit ();
  exit (0);
}
