/************************************************************
 (c) Applied AI Systems, Inc. 2003

************************************************************/

#include "debug.h"
#define _GNU_SOURCE
#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<sys/un.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <signal.h>
#include <userlcd.h>
#include <usermotor.h>
#include <userir.h>
#include "aaipacket.h"

/* Function Prototype for our signal handler */
static void sigHandler (int dummy);
#include "simpleserver.h"

/** Maximun number of characters that can be read for a single command
 * including all parameters and null-terminator */
#define MAX_PACKET_SIZE 257
#define MAX_DATA_SIZE (MAX_PACKET_SIZE - 23)

#define BAUDRATE B9600
#define MODEMDEVICE "/dev/ttyS0"
#define _POSIX_SOURCE 1		/* POSIX compliant source */

static int joy_absoluteSpeedControl (unsigned char *dataIn,
				     unsigned char *dataOut);
static int joy_stopMotors (unsigned char *dataIn, unsigned char *dataOut);
static int joy_disableMotors (unsigned char *dataIn, unsigned char *dataOut);
static int joy_enableMotors (unsigned char *dataIn, unsigned char *dataOut);
static int joy_watchdog (unsigned char *dataIn, unsigned char *dataOut);

/* Local Variables */
static time_t lastCommandTime = 0;
static int currentButtons = 0;
static int currentX = 0;
static int currentY = 0;

#define DEFAULT_PORT 16000

static SSCommandTable joyCommandTable[] = {
//  {"LR", 6, 1, absoluteSpeedControl}, // Left/Right Absolute Speed Control
  {"SD", 8, 1, joy_absoluteSpeedControl},	// Speed/Direction Absolute Speed Control
  {"ST", 0, 1, joy_stopMotors},	// Stop
  {"DM", 0, 1, joy_disableMotors},	// Disable Motors
  {"ON", 0, 1, joy_enableMotors},	// Disable Motors
  {NULL, 0, 0, joy_watchdog}
};

/** A wrapper to open a socket then call ptzController
 * \param arg unused. BUG: this should be used to specify (at least) the port.
 */
int
main (int argc, char **argv)
{
  struct termios oldtio, newtio;
  int create_socket, clientSocket, addrlen;
  int enableMotorPower;
  unsigned int port, socketType;
  int done;

  if (initMotors ())
    exit (-1);

  configureSpeedPID (0, 2048, 4, 512);
  configureSpeedPID (1, 2048, 4, 512);
  configureSpeedPID (2, 2048, 4, 512);
  configureSpeedPID (3, 2048, 4, 512);

  setBrakes (0);

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  if (argc > 2)
    port = strtol (argv[2], (char **) NULL, 0);
  else
    port = DEFAULT_PORT;

  if (argc > 3)
    socketType = argv[3][0] == 't' ? SOCK_STREAM : SOCK_DGRAM;
  else
    socketType = SOCK_DGRAM;

  if (argc > 1)
    create_socket = createNewSocket (socketType, argv[1], port);
  else
    create_socket = createNewSocket (socketType, NULL, port);

  if (socketType == SOCK_STREAM)
    listen (create_socket, 1);

  sleep (1);
  setMotorPower (0);

  addrlen = sizeof (struct sockaddr_in);
  done = 0;
  while (done >= 0)
    {
      if (socketType == SOCK_STREAM)
	{
	  clientSocket = accept (create_socket, NULL, NULL);
	  debug_print (minVerbLevel (2), "Accepting connection\n");
	}
      else
	{
	  clientSocket = create_socket;
	}

      done = simpleServer (socketType, clientSocket, joyCommandTable);

      if (socketType == SOCK_STREAM)
	{
	  debug_print (minVerbLevel (2), "Closing connection\n");
	  shutdown (clientSocket, SHUT_RDWR);
	  close (clientSocket);
	}
    }
  close (create_socket);
  debug_print (minVerbLevel (2), "Exiting server\n");

  return (0);
}

static int
joy_absoluteSpeedControl (unsigned char *dataIn, unsigned char *dataOut)
{
  char buffer[7];
  char convbuff[3];
  signed int desiredSpeeds[4];
  int position;
  int tiltAngle, panAngle, angle, zoom;
  int i;
  unsigned int value;
  int ptChanged = 0;

  lastCommandTime = time (NULL);

  // Convert from Ascii to binary data
  convbuff[2] = '\0';
  for (i = 0; i < 3; i++)
    {
      convbuff[0] = dataIn[i * 2];
      convbuff[1] = dataIn[i * 2 + 1];
      value = strtoul (convbuff, NULL, 16);
      buffer[i] = value;
    }

  currentButtons = buffer[0];
  currentY = buffer[1];
  currentX = buffer[2];

  debug_print (minVerbLevel (1), "Set X Y to %d, %d\n", currentX, currentY);
  desiredSpeeds[0] = (2 * currentY + currentX) / 16;
  desiredSpeeds[1] = (2 * currentY - currentX) / 16;
  desiredSpeeds[2] = desiredSpeeds[0];
  desiredSpeeds[3] = desiredSpeeds[1];

  setMotorSpeedAll (desiredSpeeds);

  if (dataOut)
    {
      dataOut[0] = '0';
      dataOut[1] = '0';
      return (2);
    }
  else
    return (0);
}

static int
joy_stopMotors (unsigned char *dataIn, unsigned char *dataOut)
{
  lastCommandTime = time (NULL);

  setMotorSpeedAll ((int[4])
		    {
		    0, 0, 0, 0});
  if (dataOut)
    {
      dataOut[0] = '0';
      dataOut[1] = '0';
      return (2);
    }
  else
    return (0);
}

static int
joy_disableMotors (unsigned char *dataIn, unsigned char *dataOut)
{
  lastCommandTime = time (NULL);

  debug_print (minVerbLevel (1), "Disabling Motor Power\n");
  setMotorSpeedAll ((int[4])
		    {
		    0, 0, 0, 0});
  setMotorPower (0);
  if (dataOut)
    {
      dataOut[0] = '0';
      dataOut[1] = '0';
      return (2);
    }
  else
    return (0);
}


static int
joy_enableMotors (unsigned char *dataIn, unsigned char *dataOut)
{
  lastCommandTime = time (NULL);

  debug_print (minVerbLevel (1), "Enabling Motor Power\n");

  setMotorSpeedAll ((int[4])
		    {
		    0, 0, 0, 0});
  currentX = 0;
  currentY = 0;

  setMotorPower (1);
  if (dataOut)
    {
      dataOut[0] = '0';
      dataOut[1] = '0';
      return (2);
    }
  else
    return (0);
}

static int
joy_watchdog (unsigned char *dataIn, unsigned char *dataOut)
{
  time_t currentTime;
  int result = 0;
  int power, buttons;
  static int buttonHoldTime = 0;
  static int enableMotorPower = 0;


  power = getMotorPower ();
  if (ON != power)
    {
      buttons = getButtons ();

      if ((1 == buttons) || (2 == buttons))
	{
	  if (buttonHoldTime > 10)
	    {
	      enableMotorPower = 1;
	    }
	  else
	    buttonHoldTime++;
	}
      else
	buttonHoldTime = 0;
      if (!buttons && enableMotorPower)
	{
	  setMotorPower (1);
	  enableMotorPower = 0;
	}
    }

  currentTime = time (NULL);
  if (lastCommandTime)
    {
      if ((currentTime - lastCommandTime) > 1)
	{
	  debug_print (minVerbLevel (1),
		       "CONNECTION TIMED OUT: Setting X Y to 0 0\n");
	  setMotorPower (0);
	  enableMotorPower = 0;
	  setMotorSpeedAll ((int[4])
			    {
			    0, 0, 0, 0});
	  lastCommandTime = 0;
	  result = -1;		// its been too long since the last check, disconnect
	}
    }
  return (result);
}

static void
sigHandler (int dummy)
     /* On critical signals, we should detach from shared memory */
{

  setMotorPower (0);
  closeMotors ();
  /*release the motor */

  exit (-1);
}
