#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<sys/un.h>
#include <arpa/inet.h>
#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include "debug.h"

#define UNIX_PATH_MAX 108
int
connectToServer (int type, char *host)
{
  int create_socket = -1;
  struct sockaddr_un unixAddress;
  struct sockaddr_in inetAddress;
  struct sockaddr *address = NULL;
  int addressSize = 0;
  char *colon;
  char *hostAddress;
  char *addressPort;
  int portNum;
  int len;
  int useUDP;

  if (host)
    addressPort = (char *) host;
  else
    {
      debug_print (minVerbLevel (2), "Need a host name\n");
      return (-1);
    }

  useUDP = 0;
  colon = strchr (addressPort, ':');
  if (colon)
    {
      len = strlen (addressPort);
      if ('u' == addressPort[len - 1])
	useUDP = 1;
      len = colon - addressPort;
      portNum = strtol (colon + 1, NULL, 10);
    }
  else
    {
      debug_print (minVerbLevel (2),
		   "Host name (%s) requires a port number\n", addressPort);
      return (-1);
    }

  if (portNum < 1024 || portNum > 65535)
    portNum = 15000;

  hostAddress = (char *) malloc (len + 1);
  if (hostAddress)
    {
      strncpy (hostAddress, (char *) addressPort, len);
      hostAddress[len] = '\0';
    }

  if (type == PF_UNIX || type == PF_INET)
    {
      if (useUDP)
	create_socket = socket (type, SOCK_DGRAM, 0);
      else
	create_socket = socket (type, SOCK_STREAM, 0);
      if (create_socket >= 0)
	{
	  debug_print (minVerbLevel (2), "The Socket was created\n");
	  switch (type)
	    {
	    case PF_INET:
	      inetAddress.sin_family = AF_INET;
	      inetAddress.sin_port = htons (portNum);
	      inet_pton (AF_INET, hostAddress, &inetAddress.sin_addr);
	      address = (struct sockaddr *) &inetAddress;
	      addressSize = sizeof (inetAddress);
	      break;

	    case PF_UNIX:

	      unixAddress.sun_family = AF_UNIX;
	      memset (unixAddress.sun_path, 0, UNIX_PATH_MAX);
	      snprintf (unixAddress.sun_path + 1, UNIX_PATH_MAX,
			"IIPU-00:%05d", portNum);
	      address = (struct sockaddr *) &unixAddress;
	      addressSize = sizeof (unixAddress);
	      break;
	    default:
	      break;
	    }

	  if (connect (create_socket, address, addressSize) == 0)
	    {
	      debug_print (minVerbLevel (2),
			   "The connection was accepted with the server %s...\n",
			   hostAddress);
	    }
	  else
	    {
	      debug_perror (minVerbLevel (2),
			    "The connection was not accepted by the server");
	      debug_print (minVerbLevel (2),
			   "The connection was not accepted by the server %s...\n",
			   hostAddress);
	      close (create_socket);
	      create_socket = -1;
	    }
	}
    }
  free (hostAddress);
  return (create_socket);
}

int
disconnectFromServer (int fd)
{
  if (fd >= 0)
    {
      shutdown (fd, SHUT_RDWR);
      close (fd);
      fd = -1;
    }
  return (0);
}
