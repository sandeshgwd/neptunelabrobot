/*
 */

#include <stdio.h>
#include <signal.h>
#include "userir.h"
#include "userlcd.h"

/* 
 * bit0		brake0 for MOTOR1
 * bit1		brake1 for MOTOR1
 * bit2		brake0 for MOTOR2
 * bit3		brake1 for MOTOR2
 * bit4		direction0 for MOTOR1
 * bit5		direction1 for MOTOR1
 * bit6		direction0 for MOTOR2
 * bit7		direction1 for MOTOR2
 */

void sigHandler (int dummy);

int
main ()
{
  int i, error, irs[12];
  unsigned int bumper = 0;

  if(initIR())
	exit(-1);

  lcdopen (0);

  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  while (1)
    {
      getIRs(irs);
      printf ("ir: ");
      for (i = 0; i < 5; i++)
	{
	  lcdgoto (0, i * 3);
	  lcdputbyte (irs[i]);
	}
      for (i = 0; i < 5; i++)
	{
	  lcdgoto (1, i * 3);
	  lcdputbyte (irs[i + 5]);
	}
      for (i = 0; i < 12; i++)
	{
	  printf ("%3X ", irs[i]);
	}
      bumper = getBumper();
      printf ("bumper: %02X\n", bumper);
      fflush (stdout);
      usleep (250000);
    }
  lcdclose ();
  closeIR();
}

void
sigHandler (int dummy)
{
  exit (-1);
}
