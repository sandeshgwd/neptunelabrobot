/*
 * Quick tests for RT-Linux lcd driver and user-space interface.
 */

#include <unistd.h>		/*usleep(), read(), write(), etc... */
#include <stdio.h>
#include <string.h>
#include <sys/types.h>		/* Next 3 for open() */
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include "lcd.h"
#include "userlcd.h"
#define LINE_LENGTH 16

int
main (int argc, char **argv)
{
  int err;
  char line[LINE_LENGTH + 1];

  if ((err = lcdopen (0)) < 0)
    {
      exit (0);
    }

  lcdclr ();
  if (argc > 1)
    {
      strncpy (line, argv[1], LINE_LENGTH);
      line[LINE_LENGTH] = '\0';
      if ((err = lcdputs (line)) != 0)
	{
	  lcdclose ();
	  exit (0);
	}

      if (argc > 2)
	{
	  lcdgoto (1, 0);
	  strncpy (line, argv[2], LINE_LENGTH);
	  line[LINE_LENGTH] = '\0';
	  if ((err = lcdputs (line)) != 0)
	    {
	      lcdclose ();
	      exit (0);
	    }
	}
    }
  if ((err = lcdclose ()) < 0)
    {
      exit (0);
    }
  exit (0);
}
