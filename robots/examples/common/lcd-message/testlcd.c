/*
 * Quick tests for RT-Linux lcd driver and user-space interface.
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include "lcd.h"
#include "userlcd.h"

int done;

void
handler (int arg)
{
  done = 1;
}

int
main (void)
{
  int err;
  int fd;

  fd = open("/dev/lcd0", O_WRONLY);
  ioctl(fd, LCD_IOC_INIT, 0);
  write(fd, "!", 1);
  ioctl(fd, LCD_IOC_CLEAR, 0);

  err = lcdopen (0);
  if (err < 0)
    {
      perror ("lcdopen() failed: ");
    }

  err = lcdinit ();
  if (err < 0)
    {
      perror ("lcdinit() failed: ");
    }

  done = 0;
  signal (SIGINT, handler);
  while (!done)
    {
      err = lcdputs ("Strange game,");
      if (err != 0)
	{
	  perror ("lcdputs1 failed: ");
	}

      err = lcdgoto (1, 0);
      if (err != 0)
	{
	  perror ("lcdgoto()1 failed: ");
	}

      err = lcdputs ("Prof. Falken...");
      if (err != 0)
	{
	  perror ("lcdputs2 failed: ");
	}

      sleep (3);

      lcdscroll (16);
      lcdclr ();
      err = lcdputs ("The only");
      if (err != 0)
	{
	  perror ("lcdputs3 failed: ");
	}

      lcdgoto (1, 0);
      err = lcdputs ("winning move...");
      if (err != 0)
	{
	  perror ("lcdputs4 failed: ");
	}

      sleep (3);

      lcdscroll (16);
      lcdclr ();
      err = lcdputs ("is not to play.");
      if (err != 0)
	{
	  perror ("lcdputs2 failed: ");
	}

      sleep (3);

      lcdscroll (16);
      lcdclr ();
    }

  err = lcdclose ();
  if (err != 0)
    {
      perror ("lcdclose() failed: ");
    }

  return 0;
}
