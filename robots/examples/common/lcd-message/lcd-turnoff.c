/*
 * Quick tests for RT-Linux lcd driver and user-space interface.
 */

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include "userlcd.h"

int
main (int argc, char **argv)
{

  int err;

  if ((err = lcdopen (0)) < 0)
    {
      exit (0);
    }

  lcdclr ();
  if ((err = lcdputs ("Turn off now")) != 0)
    {
      exit (0);
    }

  if ((err = lcdclose ()) < 0)
    {

      exit (0);
    }

  exit (0);
}
