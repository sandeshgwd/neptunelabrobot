/*
 * Quick tests for RT-Linux lcd driver and user-space interface.
 */

#include <unistd.h>		/*usleep(), read(), write(), etc... */
#include <stdio.h>
#include <sys/types.h>		/* Next 3 for open() */
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include "lcd.h"
#include "userlcd.h"

int
main (int argc, char **argv)
{

  int err;

  if ((err = lcdopen (0)) < 0)
    {
      exit (0);
    }

  lcdclr ();
  if ((err = lcdputs ("Shutting down")) != 0)
    {
      exit (0);
    }

  lcdgoto (1, 0);
  if ((err = lcdputs ("Please wait...")) != 0)
    {
      exit (0);
    }

  if ((err = lcdclose ()) < 0)
    {

      exit (0);
    }
  exit (0);
}
