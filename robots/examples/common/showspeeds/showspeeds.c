/************************************************************
 (c) Applied AI Systems, Inc. 2003

************************************************************/

#include "debug.h"
#define _GNU_SOURCE
#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<sys/un.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <signal.h>
#include <userlcd.h>
#include <usermotor.h>
#include <userir.h>

/* Function Prototype for our signal handler */
static void sigHandler (int dummy);
#include "simpleserver.h"

/** Maximun number of characters that can be read for a single command
 * including all parameters and null-terminator */
#define MAX_PACKET_SIZE 257
#define MAX_DATA_SIZE (MAX_PACKET_SIZE - 23)

/* Local Variables */
static time_t lastCommandTime = 0;
static int currentX = 0;
static int currentY = 0;

static int
checkButtons ()
{
  time_t currentTime;
  int result = 0;
  int power, buttons;
  static int buttonHoldTime = 0;
  static int enableMotorPower = 0;


  power = getMotorPower ();
  if (ON != power)
    {
      buttons = getButtons ();

      if ((1 == buttons) || (2 == buttons))
	{
	  if (buttonHoldTime > 10)
	    {
	      enableMotorPower = 1;
	    }
	  else
	    buttonHoldTime++;
	}
      else
	buttonHoldTime = 0;
      if (!buttons && enableMotorPower)
	{
	  setMotorPower (1);
	  enableMotorPower = 0;
	}
    }
  return (result);
}

static void
sigHandler (int dummy)
     /* On critical signals, we should detach from shared memory */
{

  setMotorPower (0);
  closeMotors ();
  /*release the motor */

  exit (-1);
}

/** A wrapper to open a socket then call ptzController
 * \param arg unused. BUG: this should be used to specify (at least) the port.
 */
int
main (int argc, char **argv)
{
  int enableMotorPower;
  int speeds[4];
  unsigned int port, socketType;
  int done;

  if (initMotors ())
    exit (-1);

  configureSpeedPID (0, 160, 2, 100);
  configureSpeedPID (1, 160, 2, 100);
  configureSpeedPID (2, 160, 2, 100);
  configureSpeedPID (3, 160, 2, 100);

  setBrakes (0);

  /* Install our own signal handler to catch these signals so we can
   * release the shared memory before exitting.  This is already
   * handled automatically because we use attach/detatch, but it is
   * a good idea to do it explicitly */
  signal (SIGTERM, sigHandler);
  signal (SIGQUIT, sigHandler);
  signal (SIGINT, sigHandler);
  signal (SIGHUP, sigHandler);

  sleep (1);
  setMotorPower (0);

  done = 0;
  while (done >= 0)
    {
      checkButtons ();
      getMotorSpeedAll (speeds);
      fprintf (stdout, "%d %d %d %d\n", speeds[0], speeds[1], speeds[2],
	       speeds[3]);
      usleep (250000);
    }
  debug_print (minVerbLevel (2), "Exiting server\n");

  return (0);
}
