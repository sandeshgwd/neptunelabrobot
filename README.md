NGS-Neptune Codebase (The actual robot's name is LABO3, currently named as Neptune for NGS use)


The instructions below are based on the following assumption:
1)	Installed ROS - Indigo on the robot and the laptop
2)	RTAI patch (Document placed at doc/neptuneOSUpgradeWithRTAIPatch.odt) 
2)	Cloned the neptune code from 
	https://bitbucket.org/nextgensystems/neptune


Packages definition:
labo3_base:		It has the code to run the robot's base with odometry. It accepts the cmd_vel ros_node for its movement.

labo3_framework:	This package includes the URDF of the robot model for its viewing on the RVIZ and gazebo.

labo3_navigation:	This package has the settings for the ros navigation stack.

labo3_teleop:		This package deals with running the robot using joystick or keyboard. It also includes a modified version of keyboard navigation as it is different from the pr2 movement.

robots:			All the driver files related to labo3 are kept inside this package. These drivers are launched at the start.
			Include the rc_part1.local and rc_part2.local scripts in the /etc/rc.local file for the drivers to run on start up.


Connecting to Neptune in NGS with laptop connected to NGS network:
ssh neptune@neptune.local
password: 8172725277

If unalble to connect the neptune, these are the following steps for re-configuring the ASUS RT-N65U router
1)	The router has been updated with paladian custom firmware which is also been included in the folder routerFirmware.
	This step is optional only if new firmware available at https://code.google.com/p/rt-n56u/

2)	Change the LAN IP to: 192.168.1.3

3)	Disable the DHCP and change the Staring IP to 192.168.1.4

4)	On 2.4 GHz wireless setup, in the BRIDGE-MODE configuration, make following changes
	mode: AP & AP-Client
	Channel: Based on NGS channel
	SSID: NGS
	Security: WAP2-Personal(AES)
	password (Add NGS password): 8172725459


Common steps while working with Neptune:
Step 1:	If this is the first time you are working with Neptune, then additional packages would be required for its working.
	rosinstall . netpune.rosinstall

Step 2: To start the robot to accept commands for its movement (Run this on the neptune command prompt)
	roslaunch labo3_base neptune_base.launch

Step 3: To view the robots movement on RVIZ (Run this on the laptop command prompt)
	roslaunch labo3_framework labo3_rviz.launch

Step 4: For laser scanning and Gmapping (Run this on the neptune command prompt)
	roslaunch labo3_base laser_gmapping.launch

(Optional steps based on requirement)
Step 5: If Joystick navigation required (Run this on the laptop command prompt)
	roslaunch labo3_teleop teleop_joystick.launch

Step 6: If keyboard tele-operation required (Run this on the laptop command prompt)
	roslaunch labo3_teleop teleop_keyboard.launch
