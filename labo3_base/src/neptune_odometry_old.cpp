/*
 * Neptune_base driver interface
 * Author: Sandesh Gowda
 * Created:30/10/2014
 */

#include <ros/ros.h>
#include <labo3_base/EncoderData.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/JointState.h>
extern "C" {
#include <usermotor.c>
};

#define PI                  22/7
#define RAD_WHEEL           0.095
#define COUNTS_PER_REV_LEFT 3037.8095238095
#define COUNTS_PER_REV_RGHT 3042.55
#define COUNTER_VALUE       65535
#define FORWARD             1
#define BACKWARD            0
#define ON		            1
#define OFF		            0
#define DIST_BET_WHEELS		0.29032
#define LEFT_MOTOR			2
#define RGHT_MOTOR			3
#define NUM_MOTORS			4

struct EncoderStruct
{
	double leftTicks;
	double rghtTicks;
};

class Neptune
{
public:
	Neptune( ros::NodeHandle &node )  ;
	void neptuneOdometry()					  ;

private:
	void initialise();
	void cmdVel(const geometry_msgs::Twist::ConstPtr& vel);
	//void movementCallBack(const labo3_base::EncoderData::ConstPtr &motorData);
	//void initialise();
	
	EncoderStruct   motorMovement    ;

	ros::Subscriber cmdVel_sub		 ;
	ros::Publisher  odom_pub		 ;
	ros::Publisher  joint_pub	     ;

	tf::TransformBroadcaster odom_broadcaster ;

	// Drive related variables
	double leftDriveVel			;
	double rghtDriveVel			;
	int power, motorPower		;
    signed int desiredSpeeds[NUM_MOTORS]	;
    unsigned int encoderXtract[8]; // first 4 for counts and last 4 for direction

    geometry_msgs::Twist cmd;
    labo3_base::EncoderData encoderData ;

	// Odometry related variables
	double revPerCountLeft  ;
	double revPerCountRght  ;

	double wLeft            ;
	double wRght            ;
	double leftPos          ;
	double rightPos         ;

	double x                ;
	double y                ;
	double w                ;
	double vx               ;
	double vy               ;
	double vw               ;

	double countsLeft       ;
	double countsRght       ;

	ros::Time currTime	           ;
	ros::Time prevTime             ;

	ros::Time movementCallBackTime ;

};

void Neptune::initialise()
{
	// Drive Initialization
	if (initMotors())
	{
		exit(-1);
	}

	configureSpeedPID (2, 2048, 4, 512);
	configureSpeedPID (3, 2048, 4, 512);

	setBrakes(0);
	motorPower = OFF;
	setMotorPower (motorPower);

	leftDriveVel = 0.0	;
	rghtDriveVel = 0.0	;
	desiredSpeeds[0] = 0.0	;
	desiredSpeeds[1] = 0.0	;

	getEncoderAll( encoderXtract );
	encoderData.left_counter = encoderXtract[2];
	encoderData.rght_counter = encoderXtract[3];
	encoderData.left_dir     = encoderXtract[6];
	encoderData.rght_dir     = encoderXtract[7];

	// Odometry Initialization
	revPerCountLeft      = 2*PI*RAD_WHEEL/COUNTS_PER_REV_LEFT ;
	revPerCountRght      = 2*PI*RAD_WHEEL/COUNTS_PER_REV_RGHT ;

	wLeft                = 0.0                               ;
	wRght                = 0.0                               ;

	x                    = 0.0                               ;
	y                    = 0.0                               ;
	w                    = 0.0                               ;
	vx                   = 0.0                               ;
	vy                   = 0.0                               ;
	vw                   = 0.0                               ;

	countsLeft           = COUNTER_VALUE                     ;
	countsRght           = COUNTER_VALUE                     ;

	currTime             = ros::Time::now() 	             ;
	prevTime             = ros::Time::now()		             ;

	movementCallBackTime = ros::Time::now()		             ;
}

Neptune::Neptune( ros::NodeHandle &node )
{
	//encoder_data_sub	=  node.subscribe<labo3_base::EncoderData>("encoder_data", 1, &Neptune::movementCallBack, this);
	cmdVel_sub 		=  node.subscribe<geometry_msgs::Twist>("cmd_vel", 50, &Neptune::cmd_vel, this);
	odom_pub   		=  node.advertise<nav_msgs::Odometry>("odom", 50);
	joint_pub   	=  node.advertise<sensor_msgs::JointState>("joint_states", 50);
	Neptune::initialise();
}

void Neptune::cmdVel(const geometry_msgs::Twist::ConstPtr& vel)
{

	leftDriveVel = ((vel->linear.x)*(200/19)) - ((vel->angular.z)*(191/125));
	rghtDriveVel = ((vel->linear.x)*(200/19)) + ((vel->angular.z)*(191/125));

	power = getMotorPower ();
	if (ON != power)
	{
		motorPower = ON;
		setMotorPower (enableMotorPower);
	}

	if (motorPower)
	{
		desiredSpeeds[2] = leftDriveVel;
		desiredSpeeds[3] = rghtDriveVel;
	}
	else
	{
		desiredSpeeds[2] = 0.0;
		desiredSpeeds[3] = 0.0;
	}
	setMotorSpeedAll (desiredSpeeds);
}

void Neptune::movementCallBack(const labo3_base::EncoderData::ConstPtr &motorData)
{
	double dt               = (ros::Time::now() - movementCallBackTime).toSec() ;

	motorMovement.leftTicks = countsLeft - motorData->left_counter               ;   // Number of ticks traveled
	motorMovement.rghtTicks = countsRght - motorData->rght_counter               ;   // Number of ticks traveled

	if(motorData->left_dir == BACKWARD)
	motorMovement.leftTicks = -(motorMovement.leftTicks)                        ;
	if(motorData->rght_dir == BACKWARD)
	motorMovement.rghtTicks = -(motorMovement.rghtTicks)                        ;

	ROS_INFO("left_ticks = %lf right_ticks = %lf",motorMovement.leftTicks,motorMovement.rghtTicks);
	countsLeft              = motorData->left_counter                            ;
	countsRght              = motorData->rght_counter                            ;

	sensor_msgs::JointState joint_state;
	joint_state.header.stamp 		= currTime								 ;
	joint_state.name.push_back("left_wheel_joint")                           ;
	wLeft = motorMovement.leftTicks * revPerCountLeft                        ;
	leftPos += wLeft;
	joint_state.position.push_back(leftPos);
	joint_state.velocity.push_back(wLeft);

	joint_state.name.push_back("right_wheel_joint")                          ;
	wRght = motorMovement.rghtTicks * revPerCountRght                        ;
	rightPos += wRght;
	joint_state.position.push_back(rightPos);
	joint_state.velocity.push_back(wRght);


	joint_pub.publish(joint_state);

	//FIXME for real-time controller
	/*wLeft                   = motorMovement.leftTicks * revPerCountLeft / dt    ;
	wRght                   = motorMovement.rghtTicks * revPerCountRght / dt    ;
	ROS_INFO("wLeft = %lf wRght = %lf",wLeft,wRght);*/
	movementCallBackTime    = ros::Time::now()                                  ;

}

void Neptune::neptuneOdometry()
{
	currTime    = ros::Time::now()                                  ;
	
	// Kinematic Equations for Differential Drive

	// Track Velocities
	double velLeft = wLeft * RAD_WHEEL ;
	double velRght = wRght * RAD_WHEEL ;


	// Platform Velocities
	vx = 0.5 * RAD_WHEEL * ( velLeft + velRght ) * cos(w)    ;
	vy = 0.5 * RAD_WHEEL * ( velLeft + velRght ) * sin(w)    ;
	vw = RAD_WHEEL * ( velLeft - velRght ) / DIST_BET_WHEELS ;


	double dt = ( currTime - prevTime ).toSec();

	double delta_x = vx * dt ;
	double delta_y = vy * dt ;
	double delta_w = vw * dt ;

	x += delta_x ;
	y += delta_y ;
	w += delta_w ;

	/***************************************************/
	/* Publish results   							   */

	//Quaternion created from yaw
	geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(w) ;
/*
	// Step 1: Publish the transform over tf
	geometry_msgs::TransformStamped odom_trans ;
	odom_trans.header.stamp     = currTime ;
	odom_trans.header.frame_id  = "odom"       ;
	odom_trans.child_frame_id   = "base_link"  ;

	odom_trans.transform.translation.x = x     ;
	odom_trans.transform.translation.y = y     ;
	odom_trans.transform.translation.z = 0.0   ;
	odom_trans.transform.rotation = odom_quat  ;*/
	tf::Transform transform;
	transform.setOrigin( tf::Vector3(x, y, 0.0) );
	tf::Quaternion q;
	q.setRPY(0, 0, w);
	transform.setRotation(q);
	odom_broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "odom", "base_footprint"));
	//send the transform
	//odom_broadcaster.sendTransform(odom_trans) ;

	// Step 2: Publish the odometry message over ROS
	nav_msgs::Odometry odom             ;
	odom.header.stamp    = currTime ;
	odom.header.frame_id = "odom"       ;

	//set the position
	odom.pose.pose.position.x  = x         ;
	odom.pose.pose.position.y  = y         ;
	odom.pose.pose.position.z  = 0.0       ;
	odom.pose.pose.orientation = odom_quat ;

	//set the velocity
	odom.child_frame_id        = "base_footprint" ;
	odom.twist.twist.linear.x  = vx          ;
	odom.twist.twist.linear.y  = vy          ;
	odom.twist.twist.angular.z = vw          ;

	//publish the message
	odom_pub.publish(odom) ;

	/***************************************************/

	prevTime = currTime;

}

//Main Function Call
int main(int argc, char **argv)
{
  ros::init(argc, argv, "neptune_odometry_publisher");

  ROS_INFO("Starting Odometry Calculation!");

  ros::NodeHandle n;

  Neptune neptune_odom(n);

  // How fast to run this node
  int rate = 50;
  ros::Rate r(rate);

  while (n.ok())
  {
	  ros::spinOnce();

	  neptune_odom.neptuneOdometry();

	  r.sleep();
  }

  return 0;
}
