/*
 * Neptune_base driver interface
 * Author: Sandesh Gowda
 * Created:30/10/2014
 */

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/JointState.h>
extern "C" {
#include <usermotor.c>
};

#define PI                  22/7
#define RAD_WHEEL           0.095
#define COUNTS_PER_ROT_LEFT 3037.8095238095
#define COUNTS_PER_ROT_RGHT 3042.55
#define MOTOR_ON            1
#define MOTOR_OFF		    0
#define DIST_BET_WHEELS		0.29032
#define LEFT_MOTOR			2
#define RGHT_MOTOR			3
#define NUM_MOTORS			4
#define SCALING_FACTOR			40*RAD_WHEEL

class Neptune
{
public:
	Neptune( ros::NodeHandle &node )  ;
	void neptuneOdometry()			  ;

private:
	void initialise();
	void neptuneDrive(const geometry_msgs::Twist::ConstPtr &vel);

	ros::Subscriber neptuneDrive_sub ;
	ros::Publisher  odom_pub		 ;
	ros::Publisher  joint_pub	     ;

	tf::TransformBroadcaster odom_broadcaster ;

	// Drive related variables
	double leftDriveVel			;
	double rghtDriveVel			;

	int power, motorPower		;

    int desiredSpeeds[NUM_MOTORS]	;
    int encoderSpeedXtract[4];

	// Odometry related variables
	double circumference;

	double leftCurrPos         ;
	double rghtCurrPos         ;

	double velLeft   ;
	double velRght   ;

	double x                ;
	double y                ;
	double w                ;
	double vx               ;
	double vy               ;
	double vw               ;

	ros::Time currTime ;
	ros::Time movementCallBackTime ;

};

void Neptune::initialise()
{
	// Drive Initialization
	if (initMotors())
	{
		exit(-1);
	}

	configureSpeedPID (2, 2048, 4, 512);
	configureSpeedPID (3, 2048, 4, 512);

	setBrakes(0);
	motorPower = MOTOR_OFF;
	setMotorPower (motorPower);

	leftDriveVel = 0.0	;
	rghtDriveVel = 0.0	;
	desiredSpeeds[0] = 0.0	;
	desiredSpeeds[1] = 0.0	;

	getMotorSpeedAll( encoderSpeedXtract );

	// Odometry Initialization
	circumference      = 2*PI*RAD_WHEEL;

	velLeft     = 0.0 ;
	velRght     = 0.0 ;

	leftCurrPos         = 0.0 ;
	rghtCurrPos         = 0.0 ;

	x                    = 0.0                               ;
	y                    = 0.0                               ;
	w                    = 0.0                               ;
	vx                   = 0.0                               ;
	vy                   = 0.0                               ;
	vw                   = 0.0                               ;

	currTime = ros::Time::now()		             ;
	movementCallBackTime = ros::Time::now()		             ;

	ROS_INFO("Initialise");
}

Neptune::Neptune( ros::NodeHandle &node )
{
	neptuneDrive_sub =  node.subscribe<geometry_msgs::Twist>("cmd_vel", 1, &Neptune::neptuneDrive, this);
	odom_pub   		 =  node.advertise<nav_msgs::Odometry>("odom", 1);
	joint_pub   	 =  node.advertise<sensor_msgs::JointState>("joint_states", 1);
	initialise();
}

void Neptune::neptuneDrive(const geometry_msgs::Twist::ConstPtr& vel)
{
	float linearVel  = vel->linear.x	;
	float angularVel = vel->angular.z	;
	if(linearVel > 1)
	{
		linearVel = 1;
		ROS_INFO("MAX LINEAR SPEED is 1 m/s");
	}
	else if(linearVel < -1)
	{
		linearVel = -1;
		ROS_INFO("MAX LINEAR SPEED is 1 m/s");
	}
	if(angularVel > PI)
	{
		angularVel = PI;
		ROS_INFO("MAX ANGULAR SPEED is PI rad/s");
	}
	else if(angularVel < -PI)
	{
		angularVel = -PI;
		ROS_INFO("MAX ANGULAR SPEED is PI rad/s");
	}
	
	leftDriveVel = SCALING_FACTOR * (((2*(linearVel)) - ((angularVel)*DIST_BET_WHEELS))/(2*RAD_WHEEL));
	rghtDriveVel = SCALING_FACTOR * (((2*(linearVel)) + ((angularVel)*DIST_BET_WHEELS))/(2*RAD_WHEEL));

	power = getMotorPower ();
	if (ON != power)
	{
		motorPower = MOTOR_ON;
		setMotorPower (motorPower);
	}

	if (motorPower)
	{
		desiredSpeeds[2] = leftDriveVel;
		desiredSpeeds[3] = rghtDriveVel;
	}
	else
	{
		desiredSpeeds[2] = 0.0;
		desiredSpeeds[3] = 0.0;
	}
	setMotorSpeedAll (desiredSpeeds);
}

void Neptune::neptuneOdometry()
{
	currTime		= ros::Time::now();
	double dt               = (currTime - movementCallBackTime).toSec() ;
	if (dt == 0.0)
		return;

	getMotorSpeedAll(encoderSpeedXtract);

	// *********************Moving the wheels of LABO3********************* //
	sensor_msgs::JointState joint_state;

    leftCurrPos   += dt * encoderSpeedXtract[LEFT_MOTOR] * 128 * 2 * PI / COUNTS_PER_ROT_LEFT     ;
	rghtCurrPos   += dt * encoderSpeedXtract[RGHT_MOTOR] * 128 * 2 * PI / COUNTS_PER_ROT_RGHT     ;

	if (leftCurrPos > (2 * PI))
		leftCurrPos = leftCurrPos - (2 * PI);
	else if (leftCurrPos < 0)
		leftCurrPos = (2 * PI) + leftCurrPos ;
	if (rghtCurrPos > (2 * PI))
		rghtCurrPos = rghtCurrPos - (2 * PI);
	else if (rghtCurrPos < 0)
		rghtCurrPos = (2 * PI) + rghtCurrPos ;

	joint_state.header.stamp 		= ros::Time::now() ;
	joint_state.name.push_back("left_wheel_joint");
	joint_state.position.push_back(leftCurrPos);

	joint_state.name.push_back("right_wheel_joint");
	joint_state.position.push_back((-1)*rghtCurrPos);

	joint_state.name.push_back("right_castor_frame_joint");
	joint_state.position.push_back(0.0);

	joint_state.name.push_back("right_castor_wheel_joint");
	joint_state.position.push_back(0.0);

	joint_state.name.push_back("left_castor_frame_joint");
	joint_state.position.push_back(0.0);

	joint_state.name.push_back("left_castor_wheel_joint");
	joint_state.position.push_back(0.0);

	joint_state.name.push_back("laser_joint");
	joint_state.position.push_back(0.0);

	joint_state.name.push_back("camera_joint");
	joint_state.position.push_back(0.0);

	joint_pub.publish(joint_state);
	// ******************************************************************** //

	// ***********************Moving the LABO3 ROBOT*********************** //

	velLeft = encoderSpeedXtract[LEFT_MOTOR] * 128 * circumference / COUNTS_PER_ROT_LEFT ;
	velRght = encoderSpeedXtract[RGHT_MOTOR] * 128 * circumference / COUNTS_PER_ROT_RGHT ;

	// Platform Velocities
	vx = 0.5 * ( velRght + velLeft ) * cos(w)    ;
	vy = 0.5 * ( velRght + velLeft ) * sin(w)    ;
	vw = ( velRght - velLeft ) / DIST_BET_WHEELS ;

	double delta_x = vx * dt ;
	double delta_y = vy * dt ;
	double delta_w = vw * dt ;

	x += delta_x ;
	y += delta_y ;
	w += delta_w ;

	//Quaternion created from yaw
	geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(w) ;

	// Step 1: Publish the transform over tf
	geometry_msgs::TransformStamped odom_trans ;
	odom_trans.header.stamp     = currTime ;
	odom_trans.header.frame_id  = "odom"       ;
	odom_trans.child_frame_id   = "base_footprint"  ;

	odom_trans.transform.translation.x = x     ;
	odom_trans.transform.translation.y = y     ;
	odom_trans.transform.translation.z = 0.0   ;
	odom_trans.transform.rotation = odom_quat  ;

	//send the transform
	odom_broadcaster.sendTransform(odom_trans) ;

	// Step 2: Publish the odometry message over ROS
	nav_msgs::Odometry odom             ;
	odom.header.stamp    = currTime ;
	odom.header.frame_id = "odom"       ;

	//set the position
	odom.pose.pose.position.x  = x         ;
	odom.pose.pose.position.y  = y         ;
	odom.pose.pose.position.z  = 0.0       ;
	odom.pose.pose.orientation = odom_quat ;
	odom.pose.covariance = {0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03};
	//set the velocity
	odom.child_frame_id        = "base_footprint" ;
	odom.twist.twist.linear.x  = vx          ;
	odom.twist.twist.linear.y  = vy          ;
	odom.twist.twist.angular.z = vw          ;
	odom.twist.covariance = {0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03};
	//publish the message
	odom_pub.publish(odom) ;

	movementCallBackTime    = ros::Time::now();
}

//Main Function Call
int main(int argc, char **argv)
{
	ros::init(argc, argv, "neptune_launcher");
	ros::NodeHandle n;

	Neptune neptune(n);

	// How fast to run this node
	int rate = 50;
	ros::Rate r(rate);

	while (n.ok())
	{
	ros::spinOnce();

	neptune.neptuneOdometry();

	r.sleep();
	}

	return 0;
}

