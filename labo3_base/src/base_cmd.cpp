/*
* Author: isao.saito@mavs.uta.edu
* Converted to ROS Isura Ranatunga isura.ranatunga@mavs.uta.edu
* Usage ./motorTest <move duration> <left speed> <right speed>
*/


#include <ros/ros.h>
#include "geometry_msgs/Twist.h"


using namespace std;

 

int main(int argc, char** argv)
{

   ros::init(argc, argv, "neptune_base_cmd");

  ros::NodeHandle nh;

  ros::Publisher cmdVel_pub = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1,true);

  geometry_msgs::Twist cmd;	

  cmd.linear.x = 3.0;
  //cmd.linear.y = 3.0;
  cmd.angular.z = 0.0;

//  for(int i=1;i<100;i++)
//  {
  ROS_INFO("Starting...");
    cmdVel_pub.publish(cmd);
    ros::Duration(5.0).sleep();
//  }


  ROS_INFO("Stopping...");	
  geometry_msgs::Twist cmd_stop;
   cmdVel_pub.publish(cmd_stop);	

   return 0;
}
