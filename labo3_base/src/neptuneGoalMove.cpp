/*
 * Neptune Move to Goal
 * Author: Sandesh Gowda
 * Created: 02/10/2015
 */

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <angles/angles.h>
#include <tf/tf.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <math.h>

#define PI                  22/7

class NeptuneGoalMove
{
public:
	NeptuneGoalMove( ros::NodeHandle &node )  ;
	void setSpeed(double linear_speed, double angular_speed);
	void commandNeptune()			  ;

	double x(){return m_x;}
	double y(){return m_y;}
	double yaw(){return m_yaw;}

private:
	void initialise();
	void odomCallback(const nav_msgs::Odometry::ConstPtr &odom);

	ros::Publisher m_cmd_vel_pub		;
	ros::Subscriber  m_odom_sub		 	;

	double m_x		;
	double m_y		;
	double m_yaw	;
};

void NeptuneGoalMove::initialise()
{
	m_x		= 	0.0	;
	m_y		=	0.0	;
	m_yaw   = 	0.0 ;

	ROS_INFO("Initialise");
}

NeptuneGoalMove::NeptuneGoalMove( ros::NodeHandle &node )
{
	m_cmd_vel_pub 	=  node.advertise<geometry_msgs::Twist>("cmd_vel", 1);
	m_odom_sub   	=  node.subscribe<nav_msgs::Odometry>("odom", 1, &NeptuneGoalMove::odomCallback, this);
	initialise();
}

void NeptuneGoalMove::odomCallback(const nav_msgs::Odometry::ConstPtr &odom)
{
	m_x 	= 	odom->pose.pose.position.x;
	m_y		=	odom->pose.pose.position.y;
	m_yaw 	= 	tf::getYaw(odom->pose.pose.orientation);
}

void NeptuneGoalMove::setSpeed(double linear_speed, double angular_speed)
{
    geometry_msgs::Twist vel;
    vel.linear.x = linear_speed;
    vel.angular.z = angular_speed;
    m_cmd_vel_pub.publish(vel);
}

void NeptuneGoalMove::commandNeptune()
{
	geometry_msgs::Twist vel;
	int goalPointX = 1.0;
	int goalPointY = 1.0;
	ROS_INFO("The x = %lf ", x());
	ROS_INFO("The y = %lf ", y());
	double dist = sqrt(pow(goalPointX-x(),2) + pow(goalPointY-y(),2));
	ROS_INFO("The dist = %lf ", dist);

	double theta = atan((goalPointY-y())/(goalPointX-x()));
	double errorTheta = theta - yaw();
	ROS_INFO("The yaw = %lf ", yaw());
	ROS_INFO("The Error in theta = %lf ", errorTheta);
	if(dist > 0.05)
	{
		double OneDegreeAngle = PI/180.0;

		if(std::abs(errorTheta) <= OneDegreeAngle)
		{
			setSpeed(0.2,0.0);
		}
		else
		{
			if(errorTheta > 0.0)
			{
				setSpeed(0.0,0.2);
			}
			else if(errorTheta < 0.0)
			{
				setSpeed(0.0,-0.2);
			}
		}
	}
	else if (dist <= 0.1)
	{
		setSpeed(0.0,0.0);
	}

}

//Main Function Call
int main(int argc, char **argv)
{
	ros::init(argc, argv, "neptune_move_to_Goal");
	ros::NodeHandle n;

	NeptuneGoalMove moveToGoal(n);

	// How fast to run this node
	int rate = 10;
	ros::Rate r(rate);

	while (n.ok())
	{
	ros::spinOnce();

	moveToGoal.commandNeptune();

	r.sleep();
	}

	return 0;
}

