/*
 * Neptune_base driver interface
 * Author: Sandesh Gowda
 * Created:30/10/2014
 */

#include <ros/ros.h>
#include <labo3_base/EncoderData.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/JointState.h>
extern "C" {
#include <usermotor.c>
};

#define PI                  22/7
#define RAD_WHEEL           0.095
#define COUNTS_PER_ROT_LEFT 3037.81
#define COUNTS_PER_ROT_RGHT 3042.55
#define LEFT_BACKLASH	192
#define RGHT_BACKLASH	261
#define COUNTER_VALUE	65535
#define MOTOR_ON            1
#define MOTOR_OFF		    0
#define FORWARD             1
#define BACKWARD            0
#define DIST_BET_WHEELS		0.29032
#define LEFT_MOTOR			2
#define RGHT_MOTOR			3
#define LEFT_DIR			6
#define RGHT_DIR			7
#define NUM_MOTORS			4
#define SCALING_FACTOR			40*RAD_WHEEL // rad at 1m is (1/RAD_WHEEL)

struct EncoderStruct
{
	double leftTicks;
	double rghtTicks;
};

class Neptune
{
public:
	Neptune( ros::NodeHandle &node )  ;
	void neptuneOdometry()			  ;

private:
	void initialise();
	void neptuneDrive(const geometry_msgs::Twist::ConstPtr &vel);

	ros::Subscriber neptuneDrive_sub ;
	ros::Publisher  odom_pub		 ;
	ros::Publisher  joint_pub	     ;

	tf::TransformBroadcaster odom_broadcaster ;

	// Drive related variables
	double leftDriveVel			;
	double rghtDriveVel			;

	int power, motorPower		;

	int count			;

    	int desiredSpeeds[NUM_MOTORS]	;
    	//FIXING:: int encoderSpeedXtract[4];
	unsigned int encoderXtract[8]; // first 4 for counts and last 4 for direction

	labo3_base::EncoderData encoderData ;
	EncoderStruct   motorMovement    ;

	// Odometry related variables
	double circumference;

	double leftCurrPos         ;
	double rghtCurrPos         ;

	double velLeft   ;
	double velRght   ;

	double x                ;
	double y                ;
	double w                ;
	double vx               ;
	double vy               ;
	double vw               ;

	ros::Time currTime ;
	ros::Time movementCallBackTime ;

};

void Neptune::initialise()
{
	// Drive Initialization
	if (initMotors())
	{
		exit(-1);
	}

	configureSpeedPID (2, 2048, 4, 512);
	configureSpeedPID (3, 2048, 4, 512);

	setBrakes(0);
	motorPower = MOTOR_OFF;
	setMotorPower (motorPower);

	leftDriveVel = 0.0	;
	rghtDriveVel = 0.0	;
	desiredSpeeds[0] = 0.0	;
	desiredSpeeds[1] = 0.0	;

	//FIXING:: getMotorSpeedAll( encoderSpeedXtract );

	getEncoderAll( encoderXtract );
	encoderData.left_counter = encoderXtract[LEFT_MOTOR];
	encoderData.rght_counter = encoderXtract[RGHT_MOTOR];
	encoderData.left_dir     = encoderXtract[LEFT_DIR];
	encoderData.rght_dir     = encoderXtract[RGHT_DIR];

	ROS_INFO("encoderXtract[0] = %d", encoderXtract[0]);
	ROS_INFO("encoderXtract[1] = %d", encoderXtract[1]);
	ROS_INFO("encoderXtract[2] = %d", encoderXtract[2]);
	ROS_INFO("encoderXtract[3] = %d", encoderXtract[3]);
	                              
	ROS_INFO("encoderXtract[4] = %d", encoderXtract[4]);
	ROS_INFO("encoderXtract[5] = %d", encoderXtract[5]);
	ROS_INFO("encoderXtract[6] = %d", encoderXtract[6]);
	ROS_INFO("encoderXtract[7] = %d", encoderXtract[7]);

	count = 0;
	// Odometry Initialization
	//FIXING:: circumference      = 2*PI*RAD_WHEEL;

	velLeft     = 0.0 ;
	velRght     = 0.0 ;

	leftCurrPos         = 0.0 ;
	rghtCurrPos         = 0.0 ;

	x                    = 0.0                               ;
	y                    = 0.0                               ;
	w                    = 0.0                               ;
	vx                   = 0.0                               ;
	vy                   = 0.0                               ;
	vw                   = 0.0                               ;

	currTime = ros::Time::now()		             ;
	movementCallBackTime = ros::Time::now()		             ;

	ROS_INFO("Initialise");
}

Neptune::Neptune( ros::NodeHandle &node )
{
	neptuneDrive_sub =  node.subscribe<geometry_msgs::Twist>("cmd_vel", 1, &Neptune::neptuneDrive, this);
	odom_pub   		 =  node.advertise<nav_msgs::Odometry>("odom", 1);
	joint_pub   	 =  node.advertise<sensor_msgs::JointState>("joint_states", 1);
	initialise();
}

void Neptune::neptuneDrive(const geometry_msgs::Twist::ConstPtr& vel)
{
	float linearVel  = vel->linear.x	;
	float angularVel = vel->angular.z	;
	if(linearVel > 1)
	{
		linearVel = 1;
		ROS_INFO("MAX LINEAR SPEED is 1 m/s");
	}
	else if(linearVel < -1)
	{
		linearVel = -1;
		ROS_INFO("MAX LINEAR SPEED is 1 m/s");
	}
	if(angularVel > PI)
	{
		angularVel = PI;
		ROS_INFO("MAX ANGULAR SPEED is PI rad/s");
	}
	else if(angularVel < -PI)
	{
		angularVel = -PI;
		ROS_INFO("MAX ANGULAR SPEED is PI rad/s");
	}
	
	leftDriveVel = SCALING_FACTOR * (((2*(linearVel)) - ((angularVel)*DIST_BET_WHEELS))/(2*RAD_WHEEL));
	rghtDriveVel = SCALING_FACTOR * (((2*(linearVel)) + ((angularVel)*DIST_BET_WHEELS))/(2*RAD_WHEEL));

	power = getMotorPower ();
	if (ON != power)
	{
		motorPower = MOTOR_ON;
		setMotorPower (motorPower);
	}

	if (motorPower)
	{
		desiredSpeeds[2] = leftDriveVel;
		desiredSpeeds[3] = rghtDriveVel;
	}
	else
	{
		desiredSpeeds[2] = 0.0;
		desiredSpeeds[3] = 0.0;
	}
	setMotorSpeedAll (desiredSpeeds);
}

void Neptune::neptuneOdometry()
{
	currTime		= ros::Time::now();
	double dt               = (currTime - movementCallBackTime).toSec() ;

	if (dt == 0.0)
		return;
	if (count < 50)
	{
		getEncoderAll( encoderXtract );
		encoderData.left_counter = encoderXtract[LEFT_MOTOR];
		encoderData.rght_counter = encoderXtract[RGHT_MOTOR];
		encoderData.left_dir     = encoderXtract[LEFT_DIR];
		encoderData.rght_dir     = encoderXtract[RGHT_DIR];
		count++;
		return;
	}
	//FIXING:: getMotorSpeedAll(encoderSpeedXtract);
	getEncoderAll( encoderXtract );

	motorMovement.leftTicks = encoderData.left_counter - encoderXtract[LEFT_MOTOR]	;   // Number of ticks traveled
	motorMovement.rghtTicks = encoderData.rght_counter - encoderXtract[RGHT_MOTOR]	;   // Number of ticks traveled

	if(encoderData.left_counter != encoderXtract[LEFT_MOTOR])
	{
		ROS_INFO("encoderData.left_counter = %d", encoderData.left_counter);
		ROS_INFO("encoderXtract[LEFT_MOTOR] = %d", encoderXtract[LEFT_MOTOR]);
		ROS_INFO("encoderData.left_dir = %d", encoderData.left_dir);
		ROS_INFO("encoderXtract[LEFT_DIR] = %d", encoderXtract[LEFT_DIR]);
	}
/*
	if(encoderData.rght_counter != encoderXtract[RGHT_MOTOR])
	{
		ROS_INFO("encoderData.rght_counter = %d", encoderData.rght_counter);
		ROS_INFO("encoderXtract[RGHT_MOTOR] = %d", encoderXtract[RGHT_MOTOR]);
	}
*/
	if(encoderData.left_dir != encoderXtract[LEFT_DIR])
		motorMovement.leftTicks = (motorMovement.leftTicks + LEFT_BACKLASH);
	if(encoderData.rght_dir != encoderXtract[RGHT_DIR])
		motorMovement.rghtTicks = (motorMovement.rghtTicks + RGHT_BACKLASH);
	
	if(encoderXtract[LEFT_DIR] == BACKWARD)
		motorMovement.leftTicks = -(motorMovement.leftTicks)	;
	if(encoderXtract[RGHT_DIR] == BACKWARD)
		motorMovement.rghtTicks = -(motorMovement.rghtTicks)	;

	encoderData.left_counter = encoderXtract[LEFT_MOTOR];
	encoderData.rght_counter = encoderXtract[RGHT_MOTOR];
	encoderData.left_dir     = encoderXtract[LEFT_DIR];
	encoderData.rght_dir     = encoderXtract[RGHT_DIR];

	// *********************Moving the wheels of LABO3********************* //
	sensor_msgs::JointState joint_state;

	leftCurrPos   = 2 * PI * motorMovement.leftTicks / COUNTS_PER_ROT_LEFT; //radians
	rghtCurrPos   = 2 * PI * motorMovement.rghtTicks / COUNTS_PER_ROT_RGHT; //radians	

	if (leftCurrPos > (2 * PI))
		leftCurrPos = leftCurrPos - (2 * PI);
	else if (leftCurrPos < 0)
		leftCurrPos = (2 * PI) + leftCurrPos ;
	if (rghtCurrPos > (2 * PI))
		rghtCurrPos = rghtCurrPos - (2 * PI);
	else if (rghtCurrPos < 0)
		rghtCurrPos = (2 * PI) + rghtCurrPos ;


	joint_state.header.stamp 		= ros::Time::now() ;
	joint_state.name.push_back("left_wheel_joint");
	joint_state.position.push_back(leftCurrPos);

	joint_state.name.push_back("right_wheel_joint");
	joint_state.position.push_back((-1)*rghtCurrPos);

	joint_state.name.push_back("right_castor_frame_joint");
	joint_state.position.push_back(0.0);

	joint_state.name.push_back("right_castor_wheel_joint");
	joint_state.position.push_back(0.0);

	joint_state.name.push_back("left_castor_frame_joint");
	joint_state.position.push_back(0.0);

	joint_state.name.push_back("left_castor_wheel_joint");
	joint_state.position.push_back(0.0);

	joint_state.name.push_back("laser_joint");
	joint_state.position.push_back(0.0);

	joint_state.name.push_back("camera_joint");
	joint_state.position.push_back(0.0);

	joint_pub.publish(joint_state);
	// ******************************************************************** //

	// ***********************Moving the LABO3 ROBOT*********************** //

	velLeft = leftCurrPos / dt ; // rad/sec
	velRght = rghtCurrPos / dt ; // rad/sec

	//ROS_INFO("velLeft = %lf", velLeft);
    //ROS_INFO("velRght = %lf", velRght);

	// Platform Velocities
	vx = 0.5 * RAD_WHEEL * ( velRght + velLeft ) * cos(w)    ;
	vy = 0.5 * RAD_WHEEL * ( velRght + velLeft ) * sin(w)    ;
	vw = ( velRght - velLeft ) * RAD_WHEEL / DIST_BET_WHEELS ;

	//ROS_INFO("vx = %lf", vx);
    //ROS_INFO("vy = %lf", vy);

	double delta_x = vx * dt ;
	double delta_y = vy * dt ;
	double delta_w = vw * dt ;

	x += delta_x ;
	y += delta_y ;
	w += delta_w ;

	//ROS_INFO("x = %lf", x);
    //ROS_INFO("y = %lf", y);
    //ROS_INFO("w = %lf", w);

    //Quaternion created from yaw
	geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(w) ;

	// Step 1: Publish the transform over tf
	geometry_msgs::TransformStamped odom_trans ;
	odom_trans.header.stamp     = currTime ;
	odom_trans.header.frame_id  = "odom"       ;
	odom_trans.child_frame_id   = "base_footprint"  ;

	odom_trans.transform.translation.x = x     ;
	odom_trans.transform.translation.y = y     ;
	odom_trans.transform.translation.z = 0.0   ;
	odom_trans.transform.rotation = odom_quat  ;

	//send the transform
	odom_broadcaster.sendTransform(odom_trans) ;

	// Step 2: Publish the odometry message over ROS
	nav_msgs::Odometry odom             ;
	odom.header.stamp    = currTime ;
	odom.header.frame_id = "odom"       ;

	//set the position
	odom.pose.pose.position.x  = x         ;
	odom.pose.pose.position.y  = y         ;
	odom.pose.pose.position.z  = 0.0       ;
	odom.pose.pose.orientation = odom_quat ;

	//set the velocity
	odom.child_frame_id        = "base_footprint" ;
	odom.twist.twist.linear.x  = vx          ;
	odom.twist.twist.linear.y  = vy          ;
	odom.twist.twist.angular.z = vw          ;

	//publish the message
	odom_pub.publish(odom) ;

	movementCallBackTime    = ros::Time::now();
}

//Main Function Call
int main(int argc, char **argv)
{
	ros::init(argc, argv, "neptune_launcher");
	ros::NodeHandle n;

	Neptune neptune(n);

	// How fast to run this node
	int rate = 50;
	ros::Rate r(rate);

	while (n.ok())
	{
	ros::spinOnce();

	neptune.neptuneOdometry();

	r.sleep();
	}

	return 0;
}

